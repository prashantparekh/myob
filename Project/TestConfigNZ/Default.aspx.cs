﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Security.Policy;

public partial class _Default : System.Web.UI.Page
{
    DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
    string strParentPath;
    string strFilePath;
    static string strBusiSelFlag = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblPath.Text = "";
        try
        {
            di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            strParentPath = di.ToString(); //di.Parent.FullName + "\\ExportoryAutomation\\bin\\debug\\";
            strFilePath = strParentPath + "testlist.txt";

            string strNameSpce = "TestCases.TestSuites.";
            if (!File.Exists(strFilePath)) File.Create(strFilePath);
            if (File.Exists(strFilePath))
            {
                File.Delete(strFilePath);
                using (StreamWriter sw = new StreamWriter(strFilePath))
                {
                    sw.AutoFlush = true;
                    if (chkSignUpWithAllDemoBusiness.Checked) sw.WriteLine(strNameSpce + chkSignUpWithAllDemoBusiness.Value);
                    else if (chkSignupAndChangeEmail.Checked) sw.WriteLine(strNameSpce + chkSignupAndChangeEmail.Value);
                    else if (chkUserRegistrationAndSignUpWithAllDemoBusiness.Checked) sw.WriteLine(strNameSpce + chkUserRegistrationAndSignUpWithAllDemoBusiness.Value);

                    else
                    {
                        //if (chkCreateInvoice.Checked) sw.WriteLine(strNameSpce + chkCreateInvoice.Value);
                        //if (chkSignupAndChangeEmail.Checked) sw.WriteLine(strNameSpce + chkSignupAndChangeEmail.Value);
                        //if (chkCreateItems.Checked) sw.WriteLine(strNameSpce + chkCreateItems.Value);
                        //if (chkCreateCustomers.Checked) sw.WriteLine(strNameSpce + chkCreateCustomers.Value);
                        //if (chkCreateSuppliers.Checked) sw.WriteLine(strNameSpce + chkCreateSuppliers.Value);
                        //if (chkPayRuns.Checked) sw.WriteLine(strNameSpce + chkPayRuns.Value);
                    }
                }
            }

            lblPath.Text = "Data saved successfully.";
        }
        catch (Exception ex)
        {
            lblPath.Text += ex.Message;
        }
    }
    protected void lnkGotoJenkins_Click(object sender, EventArgs e)
    {
        char[] delim = {'/'};
        //Response.Write("<br>" + Request.Url.AbsoluteUri.Split(delim)[0] + "//" + Request.Url.AbsoluteUri.Split(delim)[2] + ":8080/job/MYOB/");
        //Response.Write("<br>" + Request.Url.Segments[0]);
        //Response.Write("<br>" + Request.Url.Segments[1]);
        if (strBusiSelFlag.Trim().Length > 0)
            Response.Redirect(Request.Url.AbsoluteUri.Split(delim)[0] + "//" + Request.Url.AbsoluteUri.Split(delim)[2] + ":8080/job/MYOB_Automation_" + strBusiSelFlag + "/");
        else
            Response.Redirect(Request.Url.AbsoluteUri.Split(delim)[0] + "//" + Request.Url.AbsoluteUri.Split(delim)[2] + ":8080/job/MYOB_Automation_AUS" + strBusiSelFlag + "/");
        
    }
    //protected void rdoAus_CheckedChanged(object sender, EventArgs e)
    //{
    //    Response.Redirect("a.htm");
    //    lblPath.Text = "1111";
    //    strBusiSelFlag = "AUS";
    //    dvLCPanel.Visible = true;
    //    Response.Write("test aus");
    //}
    //protected void rdoNZ_CheckedChanged(object sender, EventArgs e)
    //{
    //    Response.Redirect("b.htm");
    //    lblPath.Text = "2222";
    //    strBusiSelFlag = "NZ";
    //    dvLCPanel.Visible = true;
    //    Response.Write("test NS");
    //}
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("a.htm");
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        Response.Redirect("a.htm");
    }
    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        Response.Redirect("a.htm");
    }
    protected void ddlBusiness_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBusiness.SelectedIndex == 0)
        {
            strBusiSelFlag = "AUS";
            dvLCPanel.Visible = true;
            
        }
        if (ddlBusiness.SelectedIndex == 1)
        {
            strBusiSelFlag = "NZ";
            dvLCPanel.Visible = true;
            
        }
    }
}
