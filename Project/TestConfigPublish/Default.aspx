﻿<%@ page title="Home Page" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" inherits="_Default, App_Web_oxdhsbtz" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" language="javascript">
        function CheckAll(objPnl, objEle) {
            var tab = document.getElementById(objPnl); // table with id tbl1
            var elems = tab.getElementsByTagName("input");
            var len = elems.length;

            for (var i = 0; i < len; i++) {
                if (elems[i].type == "checkbox") {
                    elems[i].checked = objEle.checked;
                }
            }
        }
        function IsTestSelected(objPnl, objEle) {
            var tab = document.getElementById(objPnl); // table with id tbl1
            var elems = tab.getElementsByTagName("input");
            var len = elems.length;
            var intChkCnt = 0;

            for (var i = 0; i < len; i++) {
                if (elems[i].type == "checkbox" && elems[i].checked) {
                    intChkCnt++;
                    return true;
                }
            }
            if(intChkCnt == 0) {
                alert("Please select atleast one test.") 
                return false;
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Automation Tests Configuration
    </h2>
    <br />
 
    <div style="text-align: center; font-weight: 600;">
        <asp:Label ID="lblPath" runat="server" Text=""></asp:Label></div>
    

    <div>
    Automation tests must be run on Firefox.
        <fieldset>
            <legend>Select business</legend>
            <table >
                <tr>
                    <%--<td>
                        
                        <asp:RadioButton ID="rdoAus" runat="server" Text="Aus" GroupName="BusiSel" oncheckedchanged="rdoAus_CheckedChanged" />
                        
                    </td>--%>
                
                    <td>
                        
                        <%--<asp:RadioButton ID="rdoNZ" runat="server" Text="NZ" GroupName="BusiSel" oncheckedchanged="rdoNZ_CheckedChanged" />--%>
                        
                        <asp:DropDownList ID="ddlBusiness" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="ddlBusiness_SelectedIndexChanged" >
                            <asp:ListItem Value="0">-- Select --</asp:ListItem>
                            <asp:ListItem>AUS</asp:ListItem>
                            <asp:ListItem>NZ</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ddlBusiness" ValidationGroup="Save" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div id="dvLCPanel" runat="server" class="dvHide" visible="false" >
    <div>
        <%--<fieldset>
            <legend>Select Browser </legend>
            
            <table>
                <tr>
                    <td>
                        <input id="chkFirefox" runat="server" type="radio" value="1" name="browser" checked />Firefox
                    </td>
                    <td>
                        <input id="chkChrome" runat="server" type="radio" value="2" name="browser" />Chrome
                    </td>
                    <td>
                        <input id="ChkIntExp" runat="server" type="radio" value="3" name="browser" />Internet
                        Explorer
                    </td>
                    <td>
                        <input id="ChkSafari" runat="server" type="radio" value="4" name="browser" />Safari
                    </td>
                </tr>
            </table>
        </fieldset>--%>
    </div>
    <div>
        <fieldset>
            
                <input id="chkSignUpWithAllDemoBusiness" type="checkbox" runat="server" onclick="javascript:CheckAll('tblSignUp',this);"
                    value="SignUpWithAllDemoBusiness" />Create new business for advisor with demo business.
            
        </fieldset>
    </div>

    <%--<div>
        <fieldset>
            <legend>
                <input id="chkSignUpWithAllDemoBusiness" type="checkbox" runat="server" onclick="javascript:CheckAll('tblSignUp',this);"
                    value="SignUpWithAllDemoBusiness" />Create new business for advisor with demo
                business </legend>
            <table id="tblSignUp">
                <tr>
                    <td>
                        <input id="chkSignupAndChangeEmail" runat="server" type="checkbox" value="UserRegistration" />Check
                        user Sign up and edit Email Address Functionality (read data from excel).
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkCreateInvoice" runat="server" type="checkbox" value="CreateInvoice" />Create
                        Invoice
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkCreateItems" runat="server" type="checkbox" value="CreateItems" />Create
                        Items
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkCreateCustomers" runat="server" type="checkbox" value="CreateCustomer" />Create
                        Customers
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkCreateSuppliers" runat="server" type="checkbox" value="CreateSupplier" />Create
                        Suppliers
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkCreateEmployee" runat="server" type="checkbox" value="CreateEmployee" />Create
                        Employees
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkPayRuns" runat="server" type="checkbox" value="Payruns" />Pay runs
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>--%>
    <div style="text-align: center; white-space: nowrap;">
        <center>
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text=" Save " OnClick="btnSave_Click" OnClientClick="javascript:return IsTestSelected('tblSignUp',this)" CausesValidation="true" ValidationGroup="Save" />
                    </td>
                    <td>
                        |
                    </td>
                    <td>
                        <%--<a href="http://localhost:8080/job/MYOB/" class="btn">Go to Jenkins to run suite(s) </a>--%>
                        <asp:LinkButton ID="lnkGotoJenkins" runat="server" class="btn" OnClick="lnkGotoJenkins_Click">Go to Jenkins to run suite(s) </asp:LinkButton>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </div>
    
    <br />
    <br />
</asp:Content>
