﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Security.Policy;

public partial class _Default : System.Web.UI.Page
{
    DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
    string strParentPath;
    string strFilePath;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblPath.Text = "";
        try
        {
            di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            strParentPath = di.ToString(); //di.Parent.FullName + "\\ExportoryAutomation\\bin\\debug\\";
            strFilePath = strParentPath + "testlist.txt";

            string strNameSpce = "TestCases.TestSuites.";
            if (!File.Exists(strFilePath)) File.Create(strFilePath);
            if (File.Exists(strFilePath))
            {
                File.Delete(strFilePath);
                using (StreamWriter sw = new StreamWriter(strFilePath))
                {
                    sw.AutoFlush = true;
                    if (chkSignUpWithAllDemoBusiness.Checked) sw.WriteLine(strNameSpce + chkSignUpWithAllDemoBusiness.Value);
                    else
                    {
                        if (chkCreateInvoice.Checked) sw.WriteLine(strNameSpce + chkCreateInvoice.Value);
                        if (chkSignupAndChangeEmail.Checked) sw.WriteLine(strNameSpce + chkSignupAndChangeEmail.Value);
                        if (chkCreateItems.Checked) sw.WriteLine(strNameSpce + chkCreateItems.Value);
                        if (chkCreateCustomers.Checked) sw.WriteLine(strNameSpce + chkCreateCustomers.Value);
                        if (chkCreateSuppliers.Checked) sw.WriteLine(strNameSpce + chkCreateSuppliers.Value);
                        if (chkPayRuns.Checked) sw.WriteLine(strNameSpce + chkPayRuns.Value);
                    }
                    //sw.WriteLine(strNameSpce + "zzzGenerateSummaryReport");
                    //lblPath.Text = "Data saved successfully.";
                }
            }


            //string cfgPath = di.Parent.FullName + @"\automated-test-script\bin\Debug\LogMeOnce.dll.config"; //@"D:\Prashant1\LogMeOnce\Project\LogMeOnce\bin\Debug\LogMeOnce.dll.config";
            //var configMap = new ExeConfigurationFileMap { ExeConfigFilename = cfgPath };
            //var cf = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            //if (chkChrome.Checked) cf.AppSettings.Settings["UseBrowser"].Value = chkChrome.Value;
            //else if (ChkIntExp.Checked) cf.AppSettings.Settings["UseBrowser"].Value = ChkIntExp.Value;
            //else if (ChkSafari.Checked) cf.AppSettings.Settings["UseBrowser"].Value = ChkSafari.Value;
            //else cf.AppSettings.Settings["UseBrowser"].Value = chkFirefox.Value;
            //cf.Save();
            lblPath.Text = "Data saved successfully.";
        }
        catch (Exception ex)
        {
            lblPath.Text += ex.Message;
        }
    }
    protected void lnkGotoJenkins_Click(object sender, EventArgs e)
    {
        char[] delim = {'/'};
        //Response.Write("<br>" + Request.Url.AbsoluteUri.Split(delim)[0] + "//" + Request.Url.AbsoluteUri.Split(delim)[2] + ":8080/job/MYOB/");
        //Response.Write("<br>" + Request.Url.Segments[0]);
        //Response.Write("<br>" + Request.Url.Segments[1]);
        Response.Redirect(Request.Url.AbsoluteUri.Split(delim)[0] + "//" + Request.Url.AbsoluteUri.Split(delim)[2] + ":8080/job/MYOB_Automation/");
    }
}
