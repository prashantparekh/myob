﻿using System;
using System.Configuration;
using System.Data;
using Framework.Init;
using OpenQA.Selenium;

namespace Framework.PageObjects
{
    public class ContactsObjects
    {
        IWebDriver _driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strSignupUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("SignupUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Dashboard_lbl_HeaderWelcome;
        IWebElement Dashboard_btn_logout;
        IWebElement Dashboard_lnk_MenuInvoices;
        IWebElement Dashboard_lnk_MenuExpenses;
        IWebElement Dashboard_lnk_MenuBanking;
        IWebElement Dashboard_lnk_MenuContacts;
        IWebElement Dashboard_lnk_MenuPayroll;
        IWebElement Dashboard_lnk_MenuReports;
        IWebElement Dashboard_lnk_SubMenuCreateContact;
        IWebElement CreateContact_txt_BusinessName;
        IWebElement CreateContact_txt_Address;
        IWebElement CreateContact_txt_Suburb;
        IWebElement CreateContact_txt_State;
        IWebElement CreateContact_txt_PostCode;
        IWebElement CreateContact_txt_Country;
        IWebElement CreateContact_btn_SaveAndAddAnother;
        IWebElement CreateContact_ddl_ContactIs;

        public IWebDriver CreateCustomer(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Create contact page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/contacts/new");
            /* commented to redirect to page via static URL 
            Actions actions = new Actions(_driver);
            actions.MoveToElement(Dashboard_lnk_MenuContacts).Build().Perform();
            new Common(_driver).pause(3000);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuCreateContact));
                Dashboard_lnk_SubMenuCreateContact = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuCreateContact));
                actions.MoveToElement(Dashboard_lnk_MenuContacts).MoveToElement(Dashboard_lnk_SubMenuCreateContact).Click().Build().Perform();
                Report.AddToHtmlReportPassed("'Create contact' submenu button under Contacts menu on Dashboard Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Create contact' submenu button under Contacts menu on Dashboard Page.");
            }*/

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Create contact page for Customer.", false, true);
            InitCreateContactPageElements();
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [Customers$] ", strRegDataExcelFilePath);
            string strBusinessName = string.Empty, strAddress = string.Empty, strSuburb = string.Empty, strState = string.Empty, strPostCode = string.Empty, strCountry = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InitCreateContactPageElements(false);
                new Common(_driver).pause(2000);
                strBusinessName = Convert.ToString(dt.Rows[i][0]);
                strAddress = Convert.ToString(dt.Rows[i][1]);
                strSuburb = Convert.ToString(dt.Rows[i][2]);
                strState = Convert.ToString(dt.Rows[i][3]);
                strPostCode = Convert.ToString(dt.Rows[i][4]);
                strCountry = Convert.ToString(dt.Rows[i][5]);
                Common.enterText(CreateContact_txt_BusinessName, strBusinessName);
                Common.enterText(CreateContact_txt_Address, strAddress);
                Common.enterText(CreateContact_txt_Suburb, strSuburb);
                Common.enterText(CreateContact_txt_State, strState);
                Common.enterText(CreateContact_txt_PostCode, strPostCode);
                Common.enterText(CreateContact_txt_Country, strCountry);

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Business name: " + strBusinessName, false);
                Report.AddToHtmlReport("Address: " + strAddress, false);
                Report.AddToHtmlReport("Suburb: " + strSuburb, false);
                Report.AddToHtmlReport("State: " + strState, false);
                Report.AddToHtmlReport("Post code: " + strPostCode, false);
                Report.AddToHtmlReport("Country: " + strCountry + "<br>", false);
                Report.AddToHtmlReport("<hr>");

                CreateContact_btn_SaveAndAddAnother.Click();
                new Common(_driver).pause(4000);
            }

            return _driver;
        }

        public IWebDriver CreateSupplier(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Create contact page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/contacts/new");
            /* commented to redirect to page via static URL 
            Actions actions = new Actions(_driver);
            actions.MoveToElement(Dashboard_lnk_MenuContacts).Build().Perform();
            new Common(_driver).pause(1000);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuCreateContact));
                Dashboard_lnk_SubMenuCreateContact = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuCreateContact));
                actions.MoveToElement(Dashboard_lnk_MenuContacts).MoveToElement(Dashboard_lnk_SubMenuCreateContact).Click().Build().Perform();
                Report.AddToHtmlReportPassed("'Create contact' submenu button under Contacts menu on Dashboard Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Create contact' submenu button under Contacts menu on Dashboard Page.");
            }*/

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Create contact page for Supplier.", false, true);
            InitCreateContactPageElements();
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [Suppliers$] ", strRegDataExcelFilePath);
            string strBusinessName = string.Empty, strAddress = string.Empty, strSuburb = string.Empty, strState = string.Empty, strPostCode = string.Empty, strCountry = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InitCreateContactPageElements(false);
                new Common(_driver).pause(2000);
                strBusinessName = Convert.ToString(dt.Rows[i][0]);
                strAddress = Convert.ToString(dt.Rows[i][1]);
                strSuburb = Convert.ToString(dt.Rows[i][2]);
                strState = Convert.ToString(dt.Rows[i][3]);
                strPostCode = Convert.ToString(dt.Rows[i][4]);
                strCountry = Convert.ToString(dt.Rows[i][5]);
                Common.enterText(CreateContact_txt_BusinessName, strBusinessName);
                new Common(_driver).select(CreateContact_ddl_ContactIs, "Supplier");
                Common.enterText(CreateContact_txt_Address, strAddress);
                Common.enterText(CreateContact_txt_Suburb, strSuburb);
                Common.enterText(CreateContact_txt_State, strState);
                Common.enterText(CreateContact_txt_PostCode, strPostCode);
                Common.enterText(CreateContact_txt_Country, strCountry);

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Business name: " + strBusinessName, false);
                Report.AddToHtmlReport("Contact is: Supplier", false);
                Report.AddToHtmlReport("Address: " + strAddress, false);
                Report.AddToHtmlReport("Suburb: " + strSuburb, false);
                Report.AddToHtmlReport("State: " + strState, false);
                Report.AddToHtmlReport("Post code: " + strPostCode, false);
                Report.AddToHtmlReport("Country: " + strCountry + "<br>", false);
                Report.AddToHtmlReport("<hr>");

                CreateContact_btn_SaveAndAddAnother.Click();
                new Common(_driver).pause(4000);
            }

            return _driver;
        }

        private void InitCreateContactPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.CreateContact_txt_BusinessName));
                CreateContact_txt_BusinessName = _driver.FindElement(By.XPath(ElementLocators.CreateContact_txt_BusinessName));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Business name' textbox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Business name' textbox on Create contact Page.");
            }
            try
            {
                CreateContact_ddl_ContactIs = _driver.FindElement(By.XPath(ElementLocators.CreateContact_ddl_ContactIs));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contact is' combobox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contact is' combobox on Create contact Page.");
            }
            try
            {
                CreateContact_txt_Address = _driver.FindElement(By.XPath(ElementLocators.CreateContact_txt_Address));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Address' textbox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Address' textbox on Create contact Page.");
            }
            try
            {
                CreateContact_txt_Suburb = _driver.FindElement(By.XPath(ElementLocators.CreateContact_txt_Suburb));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Suburb' textbox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Suburb' textbox on Create contact Page.");
            }
            try
            {
                CreateContact_txt_State = _driver.FindElement(By.XPath(ElementLocators.CreateContact_txt_State));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'State' textbox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'State' textbox on Create contact Page.");
            }
            try
            {
                CreateContact_txt_PostCode = _driver.FindElement(By.XPath(ElementLocators.CreateContact_txt_PostCode));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Post code' textbox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Post code' textbox on Create contact Page.");
            }
            try
            {
                CreateContact_txt_Country = _driver.FindElement(By.XPath(ElementLocators.CreateContact_txt_Country));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Country' textbox on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Country' textbox on Create contact Page.");
            }
            try
            {
                CreateContact_btn_SaveAndAddAnother = _driver.FindElement(By.XPath(ElementLocators.CreateContact_btn_SaveAndAddAnother));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save & add another' button on Create contact Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save & add another' button on Create contact Page.");
            }
        }

        private void InitDashBoardPageElements(bool flgShowInReport = true)
        {
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                Dashboard_lbl_HeaderWelcome = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuInvoices = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuInvoices));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoices' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Invoices' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuExpenses = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuExpenses));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expenses' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expenses' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuBanking = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuBanking));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Banking' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Banking' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuContacts = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuContacts));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contacts' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contacts' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuPayroll = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuPayroll));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Payroll' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Payroll' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuReports = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuReports));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Reports' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Reports' menu button on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Logout' button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Logout' button on Dashboard Page.");
            }
        }

    }
}
