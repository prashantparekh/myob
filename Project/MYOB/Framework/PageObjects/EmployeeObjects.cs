﻿using System;
using System.Configuration;
using System.Data;
using Framework.Init;
using OpenQA.Selenium;

namespace Framework.PageObjects
{
    public class EmployeeObjects
    {
        IWebDriver _driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strSignupUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("SignupUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Dashboard_lbl_HeaderWelcome;
        IWebElement Dashboard_btn_logout;
        IWebElement Dashboard_lnk_MenuInvoices;
        IWebElement Dashboard_lnk_MenuExpenses;
        IWebElement Dashboard_lnk_MenuBanking;
        IWebElement Dashboard_lnk_MenuContacts;
        IWebElement Dashboard_lnk_MenuPayroll;
        IWebElement Dashboard_lnk_MenuReports;
        IWebElement Dashboard_lnk_SubMenuEmployees;
        IWebElement Employees_btn_AddEmployee;
        IWebElement CreateEmployee_txt_FirstName;
        IWebElement CreateEmployee_txt_LastName;
        IWebElement CreateEmployee_txt_StartDate;
        IWebElement CreateEmployee_txt_JobTitle;
        IWebElement CreateEmployee_txt_TFN;
        IWebElement CreateEmployee_rdo_AnnualSalary;
        IWebElement CreateEmployee_txt_SalaryRate;
        IWebElement CreateEmployee_txt_HoursPerWeek;
        IWebElement CreateEmployee_lnk_TabTax;
        IWebElement CreateEmployee_chk_AusResident;
        IWebElement CreateEmployee_chk_TaxFreeThresholdClaimed;
        IWebElement CreateEmployee_chk_Declaration;
        IWebElement CreateEmployee_btn_Save;



        public IWebDriver CreateEmployee(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Create employee page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/app/businesses/" + TestCases.Login.strURLDynCode + "/employees");
            /* commented to redirect to page via static URL 
            Actions actions = new Actions(_driver);
            actions.MoveToElement(Dashboard_lnk_MenuPayroll).Build().Perform();
            new Common(_driver).pause(1000);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuEmployees));
                Dashboard_lnk_SubMenuEmployees = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuEmployees));
                actions.MoveToElement(Dashboard_lnk_MenuPayroll).MoveToElement(Dashboard_lnk_SubMenuEmployees).Click().Build().Perform();
                Report.AddToHtmlReportPassed("'Employees' submenu button under Payroll menu on Dashboard Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Employees' submenu button under Payroll menu on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).pause(2000);
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Employees_btn_AddEmployee));
                Employees_btn_AddEmployee = _driver.FindElement(By.XPath(ElementLocators.Employees_btn_AddEmployee));
                Employees_btn_AddEmployee.Click();
                Report.AddToHtmlReportPassed("'Add employee' button on Employees Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Add employee' button on Employees Page.");
            }

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into New Employee page.", false, true);
            InitEmployeeDetailsTabElements();
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [Employees$] ", strRegDataExcelFilePath);
            string strFirstName = string.Empty, strLastName = string.Empty, strStartDate = string.Empty, strJobTitle = string.Empty, strTFN = string.Empty, strPaidOn = string.Empty, strSalaryRate = string.Empty, strHoursPerWeek = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i > 0)
                {
                    try
                    {
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.Employees_btn_AddEmployee));
                        Employees_btn_AddEmployee = _driver.FindElement(By.XPath(ElementLocators.Employees_btn_AddEmployee));
                        Employees_btn_AddEmployee.Click();
                    }
                    catch (Exception) { }
                    InitEmployeeDetailsTabElements(false);
                }
                new Common(_driver).pause(2000);
                strFirstName = Convert.ToString(dt.Rows[i][0]);
                strLastName = Convert.ToString(dt.Rows[i][1]);
                strStartDate = Convert.ToString(dt.Rows[i][2]);
                if (strStartDate.Length > 10) strStartDate = strStartDate.Substring(0, strStartDate.IndexOf(" "));
                strStartDate = Convert.ToDateTime(strStartDate).ToString("dd/MM/yyyy");
                strJobTitle = Convert.ToString(dt.Rows[i][3]);
                strTFN = Convert.ToString(dt.Rows[i][4]);
                strPaidOn = Convert.ToString(dt.Rows[i][5]);
                strSalaryRate = Convert.ToString(dt.Rows[i][6]);
                strHoursPerWeek = Convert.ToString(dt.Rows[i][7]);
                Common.enterText(CreateEmployee_txt_FirstName, strFirstName);
                Common.enterText(CreateEmployee_txt_LastName, strLastName);
                Common.enterText(CreateEmployee_txt_StartDate, strStartDate);
                Common.enterText(CreateEmployee_txt_JobTitle, strJobTitle);
                Common.enterText(CreateEmployee_txt_TFN, strTFN);
                if (strPaidOn.ToLower() == "annual salary") CreateEmployee_rdo_AnnualSalary.Click();
                Common.enterText(CreateEmployee_txt_SalaryRate, strSalaryRate);
                Common.enterText(CreateEmployee_txt_HoursPerWeek, strHoursPerWeek);

                Report.AddToHtmlReport("<br>Data Entered in Employee details tab.", false, false, true);
                Report.AddToHtmlReport("First name: " + strFirstName, false);
                Report.AddToHtmlReport("Last name: " + strLastName, false);
                Report.AddToHtmlReport("Start date: " + strStartDate, false);
                Report.AddToHtmlReport("Job title: " + strJobTitle, false);
                Report.AddToHtmlReport("Tax file number: " + strTFN, false);
                Report.AddToHtmlReport("Employee is paid on: " + strPaidOn, false);
                Report.AddToHtmlReport("Salary rate ($/yr): " + strSalaryRate, false);
                Report.AddToHtmlReport("Hours per week: " + strHoursPerWeek + "<br>", false);
                
                CreateEmployee_lnk_TabTax.Click();
                new Common(_driver).pause(1000);
                //if(i == 0) InitTaxTabElements();
                //else 
                InitTaxTabElements(false);
                new Common(_driver).pause(1000);
                
                if (CreateEmployee_chk_AusResident.GetAttribute("checked") == null) CreateEmployee_chk_AusResident.Click();
                if (CreateEmployee_chk_TaxFreeThresholdClaimed.GetAttribute("checked") == null) CreateEmployee_chk_TaxFreeThresholdClaimed.Click();
                if (CreateEmployee_chk_Declaration.GetAttribute("checked") == null) CreateEmployee_chk_Declaration.Click();

                Report.AddToHtmlReport("Data Entered in Tax tab.", false, false, true);
                Report.AddToHtmlReport("Australian resident for tax purposes: Checked", false);
                Report.AddToHtmlReport("Tax free threshold is being claimed: Checked", false);
                Report.AddToHtmlReport("Declaration: Checked <br>", false);
                Report.AddToHtmlReport("<hr>");

                CreateEmployee_btn_Save.Click();
                new Common(_driver).pause(3000);
            }

            return _driver;
        }

        private void InitTaxTabElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.CreateEmployee_chk_AusResident));
                CreateEmployee_chk_AusResident = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_chk_AusResident));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Australian resident for tax purposes' checkbox under Tax tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Australian resident for tax purposes' checkbox under Tax tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_chk_TaxFreeThresholdClaimed = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_chk_TaxFreeThresholdClaimed));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Tax free threshold is being claimed' checkbox under Tax tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Tax free threshold is being claimed' checkbox under Tax tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_chk_Declaration = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_chk_Declaration));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Declaration' checkbox under Tax tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Declaration' checkbox under Tax tab on New Employee Page.");
            }
        }

        private void InitEmployeeDetailsTabElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.CreateEmployee_txt_FirstName));
                CreateEmployee_txt_FirstName = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_FirstName));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'First name' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'First name' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_txt_LastName = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_LastName));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Last name' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Last name' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_txt_StartDate = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_StartDate));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Start date' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Start date' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_txt_JobTitle = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_JobTitle));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Job title' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Job title' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_txt_TFN = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_TFN));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Tax file number' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Tax file number' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_rdo_AnnualSalary = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_rdo_AnnualSalary));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'annual salary' radio button under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'annual salary' radio button under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_txt_SalaryRate = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_SalaryRate));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Salary rate ($/yr)' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Salary rate ($/yr)' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_txt_HoursPerWeek = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_txt_HoursPerWeek));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'hours per week' textbox under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'hours per week' textbox under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_lnk_TabTax = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_lnk_TabTax));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Tax' tab under Employee details tab on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Tax' tab under Employee details tab on New Employee Page.");
            }
            try
            {
                CreateEmployee_btn_Save = _driver.FindElement(By.XPath(ElementLocators.CreateEmployee_btn_Save));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save' button on New Employee Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save' button on New Employee Page.");
            }
        }

        private void InitDashBoardPageElements(bool flgShowInReport = true)
        {
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                Dashboard_lbl_HeaderWelcome = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuInvoices = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuInvoices));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoices' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Invoices' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuExpenses = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuExpenses));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expenses' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expenses' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuBanking = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuBanking));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Banking' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Banking' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuContacts = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuContacts));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contacts' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contacts' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuPayroll = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuPayroll));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Payroll' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Payroll' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuReports = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuReports));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Reports' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Reports' menu button on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Logout' button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Logout' button on Dashboard Page.");
            }
        }
    }
}
