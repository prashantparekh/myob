﻿using System;
using System.Configuration;
using System.Data;
using Framework.Init;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
//using Microsoft.Office.Interop.Excel;

namespace Framework.PageObjects
{
    public class LoginObjects
    {
        //bool IsTCAdded = false;
        IWebDriver _driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strSignupUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("SignupUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Login_lbl_Header;
        IWebElement Login_txt_Email;
        IWebElement Login_txt_Password;
        IWebElement Login_btn_Login;
        IWebElement Login_lnk_NotRegistered;
        IWebElement Login_lnk_ForgetYourPassword;
        IWebElement Dashboard_lbl_HeaderWelcome;
        IWebElement Dashboard_btn_logout;
        IWebElement Login_lbl_ErrEnterEmail;
        IWebElement Login_lbl_ErrEnterPwd;
        IWebElement Login_lbl_ErrInvalidUserPwd;
        IWebElement Login_lbl_HeaderForgetPwd;
        IWebElement Dashboard_lnk_MenuInvoices;
        IWebElement Dashboard_lnk_MenuExpenses;
        IWebElement Dashboard_lnk_MenuBanking; 
        IWebElement Dashboard_lnk_MenuContacts;
        IWebElement Dashboard_lnk_MenuPayroll;
        IWebElement Dashboard_lnk_MenuReports;
        IWebElement Dashboard_lnk_SubMenuCreateInvoice;
        IWebElement CreateInvoice_ddl_InvoiceTo;
        IWebElement InvoiceTo_lbl_DavidJonesClothi;
        IWebElement InvoiceGrid_td_Row1ItemCell;
        IWebElement InvoiceGrid_ddl_Col1ItemCell;
        IWebElement InvoiceGrid_lbl_ItemWM324;
        IWebElement InvoiceGrid_td_Row2ItemCell;
        IWebElement InvoiceGrid_lbl_ItemWR99;
        IWebElement CreateInvoice_txt_Notes;
        IWebElement CreateInvoice_btn_Save;
        IWebElement CreateInvoice_btn_SaveNAdd;
        IWebElement CreateInvoice_btn_PreviewNPrint;
        IWebElement CreateInvoice_btn_EmailToCust;
        IWebElement Reg_txt_FirstName;
        IWebElement Reg_txt_SurName;
        IWebElement Reg_txt_Email;
        IWebElement Reg_txt_PhoneNo;
        IWebElement Reg_txt_UserName;
        IWebElement Reg_txt_Password;
        IWebElement Reg_txt_ConfPassword;
        IWebElement Reg_txt_BusinessName;
        IWebElement Reg_btn_Save;
        IWebElement RegDone_lbl_HeaderDone;
        IWebElement RegDone_lbl_ToComplete;
        IWebElement SetupAssi_btn_Next;
        IWebElement SetupAssi_txt_BusinessName;
        IWebElement SetupAssi_txt_TradingName;
        IWebElement SetupAssi_txt_PhoneNumber;
        IWebElement SetupAssi_txt_Email;
        IWebElement SetupAssi_txt_ABN;
        IWebElement SetupAssi_txt_BusinessAddr;
        IWebElement SetupAssi_txt_BusinessCity;
        IWebElement SetupAssi_txt_BusinessState;
        IWebElement SetupAssi_txt_BusinessPostCode;
        IWebElement SetupAssi_chk_RegisteredForGST;
        IWebElement SetupAssi_chk_SameAsBusinessAddr;
        IWebElement SetupAssi_btn_Finish;
        IWebElement SetupAssi_btn_StartUsingLiveAccounts;
        IWebElement InvoiceTo_lbl_ddlFirstItem;
        IWebElement InvoiceGrid_lbl_ddlFirstItem;
        IWebElement InvoiceGrid_lbl_ddlSecondItem;
        IWebElement Dashboard_lnk_Settings;
        IWebElement Dashboard_lnk_SubMenuAccounting;
        IWebElement Accounting_txt_NewOpeningBalanceDate;
        IWebElement Accounting_lbl_CurrentOpeningBalanceDate;
        IWebElement Accounting_btn_Save;
        IWebElement Accounting_btn_Cancel;
        IWebElement EnterExpense_ddl_ExpenseFrom;
        IWebElement EnterExpense_txt_Notes;
        IWebElement EnterExpense_btn_Save;
        IWebElement EnterExpense_btn_SaveNAdd;
        IWebElement ExpenseFrom_lbl_ddlFirstItem;
        IWebElement ExpenseGrid_td_Row1ItemCell;
        IWebElement ExpenseGrid_ddl_Col1ItemCell;
        IWebElement ExpenseGrid_lbl_ddlFirstItem;
        IWebElement ExpenseGrid_td_Row2ItemCell;
        IWebElement ExpenseGrid_lbl_ddlSecondItem;
        IWebElement CreateCustReturn_ddl_ReturnFrom;
        IWebElement ReturnFrom_lbl_ddlFirstItem;
        IWebElement CreateCustReturn_txt_Notes;
        IWebElement CreateCustReturn_btn_Save;
        IWebElement CreateCustReturn_btn_SaveNAdd;
        IWebElement CreateCustReturn_btn_PreviewNPrint;
        IWebElement CreateCustReturn_btn_EmailToCust;
        
        
        public IWebDriver LoginWithValidData(IWebDriver driver)
        {
            _driver = driver;
            //Step-1
            Report.AddToHtmlReport("Step-1: Enter the URL in browser address bar.", false, true);
            _driver = Browser.OpenWithSelectedBrowser(_driver, strProjUrl, true);
            _driver = LogoutIfAlreadyLogin(_driver);

            //Step-2
            Report.AddToHtmlReport("Step-2: Enter valid Email Address, Password and click on 'Log in' button.", false, true);
            fun_EnterLoginData("panchdev", "panchdev");

            //Step-3
            Report.AddToHtmlReport("Step-3: Click on 'Logout' button on CloudDashbard page.", false, true);
            fun_DoLogout(_driver);

            return _driver;
        }

        public IWebDriver LoginWithValidDataWOlogout(IWebDriver driver, string strEmail="", string strPassword="")
        {
            _driver = driver;
            //Step-1
            Report.AddToHtmlReport("Step-1: Enter the URL in browser address bar.", false, true);
            _driver = Browser.OpenWithSelectedBrowser(_driver, strProjUrl, true);

            //Step-2
            Report.AddToHtmlReport("Step-2: Enter valid Email Address, Password and click on login button.", false, true);
            
            _driver = LogoutIfAlreadyLogin(_driver);
            if (strEmail != "" && strPassword != "") fun_EnterLoginData(strEmail, strPassword);
            else fun_EnterLoginData("panchdev", "panchdev");

            return _driver;
        }

        public IWebDriver LoginWithBlankData(IWebDriver driver)
        {
            _driver = driver;
            //Step-1
            Report.AddToHtmlReport("Step-1: Enter the URL in browser address bar.", false, true);
            _driver = Browser.OpenWithSelectedBrowser(_driver, strProjUrl, true);
            _driver = LogoutIfAlreadyLogin(_driver);

            //Step-2
            Report.AddToHtmlReport("Step-2: Enter blank Email Address and click on login button.", false, true);

            fun_EnterLoginData("", "");

            //Step-3
            Report.AddToHtmlReport("Step-3: Enter blank Password and click on login button.", false, true);
            fun_EnterLoginData("lokesh.jain@mailinator.com", "");
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Login_lbl_ErrEnterPwd));
                Login_lbl_ErrEnterPwd = _driver.FindElement(By.XPath(ElementLocators.Login_lbl_ErrEnterPwd));
                Report.AddToHtmlReportPassed("Error message text 'Enter your password.' for blank password.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "Error message text 'Enter your password.' for blank password.");
            }
            

            return _driver;
        }

        public IWebDriver LogoutIfAlreadyLogin(IWebDriver driver)
        {
            _driver = driver;
            try
            {
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(_driver).pause(5000);
            }
            catch (Exception){}
            return _driver;
        }

        public IWebDriver CreateInvoice(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Create invoice page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/VerificationProxy?request=/JSP/SalePurchase/EnterSale/EnterSale.jsp&type=sale&journalSysId=");
            /* commented to redirect to page via static URL 
            Actions actions = new Actions(_driver);
            actions.MoveToElement(Dashboard_lnk_MenuInvoices).Build().Perform();
            //actions.MoveToElement(Dashboard_lnk_MenuInvoices).Build().Perform();
            new Common(_driver).pause(4000);
            try
            {
                //new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuCreateInvoice));
                Dashboard_lnk_SubMenuCreateInvoice = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuCreateInvoice));
                actions.MoveToElement(Dashboard_lnk_MenuInvoices).MoveToElement(Dashboard_lnk_SubMenuCreateInvoice).Click().Build().Perform();
                Report.AddToHtmlReportPassed("'Create invoice' submenu button under Invoices menu on Dashboard Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Create invoice' submenu button under Invoices menu on Dashboard Page.");
            }*/
            
            InitCreateInvoicePageElements();
            //read excel
            DataTable dt = null;
            lock (this)
            {
                dt = Common.ExcelReadData("select * from [Invoices$] ", strRegDataExcelFilePath); 
            }
            string strCustomer = string.Empty, strItemCode = string.Empty, strQty = string.Empty, strNote = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InitCreateInvoicePageElements(false);
                new Common(_driver).pause(3000);
                strCustomer = Convert.ToString(dt.Rows[i][1]);
                strItemCode = Convert.ToString(dt.Rows[i][4]);
                strQty = Convert.ToString(dt.Rows[i][6]);
                
                //Step-4
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Select customer from 'Invoice to' dropdownlist.", false, true);
                CreateInvoice_ddl_InvoiceTo.Click();
                //new Common(_driver).pause(1000);

                try
                {
                    string strCustXpath = "//div[contains(@class,'typeahead')]/table/tbody/tr[2]/td[1]"; //"//div[contains(@class,'typeahead')]/table/tbody/tr[" + (i+1) +"]/td[1]";
                    //new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceTo_lbl_ddlFirstItem));
                    new Common(_driver).waitForElement(By.XPath(strCustXpath));
                    InvoiceTo_lbl_ddlFirstItem = _driver.FindElement(By.XPath(strCustXpath));
                    new Common(_driver).pause(1000);
                    Report.AddToHtmlReportPassed("'" + strCustomer + "' customer name under Invoice to dropdownlist.");
                    InvoiceTo_lbl_ddlFirstItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strCustomer + "' customer name under Invoice to dropdownlist.");
                }
                //new Common(_driver).pause(2000);

                //Step-5
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter record in grid.", false, true);
                try
                {
                    //new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_td_Row1ItemCell));
                    //InvoiceGrid_td_Row1ItemCell = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_td_Row1ItemCell));
                    //InvoiceGrid_td_Row1ItemCell.Click();
                    //new Common(_driver).pause(2000);

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_ddl_Col1ItemCell));
                    InvoiceGrid_ddl_Col1ItemCell = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_ddl_Col1ItemCell));
                    InvoiceGrid_ddl_Col1ItemCell.Click();
                    new Common(_driver).pause(1000);

                    string strItemXpath = "//table[@id='invoiceLinesTable']/tbody/tr[1]/td/div/div/input[@id='itemId']/../div[contains(@class,'typeahead')]/table/tbody/tr/td[contains(.,'" + strItemCode + "')]";
                    new Common(_driver).waitForElement(By.XPath(strItemXpath));
                    InvoiceGrid_lbl_ddlFirstItem = _driver.FindElement(By.XPath(strItemXpath));
                    Report.AddToHtmlReportPassed("'" + strItemCode + "' Item name in Item dropdownlist under row-1 cell-1.");

                    InvoiceGrid_lbl_ddlFirstItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strItemCode + "' Item name in Item dropdownlist under row-1 cell-1.");
                }
                //new Common(_driver).pause(2000);
                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_txt_Qty));
                    IWebElement InvoiceGrid_txt_Qty = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_txt_Qty));
                    InvoiceGrid_txt_Qty.SendKeys(Keys.Backspace);
                    Common.enterText(InvoiceGrid_txt_Qty, strQty);
                    //new Common(_driver).pause(2000);
                    //Report.AddToHtmlReportPassed("'" + strQty + "' Item name in Item dropdownlist under row-1 cell-1.");
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Qty' textbox in invoice grid.");
                }
                new Common(_driver).pause(1000);

                /*//Step-6
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter 2nd record in grid.", false, true);
                try
                {
                    //new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_td_Row2ItemCell));
                    //InvoiceGrid_td_Row2ItemCell = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_td_Row2ItemCell));
                    //InvoiceGrid_td_Row2ItemCell.Click();
                    //new Common(_driver).pause(2000);

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_ddl_Col1Row2ItemCell));
                    IWebElement InvoiceGrid_ddl_Col1Row2ItemCell = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_ddl_Col1Row2ItemCell));
                    InvoiceGrid_ddl_Col1Row2ItemCell.Click();
                    new Common(_driver).pause(2000);

                    //new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_lbl_ItemWR99));
                    //InvoiceGrid_lbl_ItemWR99 = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_lbl_ItemWR99));
                    //InvoiceGrid_lbl_ItemWR99.Click();
                    //Report.AddToHtmlReportPassed("'WR99' Item name in Item dropdownlist under row-1 cell-1.");

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_lbl_ddlSecondItem));
                    InvoiceGrid_lbl_ddlSecondItem = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_lbl_ddlSecondItem));
                    Report.AddToHtmlReportPassed("'" + InvoiceGrid_lbl_ddlSecondItem.Text + "' Item name in Item dropdownlist under row-1 cell-1.");

                    Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                    Report.AddToHtmlReport("Item code: " + InvoiceGrid_lbl_ddlSecondItem.Text + " <br>", false);

                    InvoiceGrid_lbl_ddlSecondItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + InvoiceGrid_lbl_ddlSecondItem.Text + "' Item name in Item dropdownlist under row-1 cell-1.");
                }
                new Common(_driver).pause(2000);*/

                //Step-7
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter random text in 'Notes' textbox.", false, true);
                CreateInvoice_txt_Notes.Click();
                CreateInvoice_txt_Notes.Clear();
                string strNotes = new Common(_driver).generateRandomChars(7) + " " + new Common(_driver).generateRandomChars(4);
                CreateInvoice_txt_Notes.SendKeys(strNotes);
                new Common(_driver).pause(1000);

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Customer: " + strCustomer, false);
                Report.AddToHtmlReport("Item code: " + strItemCode, false);
                Report.AddToHtmlReport("Qty: " + strQty, false);
                Report.AddToHtmlReport("Notes: " + strNotes + " <br>", false);

                //CreateInvoice_btn_Save.Click();
                CreateInvoice_btn_SaveNAdd.Click();
                new Common(_driver).pause(6000);
            }
            //fun_DoLogout(_driver, false);

            return _driver;
        }

        public IWebDriver CreateCustReturn(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Create returns page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/VerificationProxy?request=/JSP/SalePurchase/EnterSale/EnterSale.jsp&type=returnCredit&journalSysId=");
            

            InitCreateCustReturnPageElements();
            //read excel
            DataTable dt = null;
            lock (this)
            {
                dt = Common.ExcelReadData("select * from [CustomerReturn$] ", strRegDataExcelFilePath); 
            }
            string strCustomer = string.Empty, strItemCode = string.Empty, strDesc = string.Empty, strQty = string.Empty, strNote = string.Empty, strAllocateTo = string.Empty, strUnitPrice = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InitCreateCustReturnPageElements(false);
                new Common(_driver).pause(2000);
                strCustomer = Convert.ToString(dt.Rows[i][1]);
                strItemCode = Convert.ToString(dt.Rows[i][4]);
                strDesc = Convert.ToString(dt.Rows[i][5]);
                strQty = Convert.ToString(dt.Rows[i][6]);
                strAllocateTo = Convert.ToString(dt.Rows[i][7]);
                strUnitPrice = Convert.ToString(dt.Rows[i][8]);
                string strTaxRate = "GST";
                
                //Step-4
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Select customer from 'Return from' dropdownlist.", false, true);
                CreateCustReturn_ddl_ReturnFrom.Click();
                new Common(_driver).pause(500);

                try
                {
                    //string strCustXpath = "//td[contains(.,'" + strCustomer + "')]";
                    //new Common(_driver).waitForElement(By.XPath(ElementLocators.ReturnFrom_lbl_ddlFirstItem));
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.ReturnFrom_lbl_ddlFirstItem));
                    ReturnFrom_lbl_ddlFirstItem = _driver.FindElement(By.XPath(ElementLocators.ReturnFrom_lbl_ddlFirstItem));
                    new Common(_driver).pause(2000);
                    strCustomer = ReturnFrom_lbl_ddlFirstItem.Text;
                    Report.AddToHtmlReportPassed("'" + strCustomer + "' customer name under Return from dropdownlist.");
                    ReturnFrom_lbl_ddlFirstItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strCustomer + "' customer name under Return from dropdownlist.");
                }
                //new Common(_driver).pause(2000);

                //Step-5
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter record in grid.", false, true);
                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.CustReturnGrid_td_Row1ItemCell));
                    IWebElement CustReturnGrid_td_Row1ItemCell = _driver.FindElement(By.XPath(ElementLocators.CustReturnGrid_td_Row1ItemCell));
                    CustReturnGrid_td_Row1ItemCell.Click();
                    new Common(_driver).pause(1000);

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.CustReturnGrid_ddl_Col1ItemCell));
                    IWebElement CustReturnGrid_ddl_Col1ItemCell = _driver.FindElement(By.XPath(ElementLocators.CustReturnGrid_ddl_Col1ItemCell));
                    CustReturnGrid_ddl_Col1ItemCell.Click();
                    new Common(_driver).pause(1000);

                    string strItemXpath = "//td[contains(.,'" + strItemCode + "')]";
                    new Common(_driver).waitForElement(By.XPath(strItemXpath));
                    InvoiceGrid_lbl_ddlFirstItem = _driver.FindElement(By.XPath(strItemXpath));
                    Report.AddToHtmlReportPassed("'" + strItemCode + "' Item name in Item dropdownlist under row-1 cell-1.");

                    InvoiceGrid_lbl_ddlFirstItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strItemCode + "' Item name in Item dropdownlist under row-1 cell-1.");
                }
                //new Common(_driver).pause(2000);
                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.CustReturnGrid_td_Row1ItemCell2));
                    IWebElement CustReturnGrid_td_Row1ItemCell2 = _driver.FindElement(By.XPath(ElementLocators.CustReturnGrid_td_Row1ItemCell2));
                    CustReturnGrid_td_Row1ItemCell2.Click();
                    new Common(_driver).pause(1000);

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.CustReturnGrid_txt_Row1Item2));
                    IWebElement CustReturnGrid_txt_Row1Item2 = _driver.FindElement(By.XPath(ElementLocators.CustReturnGrid_txt_Row1Item2));
                    Common.enterText(CustReturnGrid_txt_Row1Item2, strDesc);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Description' textbox in Customer return grid.");
                }
                //new Common(_driver).pause(2000);

                string strTdGridDesc = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[2]";
                string strTdAllocateTo = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[3]";
                string strTdQty = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[4]";
                string strTdUnitPrice = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[5]";
                string strTdTaxRate = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[6]";

                //try
                //{
                //    Console.WriteLine("11");
                //    new Common(_driver).waitForElement(By.XPath(strTdGridDesc));
                //    Console.WriteLine("22");
                //    IWebElement GridDesc = _driver.FindElement(By.XPath(strTdGridDesc));
                //    Console.WriteLine("33");
                //    GridDesc.Click();
                //    Console.WriteLine("44");
                //    new Common(_driver).pause(2000);

                //    string strGridTxtDesc = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/textarea";
                //    new Common(_driver).waitForElement(By.XPath(strGridTxtDesc));
                //    Console.WriteLine("55");
                //    IWebElement GridTxtDesc = _driver.FindElement(By.XPath(strGridTxtDesc));
                //    Console.WriteLine("66");
                //    Common.enterText(GridTxtDesc, strDesc);
                //    Console.WriteLine("77");
                //    new Common(_driver).pause(1000);
                //    Console.WriteLine("88");
                //    GridTxtDesc.SendKeys(Keys.Tab);
                //    Console.WriteLine("99");
                //    new Common(_driver).pause(1000);
                //}
                //catch (Exception ex)
                //{
                //    Report.AddToHtmlReportFailed(_driver, ex, "'Description' textbox in Customer return grid.");
                //}
                //try
                //{
                //    //new Common(_driver).waitForElement(By.XPath(strGridAllocateTo));
                //    IWebElement GridCellAllocateTo = _driver.FindElement(By.XPath(strTdAllocateTo));
                //    GridCellAllocateTo.Click();
                //    new Common(_driver).pause(2000);

                //    string strGridComboHandle = "//input[contains(@class,'x-form-text x-form-field x-form-focus')]/../img";
                //    //new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                //    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                //    GridComboHandle.Click();
                //    //new Common(_driver).pause(2000);

                //    string strGridAllocateToItem = "//div[@id='account-list']/table/tbody/tr/td[contains(.,'" + strAllocateTo + "')]";
                //    //new Common(_driver).waitForElement(By.XPath(strGridAllocateToItem));
                //    IWebElement GridAllocateToItem = _driver.FindElement(By.XPath(strGridAllocateToItem));
                //    ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].click();", GridAllocateToItem);
                //    //GridAllocateToItem.Click();
                //    new Common(_driver).pause(2000);
                //    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                //}
                //catch (Exception ex)
                //{
                //    Report.AddToHtmlReportFailed(_driver, ex, "'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Customer return grid.");
                //}
                try
                {
                    new Common(_driver).waitForElement(By.XPath(strTdQty));
                    IWebElement GridQty = _driver.FindElement(By.XPath(strTdQty));
                    GridQty.Click();
                    new Common(_driver).pause(1000);

                    string strGridTxtQty = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/input[@type='text']";
                    new Common(_driver).waitForElement(By.XPath(strGridTxtQty));
                    IWebElement GridTxtQty = _driver.FindElement(By.XPath(strGridTxtQty));
                    Common.enterText(GridTxtQty, strQty);
                    //new Common(_driver).pause(1000);
                    //GridTxtQty.SendKeys(Keys.Tab);
                    //new Common(_driver).pause(1000);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Qty' textbox in Customer return grid.");
                }
                try
                {
                    new Common(_driver).waitForElement(By.XPath(strTdUnitPrice));
                    IWebElement GridUnitPrice = _driver.FindElement(By.XPath(strTdUnitPrice));
                    GridUnitPrice.Click();
                    new Common(_driver).pause(1000);

                    //Actions builder = new Actions(_driver);
                    ////builder.KeyDown(Keys.Control).Build().Perform();
                    ////builder.SendKeys("a").Build().Perform();
                    ////builder.KeyUp(Keys.Control).Build().Perform();
                    //builder.SendKeys(Keys.Backspace).SendKeys(strUnitPrice).Build().Perform();
                    ////builder.SendKeys(strUnitPrice).Build().Perform();


                    string strGridTxtUnitPrice = "//div[contains(@style,'visible')]/input[@type='text'][contains(@class,'num-field')]"; //"//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/input[@type='text']";
                    //new Common(_driver).waitForElement(By.XPath(strGridTxtUnitPrice));
                    
                    IWebElement GridTxtUnitPrice = _driver.FindElement(By.XPath(strGridTxtUnitPrice));
                    new Common(_driver).pause(1000);
                    //GridTxtUnitPrice.SendKeys(Keys.Backspace);
                    //Console.WriteLine("FF11");
                    //GridTxtUnitPrice.Click();
                    //GridTxtUnitPrice.Clear();
                    //GridTxtUnitPrice.SendKeys("22");
                    Common.enterText(GridTxtUnitPrice, strUnitPrice);
                    
                    new Common(_driver).pause(1000);
                    GridTxtUnitPrice.SendKeys(Keys.Tab);
                    //new Common(_driver).pause(1000);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Unit price' textbox in Customer return grid.");
                }

                //try
                //{
                //    //new Common(_driver).waitForElement(By.XPath(strGridTaxRate));
                //    IWebElement GridCellTaxRate = _driver.FindElement(By.XPath(strTdTaxRate));
                //    GridCellTaxRate.Click();
                //    new Common(_driver).pause(2000);

                //    string strGridComboHandle = "//input[contains(@class,'x-form-text x-form-field x-form-focus')]/following-sibling::img";
                //    //new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                //    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                //    GridComboHandle.Click();
                //    //new Common(_driver).pause(2000);

                //    string strGridTaxRateItem = "//tr[contains(@class,'x-combo-list-item')]/td[1][contains(.,'" + strTaxRate + "')]";
                //    //new Common(_driver).waitForElement(By.XPath(strGridTaxRateItem));
                //    IWebElement GridTaxRateItem = _driver.FindElement(By.XPath(strGridTaxRateItem));
                //    ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].click();", GridTaxRateItem);
                //    //GridTaxRateItem.Click();
                //    new Common(_driver).pause(2000);
                //    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                //}
                //catch (Exception ex)
                //{
                //    Report.AddToHtmlReportFailed(_driver, ex, "'" + strTaxRate + "' Item name in Tax rate dropdownlist in Customer return grid.");
                //}

                //Step-7
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter random text in 'Notes' textbox.", false, true);
                CreateCustReturn_txt_Notes.Click();
                CreateCustReturn_txt_Notes.Clear();
                string strNotes = new Common(_driver).generateRandomChars(7) + " " + new Common(_driver).generateRandomChars(4);
                CreateCustReturn_txt_Notes.SendKeys(strNotes);
                new Common(_driver).pause(1000);

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Customer: " + strCustomer, false);
                Report.AddToHtmlReport("Description: " + strDesc, false);
                Report.AddToHtmlReport("Allocate to: " + strAllocateTo, false);
                Report.AddToHtmlReport("Qty: " + strQty, false);
                Report.AddToHtmlReport("Unit price: " + strUnitPrice, false);
                Report.AddToHtmlReport("Notes: " + strNotes + " <br>", false);

                //CreateCustReturn_btn_Save.Click();
                CreateCustReturn_btn_SaveNAdd.Click();
                new Common(_driver).pause(4000);
            }
            //fun_DoLogout(_driver, false);

            return _driver;
        }

        private void InitCreateCustReturnPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.CreateCustReturn_ddl_ReturnFrom));
                CreateCustReturn_ddl_ReturnFrom = _driver.FindElement(By.XPath(ElementLocators.CreateCustReturn_ddl_ReturnFrom));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Return from' dropdownlist on Returns Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Return from' dropdownlist on Returns Page.");
            }

            try
            {
                CreateCustReturn_txt_Notes = _driver.FindElement(By.Id(ElementLocators.CreateCustReturn_txt_Notes));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Notes' textbox in Returns Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Notes' textbox in Returns Page.");
            }
            try
            {
                CreateCustReturn_btn_Save = _driver.FindElement(By.Id(ElementLocators.CreateCustReturn_btn_Save));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save' button on Returns Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Save' button on Returns Page.");
            }
            try
            {
                CreateCustReturn_btn_SaveNAdd = _driver.FindElement(By.Id(ElementLocators.CreateCustReturn_btn_SaveNAdd));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save & add another' button on Returns Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Save & add another' button on Returns Page.");
            }
            try
            {
                CreateCustReturn_btn_PreviewNPrint = _driver.FindElement(By.XPath(ElementLocators.CreateCustReturn_btn_PreviewNPrint));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Preview & print' button on Returns Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Preview & print' button on Returns Page.");
            }
            try
            {
                CreateCustReturn_btn_EmailToCust = _driver.FindElement(By.XPath(ElementLocators.CreateCustReturn_btn_EmailToCust));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Email to customer' button on Returns Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Email to customer' button on Returns Page.");
            }
        }

        public IWebDriver UserRegistration(IWebDriver driver, DataTable dt)
        {
            _driver = driver;
            int intRecCnt = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToString(dt.Rows[i][8]).ToLower().Contains("pending"))
                {
                    //Step-1
                    Report.AddToHtmlReport("Step-1: Enter the URL in browser address bar.", false, true);
                    _driver = Browser.OpenWithSelectedBrowser(_driver, strSignupUrl, true);

                    InitRegPageElements();
                    //Step-2
                    Report.AddToHtmlReport("Step-2: Enter registration details.", false, true);

                    string strFName = string.Empty, strSname = string.Empty, strEmail = string.Empty, strPhoneNo = string.Empty, strUserName = string.Empty, strPwd = string.Empty, strBusinessName = string.Empty, strEmailChange = string.Empty;
                    //DataTable dt = Common.ExcelReadData("select * from [Sheet1$]", strRegDataExcelFilePath);

                    strFName = Convert.ToString(dt.Rows[i][0]);
                    strSname = Convert.ToString(dt.Rows[i][1]);
                    strEmail = Convert.ToString(dt.Rows[i][2]);
                    strPhoneNo = Convert.ToString(dt.Rows[i][3]);
                    strUserName = Convert.ToString(dt.Rows[i][4]);
                    strPwd = Convert.ToString(dt.Rows[i][5]);
                    strBusinessName = Convert.ToString(dt.Rows[i][6]);
                    strEmailChange = Convert.ToString(dt.Rows[i][7]);
                    Common.enterText(Reg_txt_FirstName, strFName);
                    Common.enterText(Reg_txt_SurName, strSname);
                    Common.enterText(Reg_txt_Email, strEmail);
                    try
                    {
                        Common.enterText(Reg_txt_PhoneNo, strPhoneNo);
                    }
                    catch (Exception) { }
                    Common.enterText(Reg_txt_UserName, strUserName);
                    Reg_txt_UserName.SendKeys(Keys.Tab);
                    try
                    {
                        new Common(_driver).pause(5000);
                        IWebElement lblUserAvailable = _driver.FindElement(By.XPath(ElementLocators.Reg_lbl_MsgUserNameAvailability));
                        Report.AddToHtmlReportFailed(_driver, new Exception("This user name is not available."), "Username entered '" + strUserName + "', Skipped to next record.");
                        intRecCnt = Common.ExcelWriteData("UPDATE [Signup$] SET [status]='Skipped' where [first name] = '" + strFName + "' and [last name] = '" + strSname + "' ", strRegDataExcelFilePath);
                        if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
                        {
                            Browser.CloseBrowser(_driver);
                        }
                        continue;
                    }
                    catch (Exception) { }

                    Common.enterText(Reg_txt_Password, strPwd);
                    Common.enterText(Reg_txt_ConfPassword, strPwd);
                    Common.enterText(Reg_txt_BusinessName, strBusinessName);

                    try
                    {
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.Reg_txt_Captcha));
                        IWebElement Reg_txt_Captcha = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_Captcha));
                        Common.enterText(Reg_txt_Captcha, "lqcCP66Ou3DqVfCDjRmO");
                    }
                    catch (Exception) { }

                    Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                    Report.AddToHtmlReport("First name: " + strFName, false);
                    Report.AddToHtmlReport("Last name: " + strSname, false);
                    Report.AddToHtmlReport("Email: " + strEmail, false);
                    Report.AddToHtmlReport("Phone: " + strPhoneNo, false);
                    Report.AddToHtmlReport("User name: " + strUserName, false);
                    Report.AddToHtmlReport("Password: " + strPwd, false);
                    Report.AddToHtmlReport("Business name: " + strBusinessName + "<br>", false);
                    Reg_btn_Save.Click();

                    intRecCnt = Common.ExcelWriteData("UPDATE [Signup$] SET [status]='Done' where [first name] = '" + strFName + "' and [last name] = '" + strSname + "' ", strRegDataExcelFilePath);

                    InitRegDonePageElements();

                    //Step-3
                    Report.AddToHtmlReport("Step-3: Check for registration confirmation Email.", false, true);
                    checkConfiormationEmail(strEmail.Replace("@", "."));

                    //Step-5
                    Report.AddToHtmlReport("Step-5: Check for Activation confiramtion message.", false, true);
                    try
                    {
                        IWebElement RegDone_lbl_Congratulations;
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.RegDone_lbl_Congratulations));
                        RegDone_lbl_Congratulations = _driver.FindElement(By.XPath(ElementLocators.RegDone_lbl_Congratulations));
                        Report.AddToHtmlReportPassed("Header text 'Congratulations' verification after clicking on activation link in email.");
                    }
                    catch (Exception ex)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "Header text 'Congratulations' verification after clicking on activation link in email.");
                    }

                    //Step-6
                    Report.AddToHtmlReport("Step-6: Navigate to login URL.", false, true);
                    _driver.Navigate().GoToUrl(strProjUrl);
                    Report.AddToHtmlReportPassed("Redirected to '" + strProjUrl + "' URL.");
                    new Common(_driver).pause(10000);

                    //Step-7
                    Report.AddToHtmlReport("Step-7: Enter valid User name, Password and click on login button.", false, true);
                    fun_EnterLoginData(strUserName, strPwd);
                    try
                    {
                        IWebElement DashboardAdvi_lnk_HeaderMenuUsers;
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.DashboardAdvi_lnk_HeaderMenuUsers));
                        DashboardAdvi_lnk_HeaderMenuUsers = _driver.FindElement(By.XPath(ElementLocators.DashboardAdvi_lnk_HeaderMenuUsers));
                        Report.AddToHtmlReportPassed("'Users' header menu on Dashboard page.");
                        DashboardAdvi_lnk_HeaderMenuUsers.Click();
                    }
                    catch (Exception ex)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "'Users' header menu on Dashboard page.");
                    }

                    //Step-8
                    Report.AddToHtmlReport("Step-8: Click on the grid row to edit.", false, true);
                    try
                    {
                        IWebElement UserRow_lnk_UserName;
                        string strXpathUserRow = "//table[@class='MasterTable_Web20']/tbody/tr/td[contains(.,'" + strUserName + "')]";
                        new Common(_driver).waitForElement(By.XPath(strXpathUserRow));
                        UserRow_lnk_UserName = _driver.FindElement(By.XPath(strXpathUserRow));
                        Report.AddToHtmlReportPassed("Grid row with '" + strUserName + "' Username.");
                        UserRow_lnk_UserName.Click();
                    }
                    catch (Exception ex)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "Grid row with '" + strUserName + "' Username.");
                    }

                    //Step-9
                    Report.AddToHtmlReport("Step-9: Click on 'Edit user' button.", false, true);
                    try
                    {
                        IWebElement btnEditUser;
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.DashboardAdvi_lnk_EditUser));
                        btnEditUser = _driver.FindElement(By.XPath(ElementLocators.DashboardAdvi_lnk_EditUser));
                        Report.AddToHtmlReportPassed("'Edit user' button on Users page.");
                        btnEditUser.Click();
                    }
                    catch (Exception ex)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "'Edit user' button on Users page.");
                    }
                    new Common(_driver).pause(10000);
                    _driver = _driver.SwitchTo().Frame("AddEditUserWindow");

                    //Step-10
                    Report.AddToHtmlReport("Step-10: Edit 'Email Address' and click on 'Save' button on Edit user popup panel.", false, true);
                    try
                    {
                        IWebElement DashboardAdvi_txt_EditEmail;
                        new Common(_driver).waitForElement(By.Id(ElementLocators.DashboardAdvi_txt_EditEmail));
                        DashboardAdvi_txt_EditEmail = _driver.FindElement(By.Id(ElementLocators.DashboardAdvi_txt_EditEmail));
                        Report.AddToHtmlReportPassed("'Email Address' textbox on Edit user popup panel.");
                        Common.enterText(DashboardAdvi_txt_EditEmail, strEmailChange);
                    }
                    catch (Exception ex)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "'Email Address' textbox on Edit user popup panel.");
                    }
                    try
                    {
                        IWebElement DashboardAdvi_btn_EditSave;
                        DashboardAdvi_btn_EditSave = _driver.FindElement(By.Id(ElementLocators.DashboardAdvi_btn_EditSave));
                        Report.AddToHtmlReportPassed("'Save' button on Edit user popup panel.");
                        DashboardAdvi_btn_EditSave.Click();
                    }
                    catch (Exception ex)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "'Save' button textbox on Edit user popup panel.");
                    }

                    Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                    Report.AddToHtmlReport("Email Address: " + strEmailChange + "<br>", false);

                    new Common(_driver).pause(5000);
                    fun_DoLogout(_driver);
                    Report.AddToHtmlReport("<hr>");

                    if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
                    {
                        Browser.CloseBrowser(_driver);
                    }

                    //break; //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Remove before deployment @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                }
                else continue;
            }

            return _driver;
        }

        //public IWebDriver SignUpWithAllDemoBusiness(IWebDriver driver, DataTable dtDemoBusi)
        public IWebDriver SignUpWithAllDemoBusiness(IWebDriver driver, string strURL, string strUserName, string strPwd)
        {
           _driver = driver;
            
            ////DataTable dtDemoBusi = Common.ExcelReadData("select * from [DemoBusiness$]", strRegDataExcelFilePath);
            /*string strURL = string.Empty, strUserName = string.Empty, strPwd = string.Empty;
            strURL = Convert.ToString(dtDemoBusi.Rows[0][0]);
            strUserName = Convert.ToString(dtDemoBusi.Rows[0][1]);
            strPwd = Convert.ToString(dtDemoBusi.Rows[0][2]); */

            //Step-1
            Report.AddToHtmlReport("Step-1: Enter the URL in browser address bar.", false, true);
            _driver = Browser.OpenWithSelectedBrowser(_driver, strURL, true);

            //Step-2
            Report.AddToHtmlReport("Step-2: Enter valid User Name, Password and click on login button.", false, true);
            
            //_driver = LogoutIfAlreadyLogin(_driver);
            fun_EnterLoginData(strUserName, strPwd);

            DataTable dtSetupAssi = Common.ExcelReadData("select * from [SetupAssistant$]", strRegDataExcelFilePath, false);
            string strBusinessName = string.Empty, strTradingName = string.Empty, strPhoneNumber = string.Empty, strEmail = string.Empty, strABN = string.Empty, strAddress = string.Empty, strCity = string.Empty, strState = string.Empty, strPostCode = string.Empty, strRegisteredForGST = string.Empty;
            strBusinessName = Convert.ToString(dtSetupAssi.Rows[0][1]);
            strTradingName = Convert.ToString(dtSetupAssi.Rows[1][1]);
            strPhoneNumber = Convert.ToString(dtSetupAssi.Rows[2][1]);
            strEmail = Convert.ToString(dtSetupAssi.Rows[3][1]);
            strABN = Convert.ToString(dtSetupAssi.Rows[4][1]);
            strAddress = Convert.ToString(dtSetupAssi.Rows[5][1]);
            strCity = Convert.ToString(dtSetupAssi.Rows[6][1]);
            strState = Convert.ToString(dtSetupAssi.Rows[7][1]);
            strPostCode = Convert.ToString(dtSetupAssi.Rows[8][1]);
            strRegisteredForGST = Convert.ToString(dtSetupAssi.Rows[9][1]);

            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.SetupAssi_lbl_SetupAssistantPopup));
                IWebElement SetupAssi_lbl_SetupAssistantPopup = _driver.FindElement(By.XPath(ElementLocators.SetupAssi_lbl_SetupAssistantPopup));
                //Report.AddToHtmlReportPassed("'Setup assistant' popup panel.");
                TestCases.Login.IsSetupAssiPopupOpen = true;
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Setup assistant' popup panel, Skipping current business and continue with next business.");
                TestCases.Login.IsSetupAssiPopupOpen = false;
            }
            if (TestCases.Login.IsSetupAssiPopupOpen == true)
            {
                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.SetupAssi_btn_Next));
                    SetupAssi_btn_Next = _driver.FindElement(By.XPath(ElementLocators.SetupAssi_btn_Next));
                    Report.AddToHtmlReportPassed("'Next' button on Setup Assistant popup panel.");
                    SetupAssi_btn_Next.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Next' button on Setup Assistant popup panel.");
                }

                InitSetupAssistantPopupElements();
                Common.enterText(SetupAssi_txt_BusinessName, strBusinessName);
                Common.enterText(SetupAssi_txt_TradingName, strTradingName);
                Common.enterText(SetupAssi_txt_PhoneNumber, strPhoneNumber);
                Common.enterText(SetupAssi_txt_Email, strEmail);
                if (strRegisteredForGST.ToLower() == "yes")
                {
                    if (SetupAssi_chk_RegisteredForGST.GetAttribute("checked") == null) SetupAssi_chk_RegisteredForGST.Click();
                }
                else if (strRegisteredForGST.ToLower() == "no")
                {
                    if (SetupAssi_chk_RegisteredForGST.GetAttribute("checked") != null) SetupAssi_chk_RegisteredForGST.Click();
                }
                Common.enterText(SetupAssi_txt_ABN, strABN);
                Common.enterText(SetupAssi_txt_BusinessAddr, strAddress);
                Common.enterText(SetupAssi_txt_BusinessCity, strCity);
                Common.enterText(SetupAssi_txt_BusinessState, strState);
                Common.enterText(SetupAssi_txt_BusinessPostCode, strPostCode);

                if (SetupAssi_chk_SameAsBusinessAddr.GetAttribute("checked") == null) SetupAssi_chk_SameAsBusinessAddr.Click();

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Business name: " + strBusinessName, false);
                Report.AddToHtmlReport("Trading name: " + strTradingName, false);
                Report.AddToHtmlReport("Phone number: " + strPhoneNumber, false);
                Report.AddToHtmlReport("Email address: " + strEmail, false);
                Report.AddToHtmlReport("ABN: " + strABN, false);
                Report.AddToHtmlReport("Business Address: " + strAddress, false);
                Report.AddToHtmlReport("Business City: " + strCity, false);
                Report.AddToHtmlReport("Business State: " + strState, false);
                Report.AddToHtmlReport("Business Postcode: " + strPostCode, false);
                Report.AddToHtmlReport("Registered for GST: " + strRegisteredForGST + "<br>", false);

                new Common(_driver).pause(1000);
                SetupAssi_btn_Next.Click();

                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.SetupAssi_btn_Finish));
                    SetupAssi_btn_Finish = _driver.FindElement(By.XPath(ElementLocators.SetupAssi_btn_Finish));
                    Report.AddToHtmlReportPassed("'Finish' button on Setup Assistant popup panel.");
                    SetupAssi_btn_Finish.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Finish' button on Setup Assistant popup panel.");
                }
                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.SetupAssi_btn_StartUsingLiveAccounts));
                    SetupAssi_btn_StartUsingLiveAccounts = _driver.FindElement(By.XPath(ElementLocators.SetupAssi_btn_StartUsingLiveAccounts));
                    Report.AddToHtmlReportPassed("'Start using LiveAccounts' button on Setup Assistant popup panel.");
                    SetupAssi_btn_StartUsingLiveAccounts.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Start using LiveAccounts' button on Setup Assistant popup panel.");
                }


                new Common(_driver).pause(2000);

                ChangeOpBalanceDate();
            }

            return _driver;
        }

        public IWebDriver EnterExpense(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Enter expense page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/VerificationProxy?request=/JSP/SalePurchase/EnterSale/EnterSale.jsp&type=purchase&journalSysId=");
            

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Expense page and click on 'Save & add another' button.", false, true);
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [Expenses$] ", strRegDataExcelFilePath);
            string strNumber = string.Empty, strSupplier = string.Empty, strDtReceived = string.Empty, strDtDue = string.Empty, strTotalAmt = string.Empty, strAmountDue = string.Empty, strStatus = string.Empty, strDesc = string.Empty, strAllocateTo = string.Empty, strItem = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0) InitEnterExpensePageElements();
                else InitEnterExpensePageElements(false);
                new Common(_driver).pause(2000);
                strNumber = Convert.ToString(dt.Rows[i][0]);
                strSupplier = Convert.ToString(dt.Rows[i][1]);
                strDtReceived = Convert.ToString(dt.Rows[i][2]);
                strDtDue = Convert.ToString(dt.Rows[i][3]);
                strStatus = Convert.ToString(dt.Rows[i][4]);
                strItem = Convert.ToString(dt.Rows[i][5]);
                //strTotalAmt = Convert.ToString(dt.Rows[i][4]);
                //strAmountDue = Convert.ToString(dt.Rows[i][5]);
                //strStatus = Convert.ToString(dt.Rows[i][6]);
                //strDesc = Convert.ToString(dt.Rows[i][7]);
                //strAllocateTo = Convert.ToString(dt.Rows[i][8]);
                //string strTaxRate = "GST";


                EnterExpense_ddl_ExpenseFrom.Click();
                //new Common(_driver).pause(2000);
                try
                {
                    new Common(_driver).pause(1000);
                    string strSupplierItem = "//td[contains(.,'" + strSupplier + "')]";
                    IWebElement EnterExpense_ddl_SupplierItem = _driver.FindElement(By.XPath(strSupplierItem));
                    EnterExpense_ddl_SupplierItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strSupplier + "' Item name in 'Expense from' combobox on Enter expense Page.");
                }
                //new Common(_driver).pause(1000);

                //Step-5
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter record in grid.", false, true);
                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.InvoiceGrid_td_Row1ItemCell));
                    InvoiceGrid_td_Row1ItemCell = _driver.FindElement(By.XPath(ElementLocators.InvoiceGrid_td_Row1ItemCell));
                    InvoiceGrid_td_Row1ItemCell.Click();
                    new Common(_driver).pause(1000);

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.ExpenseGrid_ddl_Col1ItemCell));
                    ExpenseGrid_ddl_Col1ItemCell = _driver.FindElement(By.XPath(ElementLocators.ExpenseGrid_ddl_Col1ItemCell));
                    ExpenseGrid_ddl_Col1ItemCell.Click();
                    new Common(_driver).pause(1000);

                    string strItemXpath = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr/td[1][contains(.,'" + strItem + "')]";
                    new Common(_driver).waitForElement(By.XPath(strItemXpath));
                    InvoiceGrid_lbl_ddlFirstItem = _driver.FindElement(By.XPath(strItemXpath));
                    Report.AddToHtmlReportPassed("'" + strItem + "' Item name in Item dropdownlist under row-1 cell-1.");

                    InvoiceGrid_lbl_ddlFirstItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strItem + "' Item name in Item dropdownlist under row-1 cell-1.");
                }
                //new Common(_driver).pause(2000);

                
                //////string strTdGridDesc = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[2]";
                //////string strTdAllocateTo = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[3]";
                //////string strTdUnitPrice = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[5]";
                //////string strTdTaxRate = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[6]";

                //////try
                //////{
                //////    new Common(_driver).waitForElement(By.XPath(strTdGridDesc));
                //////    IWebElement GridDesc = _driver.FindElement(By.XPath(strTdGridDesc));
                //////    GridDesc.Click();
                //////    new Common(_driver).pause(2000);

                //////    string strGridTxtDesc = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/textarea";
                //////    new Common(_driver).waitForElement(By.XPath(strGridTxtDesc));
                //////    IWebElement GridTxtDesc = _driver.FindElement(By.XPath(strGridTxtDesc));
                //////    Common.enterText(GridTxtDesc, strDesc);
                //////    new Common(_driver).pause(1000);
                //////    GridTxtDesc.SendKeys(Keys.Tab);
                //////    new Common(_driver).pause(1000);
                //////}
                //////catch (Exception ex)
                //////{
                //////    Report.AddToHtmlReportFailed(_driver, ex, "'Description' textbox in Enter expense grid.");
                //////}
                //////try
                //////{
                //////    //new Common(_driver).waitForElement(By.XPath(strGridAllocateTo));
                //////    IWebElement GridCellAllocateTo = _driver.FindElement(By.XPath(strTdAllocateTo));
                //////    GridCellAllocateTo.Click();
                //////    new Common(_driver).pause(2000);

                //////    string strGridComboHandle = "//input[contains(@class,'x-form-text x-form-field x-form-focus')]/following-sibling::img";
                //////    //new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                //////    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                //////    GridComboHandle.Click();
                //////    //new Common(_driver).pause(2000);

                //////    string strGridAllocateToItem = "//div[@id='account-list']/table/tbody/tr/td[contains(.,'" + strAllocateTo + "')]";
                //////    //new Common(_driver).waitForElement(By.XPath(strGridAllocateToItem));
                //////    IWebElement GridAllocateToItem = _driver.FindElement(By.XPath(strGridAllocateToItem));
                //////    ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].click();", GridAllocateToItem);
                //////    //GridAllocateToItem.Click();
                //////    new Common(_driver).pause(2000);
                //////    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                //////}
                //////catch (Exception ex)
                //////{
                //////    Report.AddToHtmlReportFailed(_driver, ex, "'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Enter expense grid.");
                //////}
                //////try
                //////{
                //////    new Common(_driver).waitForElement(By.XPath(strTdUnitPrice));
                //////    IWebElement GridUnitPrice = _driver.FindElement(By.XPath(strTdUnitPrice));
                //////    GridUnitPrice.Click();
                //////    new Common(_driver).pause(2000);

                //////    string strGridTxtUnitPrice = "//div[@id='invoiceGridDiv']/div/div/div/div/div/div/div/input[@type='text']";
                //////    new Common(_driver).waitForElement(By.XPath(strGridTxtUnitPrice));
                //////    IWebElement GridTxtUnitPrice = _driver.FindElement(By.XPath(strGridTxtUnitPrice));
                //////    Common.enterText(GridTxtUnitPrice, strTotalAmt);
                //////    new Common(_driver).pause(1000);
                //////    GridTxtUnitPrice.SendKeys(Keys.Tab);
                //////    new Common(_driver).pause(1000);
                //////}
                //////catch (Exception ex)
                //////{
                //////    Report.AddToHtmlReportFailed(_driver, ex, "'Unit price' textbox in Enter expense grid.");
                //////}

                //////try
                //////{
                //////    //new Common(_driver).waitForElement(By.XPath(strGridTaxRate));
                //////    IWebElement GridCellTaxRate = _driver.FindElement(By.XPath(strTdTaxRate));
                //////    GridCellTaxRate.Click();
                //////    new Common(_driver).pause(2000);

                //////    string strGridComboHandle = "//input[contains(@class,'x-form-text x-form-field x-form-focus')]/following-sibling::img";
                //////    //new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                //////    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                //////    GridComboHandle.Click();
                //////    //new Common(_driver).pause(2000);
                    
                //////    string strGridTaxRateItem = "//tr[contains(@class,'x-combo-list-item')]/td[1][contains(.,'" + strTaxRate + "')]";
                //////    //new Common(_driver).waitForElement(By.XPath(strGridTaxRateItem));
                //////    IWebElement GridTaxRateItem = _driver.FindElement(By.XPath(strGridTaxRateItem));
                //////    ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].click();", GridTaxRateItem);
                //////    //GridTaxRateItem.Click();
                //////    new Common(_driver).pause(2000);
                //////    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                //////}
                //////catch (Exception ex)
                //////{
                //////    Report.AddToHtmlReportFailed(_driver, ex, "'" + strTaxRate + "' Item name in Allocate to dropdownlist in Enter expense grid.");
                //////}

                //Step-7
                Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter random text in 'Notes' textbox.", false, true);
                EnterExpense_txt_Notes.Click();
                EnterExpense_txt_Notes.Clear();
                string strNotes = new Common(_driver).generateRandomChars(7) + " " + new Common(_driver).generateRandomChars(4);
                EnterExpense_txt_Notes.SendKeys(strNotes);
                //new Common(_driver).pause(2000);

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Supplier: " + strSupplier, false);
                Report.AddToHtmlReport("Item code: " + strItem, false);
                Report.AddToHtmlReport("Notes: " + strNotes + " <br>", false);

                //Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                //Report.AddToHtmlReport("Supplier: " + strSupplier, false);
                //Report.AddToHtmlReport("Description: " + strDesc, false);
                //Report.AddToHtmlReport("Allocate to: " + strAllocateTo, false);
                //Report.AddToHtmlReport("Unit price: " + strTotalAmt, false);
                //Report.AddToHtmlReport("Notes: " + strNotes + " <br>", false);
                Report.AddToHtmlReport("<hr>");

                EnterExpense_btn_SaveNAdd.Click();
                new Common(_driver).pause(4000);
            }
            //fun_DoLogout(_driver, false);

            return _driver;
        }

        private void InitEnterExpensePageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.EnterExpense_ddl_ExpenseFrom));
                EnterExpense_ddl_ExpenseFrom = _driver.FindElement(By.XPath(ElementLocators.EnterExpense_ddl_ExpenseFrom));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expense from' dropdownlist on Expense Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expense from' dropdownlist on Expense Page.");
            }

            try
            {
                EnterExpense_txt_Notes = _driver.FindElement(By.XPath(ElementLocators.EnterExpense_txt_Notes));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Notes' textbox in Expense Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Notes' textbox in Expense Page.");
            }
            try
            {
                EnterExpense_btn_Save = _driver.FindElement(By.XPath(ElementLocators.EnterExpense_btn_Save));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save' button on Expense Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save' button on Expense Page.");
            }
            try
            {
                EnterExpense_btn_SaveNAdd = _driver.FindElement(By.XPath(ElementLocators.EnterExpense_btn_SaveNAdd));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save & add another' button on Expense Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save & add another' button on Expense Page.");
            }
        }

        public void ChangeOpBalanceDate()
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_Settings));
                Dashboard_lnk_Settings = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_Settings));
                Report.AddToHtmlReportPassed("'Settings' menu text verification on top of page.");
                _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/VerificationProxy?request=/JSP/Accounts/Account.jsp&access=menu");
                /* commented to redirect to page via static URL 
                Actions actions = new Actions(_driver);
                actions.MoveToElement(Dashboard_lnk_Settings).Build().Perform();
                new Common(_driver).pause(4000);

                try
                {
                    Dashboard_lnk_SubMenuAccounting = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuAccounting));
                    actions.MoveToElement(Dashboard_lnk_Settings).MoveToElement(Dashboard_lnk_SubMenuAccounting).Click().Build().Perform();
                    Report.AddToHtmlReportPassed("'Accounting' submenu button under Settings menu.");
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Accounting' submenu button under Settings menu.");
                }*/
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Settings' menu text verification on top of page.");
            }
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Accounting_btn_EnterOpBalance));
                IWebElement Accounting_btn_EnterOpBalance = _driver.FindElement(By.XPath(ElementLocators.Accounting_btn_EnterOpBalance));
                new Common(_driver).pause(4000);
                TestCases.Login.strURLDynCode = Accounting_btn_EnterOpBalance.GetAttribute("href").Split('/')[6];
                //Console.WriteLine("TestCases.Login.strURLDynCode:: " + TestCases.Login.strURLDynCode);
                Report.AddToHtmlReportPassed("'Enter Opening Balances' button on Accounting Page.");
                Accounting_btn_EnterOpBalance.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Enter Opening Balances' button on Accounting Page.");
            }
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.Accounting_lnk_ChangeOpBalanceDate));
                IWebElement Accounting_lnk_ChangeOpBalanceDate = _driver.FindElement(By.Id(ElementLocators.Accounting_lnk_ChangeOpBalanceDate));
                
                //TestCases.Login.strURLDynCode = _driver.Url.Split('/')[6];

                new Common(_driver).pause(1000);
                Report.AddToHtmlReportPassed("'Change opening balance date' link button on Opening balances Page.");
                Accounting_lnk_ChangeOpBalanceDate.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Change opening balance date' link button on Opening balances Page.");
            }
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.Accounting_lbl_CurrentOpeningBalanceDate));
                Accounting_lbl_CurrentOpeningBalanceDate = _driver.FindElement(By.Id(ElementLocators.Accounting_lbl_CurrentOpeningBalanceDate));
                new Common(_driver).pause(2000);
                Report.AddToHtmlReportPassed("Current date '" + Accounting_lbl_CurrentOpeningBalanceDate.Text + "' under 'Change opening balance date' popup on Opening balances Page.");
            }
            catch (Exception ex)                             
            {
                Report.AddToHtmlReportFailed(_driver, ex, "Current date '" + Accounting_lbl_CurrentOpeningBalanceDate.Text + "' under 'Change opening balance date' popup on Opening balances Page.");
            }
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.Accounting_txt_NewOpeningBalanceDate));
                Accounting_txt_NewOpeningBalanceDate = _driver.FindElement(By.Id(ElementLocators.Accounting_txt_NewOpeningBalanceDate));
                Report.AddToHtmlReportPassed("'Change to' textbox under 'Change opening balance date' popup on Opening balances Page.");
            }
            catch (Exception ex)                             
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Change to' textbox under 'Change opening balance date' popup on Opening balances Page.");
            }
            try
            {
                Accounting_btn_Save = _driver.FindElement(By.Id(ElementLocators.Accounting_btn_Save));
                Report.AddToHtmlReportPassed("'Save' button under 'Change opening balance date' popup on Opening balances Page.");
            }
            catch (Exception ex)                             
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Save' button under 'Change opening balance date' popup on Opening balances Page.");
            }
            try
            {
                Accounting_btn_Cancel = _driver.FindElement(By.Id(ElementLocators.Accounting_btn_Cancel));
                Report.AddToHtmlReportPassed("'Cancel' button under 'Change opening balance date' popup on Opening balances Page.");
            }
            catch (Exception ex)                             
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Cancel' button under 'Change opening balance date' popup on Opening balances Page.");
            }

            if (Accounting_lbl_CurrentOpeningBalanceDate.Text == "01/07/2013")
            {
                Common.enterText(Accounting_txt_NewOpeningBalanceDate, "01/07/2012");
                new Common(_driver).pause(1000);
                Accounting_btn_Save.Click();
            }
            else
            {
                Accounting_btn_Cancel.Click();
            }
            new Common(_driver).pause(2000);

        }

        private void InitSetupAssistantPopupElements()
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.SetupAssi_txt_BusinessName));
                SetupAssi_txt_BusinessName = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_BusinessName));
                Report.AddToHtmlReportPassed("'Business name' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Business name' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_TradingName = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_TradingName));
                Report.AddToHtmlReportPassed("'Trading name' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Trading name' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_PhoneNumber = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_PhoneNumber));
                Report.AddToHtmlReportPassed("'Phone number' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Phone number' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_Email = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_Email));
                Report.AddToHtmlReportPassed("'Email address' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Email address' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_chk_RegisteredForGST = _driver.FindElement(By.Id(ElementLocators.SetupAssi_chk_RegisteredForGST));
                Report.AddToHtmlReportPassed("'Registered for GST' checkbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Registered for GST' checkbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_ABN = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_ABN));
                Report.AddToHtmlReportPassed("'ABN' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'ABN' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_BusinessAddr = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_BusinessAddr));
                Report.AddToHtmlReportPassed("'Business Address' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Business Address' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_BusinessCity = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_BusinessCity));
                Report.AddToHtmlReportPassed("'Business City' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Business City' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_BusinessState = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_BusinessState));
                Report.AddToHtmlReportPassed("'Business State' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Business State' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_txt_BusinessPostCode = _driver.FindElement(By.Id(ElementLocators.SetupAssi_txt_BusinessPostCode));
                Report.AddToHtmlReportPassed("'Business Postcode' textbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Business Postcode' textbox on Setup Assistant popup panel.");
            }
            try
            {
                SetupAssi_chk_SameAsBusinessAddr = _driver.FindElement(By.Id(ElementLocators.SetupAssi_chk_SameAsBusinessAddr));
                Report.AddToHtmlReportPassed("'Same as business address' checkbox on Setup Assistant popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Same as business address' checkbox on Setup Assistant popup panel.");
            }
        }

        private void checkConfiormationEmail(string strEmail)
        {
            _driver.Navigate().GoToUrl("http://" + strEmail);

            for (int i = 0; i < 6; i++)
            {
                new Common(_driver).pause(5000);
                _driver.Navigate().Refresh();
            }
            try
            {
                IWebElement lnkEmailRecieved;
                new Common(_driver).waitForElement(By.XPath(ElementLocators.EmailSub_lnk_Welcome));
                lnkEmailRecieved = _driver.FindElement(By.XPath(ElementLocators.EmailSub_lnk_Welcome));
                Report.AddToHtmlReportPassed("Email Subject text 'welcome to myob liveaccounts!' verification in inbox.");
                lnkEmailRecieved.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "Email Subject text 'welcome to myob liveaccounts!' verification in inbox.");
            }

            //Step-4
            Report.AddToHtmlReport("Step-4: Click on Activation link on email inbox.", false, true);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.EmailCont_lnk_ActivateAcc));
                IWebElement lnkConfirmEmail = _driver.FindElement(By.XPath(ElementLocators.EmailCont_lnk_ActivateAcc));
                Report.AddToHtmlReportPassed("Activation Link verification in Email content.");
                lnkConfirmEmail.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "Activation Link verification in Email content.");
            }
            new Common(_driver).pause(10000);
        }

        private void InitRegDonePageElements()
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.RegDone_lbl_HeaderDone));
                RegDone_lbl_HeaderDone = _driver.FindElement(By.XPath(ElementLocators.RegDone_lbl_HeaderDone));
                Report.AddToHtmlReportPassed("'Almost done!' header text verification on Sign up finish Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Almost done!' header text verification on Sign up finish Page.");
            }
            //try
            //{
            //    RegDone_lbl_ToComplete = _driver.FindElement(By.XPath(ElementLocators.RegDone_lbl_ToComplete));
            //    Report.AddToHtmlReportPassed("'To complete your registration, please click the link in the email' text verification on Sign up finish Page.");
            //}
            //catch (Exception ex)
            //{
            //    Report.AddToHtmlReportFailed(_driver, ex, "'To complete your registration, please click the link in the email' text verification on Sign up finish Page.");
            //}
        }

        private void InitRegPageElements()
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Reg_txt_FirstName));
                Reg_txt_FirstName = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_FirstName));
                Report.AddToHtmlReportPassed("'First name' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'First name' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_SurName = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_SurName));
                Report.AddToHtmlReportPassed("'Last name' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Last name' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_Email = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_Email));
                Report.AddToHtmlReportPassed("'Email' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Email' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_PhoneNo = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_PhoneNo));
                Report.AddToHtmlReportPassed("'Phone' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Phone' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_UserName = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_UserName));
                Report.AddToHtmlReportPassed("'User name' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'User name' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_Password = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_Password));
                Report.AddToHtmlReportPassed("'Password' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Password' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_ConfPassword = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_ConfPassword));
                Report.AddToHtmlReportPassed("'Confirm password' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Confirm password' textbox on Sign up Page.");
            }
            try
            {
                Reg_txt_BusinessName = _driver.FindElement(By.XPath(ElementLocators.Reg_txt_BusinessName));
                Report.AddToHtmlReportPassed("'Business name' textbox on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Business name' textbox on Sign up Page.");
            }
            try
            {
                Reg_btn_Save = _driver.FindElement(By.XPath(ElementLocators.Reg_btn_Save));
                Report.AddToHtmlReportPassed("'I agree with Terms of Use - Sign me up' button on Sign up Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'I agree with Terms of Use - Sign me up' button on Sign up Page.");
            }
        }

        private void InitCreateInvoicePageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.CreateInvoice_ddl_InvoiceTo));
                CreateInvoice_ddl_InvoiceTo = _driver.FindElement(By.XPath(ElementLocators.CreateInvoice_ddl_InvoiceTo));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoice to' dropdownlist on Invoices Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Invoice to' dropdownlist on Invoices Page.");
            }

            try
            {
                CreateInvoice_txt_Notes = _driver.FindElement(By.Id(ElementLocators.CreateInvoice_txt_Notes));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Notes' textbox in Invoices Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Notes' textbox in Invoices Page.");
            }
            try
            {
                CreateInvoice_btn_Save = _driver.FindElement(By.Id(ElementLocators.CreateInvoice_btn_Save));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save' button on Invoices Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Save' button on Invoices Page.");
            }
            try
            {
                CreateInvoice_btn_SaveNAdd = _driver.FindElement(By.Id(ElementLocators.CreateInvoice_btn_SaveNAdd));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save & add another' button on Invoices Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Save & add another' button on Invoices Page.");
            }
            try
            {
                CreateInvoice_btn_PreviewNPrint = _driver.FindElement(By.Id(ElementLocators.CreateInvoice_btn_PreviewNPrint));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Preview & print' button on Invoices Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Preview & print' button on Invoices Page.");
            }
            try
            {
                CreateInvoice_btn_EmailToCust = _driver.FindElement(By.Id(ElementLocators.CreateInvoice_btn_EmailToCust));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Email to customer' button on Invoices Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Email to customer' button on Invoices Page.");
            }
        }


        //#######################################################
        #region Private Events

        private void fun_EnterLoginData(string strEmail, string strPass)
        {
            InitLoginElements();
            Login_txt_Email.Click();
            Login_txt_Email.Clear();
            Login_txt_Email.SendKeys(strEmail);

            Login_txt_Password.Click();
            Login_txt_Password.Clear();
            if (strPass.Trim().Length > 0)Login_txt_Password.SendKeys(strPass);

            Report.AddToHtmlReport("<br>Data Entered", false, false, true);
            Report.AddToHtmlReport("Email Address: " + strEmail, false);
            Report.AddToHtmlReport("Password: " + strPass + "<br>", false);

            Login_btn_Login.Click();
            try
            {
                new Common(_driver).pause(4000);
                IWebElement Login_btn_CloseOtherSession = _driver.FindElement(By.Id(ElementLocators.Login_btn_CloseOtherSession));
                Login_btn_CloseOtherSession.Click();
            }
            catch (Exception) { }

            new Common(_driver).pause(5000);
        }

        public void fun_DoLogout(IWebDriver driver, bool flgShowInReport = true)
        {
            _driver = driver;
            //InitDashBoardPageElements(flgShowInReport);

            try
            {
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Click on 'Logout' button.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Click on 'Logout' button.");
            }
            Dashboard_btn_logout.Click();
            new Common(_driver).pause(5000);
        }

        private void InitDashBoardPageElements(bool flgShowInReport = true)
        {
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                Dashboard_lbl_HeaderWelcome = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuInvoices = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuInvoices));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoices' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Invoices' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuExpenses = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuExpenses));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expenses' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expenses' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuBanking = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuBanking));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Banking' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Banking' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuContacts = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuContacts));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contacts' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contacts' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuPayroll = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuPayroll));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Payroll' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Payroll' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuReports = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuReports));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Reports' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Reports' menu button on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Logout' button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Logout' button on Dashboard Page.");
            }
        }

        private void InitLoginElements()
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Login_lbl_Header));
                Login_lbl_Header = _driver.FindElement(By.XPath(ElementLocators.Login_lbl_Header));
                Report.AddToHtmlReportPassed("Header label text element 'Welcome to MYOB LiveAccounts' on Login Page.");
            }
            catch (Exception ex)
            {
                //Report.AddToHtmlReportFailed(_driver, ex, "Header label text element 'Welcome to MYOB LiveAccounts' on Login Page.");
            }

            try
            {
                Login_lnk_ForgetYourPassword = _driver.FindElement(By.XPath(ElementLocators.Login_lnk_ForgetYourPassword));
                Report.AddToHtmlReportPassed("'Forgot your password?' link on Login Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Forgot your password?' link on Login Page.");
            }

            try
            {
                Login_lnk_NotRegistered = _driver.FindElement(By.XPath(ElementLocators.Login_lnk_NotRegistered));
                Report.AddToHtmlReportPassed("'Not registered?' link on Login Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Not registered?' link on Login Page.");
            }
            try
            {
                Login_txt_Email = _driver.FindElement(By.Id(ElementLocators.Login_txt_Email));
                Report.AddToHtmlReportPassed("'Username' textbox on Login Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Username' textbox on Login Page.");
            }

            try
            {
                Login_txt_Password = _driver.FindElement(By.Id(ElementLocators.Login_txt_Password));
                Report.AddToHtmlReportPassed("'Password' textbox on Login Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Password' textbox on Login Page.");
            }

            try
            {
                Login_btn_Login = _driver.FindElement(By.Id(ElementLocators.Login_btn_Login));
                Report.AddToHtmlReportPassed("'Log in' button on Login Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Log in' button on Login Page.");
            }

        }
        
        #endregion

    }
}
