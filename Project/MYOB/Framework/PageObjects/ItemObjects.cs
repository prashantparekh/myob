﻿using System;
using System.Configuration;
using System.Data;
using Framework.Init;
using OpenQA.Selenium;

namespace Framework.PageObjects
{
    public class ItemObjects
    {
        IWebDriver _driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strSignupUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("SignupUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Dashboard_lbl_HeaderWelcome;
        IWebElement Dashboard_btn_logout;
        IWebElement Dashboard_lnk_MenuInvoices;
        IWebElement Dashboard_lnk_MenuExpenses;
        IWebElement Dashboard_lnk_MenuBanking;
        IWebElement Dashboard_lnk_MenuContacts;
        IWebElement Dashboard_lnk_MenuPayroll;
        IWebElement Dashboard_lnk_MenuReports;
        IWebElement Dashboard_lnk_SubMenuViewItems;
        IWebElement ViewItem_btn_CreateItem;
        static IWebElement CreateItem_txt_ItemName;
        static IWebElement CreateItem_txt_ItemNumber;
        static IWebElement CreateItem_txt_UnitOfMeasure;
        static IWebElement CreateItem_txt_Description;
        static IWebElement CreateItem_txt_SellPrice;
        static IWebElement CreateItem_ddl_AllocateTo;
        static IWebElement CreateItem_btn_SaveItem;
        static IWebElement AllocateTo_lbl_4_2400Sales1;
        static IWebElement AllocateTo_lbl_4_2600Sales2;
        static IWebElement CreateItem_txt_BuyPrice;
        static IWebElement CreateItem_ddl_BuyAllocateTo;
        static IWebElement AllocateTo_lbl_OfficeSupplies;
        static IWebElement AllocateTo_lbl_Telephone;
        

        public IWebDriver CreateItems(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Create item page.", false, true);
            InitDashBoardPageElements();
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/VerificationProxy?request=/JSP/Items/Item.jsp?navType=invoices");
            /* commented to redirect to page via static URL 
            Actions actions = new Actions(_driver);
            actions.MoveToElement(Dashboard_lnk_MenuInvoices).Build().Perform();
            new Common(_driver).pause(1000);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuViewItems));
                Dashboard_lnk_SubMenuViewItems = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuViewItems));
                actions.MoveToElement(Dashboard_lnk_MenuInvoices).MoveToElement(Dashboard_lnk_SubMenuViewItems).Click().Build().Perform();
                Report.AddToHtmlReportPassed("'View items' submenu button under Invoices menu on Dashboard Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'View items' submenu button under Invoices menu on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.ViewItem_btn_CreateItem));
                ViewItem_btn_CreateItem = _driver.FindElement(By.XPath(ElementLocators.ViewItem_btn_CreateItem));
                ViewItem_btn_CreateItem.Click();
                Report.AddToHtmlReportPassed("'Create Item' button on View item Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Create Item' button on View item Page.");
            }


            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Create item page.", false, true);
            InitCreateItemPageElements();
            //read excel
            DataTable dt = null;
            lock (this)
            {
                dt = Common.ExcelReadData("select * from [Items$] ", strRegDataExcelFilePath); 
            }
            string strItemName = string.Empty, strItemNo = string.Empty, strUnit = string.Empty, strDesc = string.Empty, strSellPrice = string.Empty, strAllocateTo = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InitCreateItemPageElements(false);
                new Common(_driver).pause(2000);
                strItemName = Convert.ToString(dt.Rows[i][0]);
                strItemNo = Convert.ToString(dt.Rows[i][1]);
                strUnit = Convert.ToString(dt.Rows[i][2]);
                strDesc = Convert.ToString(dt.Rows[i][3]);
                strSellPrice = Convert.ToString(dt.Rows[i][4]);
                strAllocateTo = Convert.ToString(dt.Rows[i][5]);
                Common.enterText(CreateItem_txt_ItemName, strItemName);
                Common.enterText(CreateItem_txt_ItemNumber, strItemNo);
                Common.enterText(CreateItem_txt_UnitOfMeasure, strUnit);
                Common.enterText(CreateItem_txt_Description, strDesc);
                //Common.enterText(CreateItem_txt_SellPrice, strSellPrice);
                //CreateItem_ddl_AllocateTo.Click();
                if (strAllocateTo.ToLower() == "4-2400 sales 1")
                {
                    Common.enterText(CreateItem_txt_SellPrice, strSellPrice);
                    CreateItem_ddl_AllocateTo.Click();
                    try
                    {
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.AllocateTo_lbl_4_2400Sales1));
                        AllocateTo_lbl_4_2400Sales1 = _driver.FindElement(By.XPath(ElementLocators.AllocateTo_lbl_4_2400Sales1));
                        AllocateTo_lbl_4_2400Sales1.Click();
                        if(i == 0) Report.AddToHtmlReportPassed("'4-2400 Sales 1' item name under Allocate to combobox on Create item Page.");
                    }
                    catch (Exception ex)
                    {
                        if (i == 0) Report.AddToHtmlReportFailed(_driver, ex, "'4-2400 Sales 1' item name under Allocate to combobox on Create item Page.");
                    }
                    //new Common(_driver).pause(2000);
                }
                else if (strAllocateTo.ToLower() == "4-2600 sales 2")
                {
                    Common.enterText(CreateItem_txt_SellPrice, strSellPrice);
                    CreateItem_ddl_AllocateTo.Click();
                    try
                    {
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.AllocateTo_lbl_4_2600Sales2));
                        AllocateTo_lbl_4_2600Sales2 = _driver.FindElement(By.XPath(ElementLocators.AllocateTo_lbl_4_2600Sales2));
                        AllocateTo_lbl_4_2600Sales2.Click();
                        if (i == 0) Report.AddToHtmlReportPassed("'4-2600 Sales 2' item name under Allocate to combobox on Create item Page.");
                    }
                    catch (Exception ex)
                    {
                        if (i == 0) Report.AddToHtmlReportFailed(_driver, ex, "'4-2600 Sales 2' item name under Allocate to combobox on Create item Page.");
                    }
                    //new Common(_driver).pause(2000);
                }
                else if (strAllocateTo.ToLower() == "office supplies")
                {
                    Common.enterText(CreateItem_txt_BuyPrice, strSellPrice);
                    CreateItem_ddl_BuyAllocateTo.Click();
                    try
                    {
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.AllocateTo_lbl_OfficeSupplies));
                        AllocateTo_lbl_OfficeSupplies = _driver.FindElement(By.XPath(ElementLocators.AllocateTo_lbl_OfficeSupplies));
                        AllocateTo_lbl_OfficeSupplies.Click();
                        if (i == 0) Report.AddToHtmlReportPassed("'Office supplies' item name under Allocate to combobox on Create item Page.");
                    }
                    catch (Exception ex)
                    {
                        if (i == 0) Report.AddToHtmlReportFailed(_driver, ex, "'Office supplies' item name under Allocate to combobox on Create item Page.");
                    }
                    //new Common(_driver).pause(2000);
                }
                else if (strAllocateTo.ToLower() == "telephone")
                {
                    try
                    {
                        Common.enterText(CreateItem_txt_BuyPrice, strSellPrice);
                        IWebElement txtAllocateTo = driver.FindElement(By.XPath("//input[@id='trackpurchases']"));
                        Common.enterText(txtAllocateTo, "telephone");
                        IWebElement strTelephone = driver.FindElement(By.XPath("//td[contains(.,'Telephone')]"));
                        strTelephone.Click();
                        if (i == 0) Report.AddToHtmlReportPassed("'Telephone' item name under Allocate to combobox on Create item Page.");
                    }
                    catch (Exception ex)
                    {
                        if (i == 0) Report.AddToHtmlReportFailed(_driver, ex, "'Telephone' item name under Allocate to combobox on Create item Page.");
                    }
                    
                    /*CreateItem_ddl_BuyAllocateTo.Click();
                    try
                    {
                        new Common(_driver).waitForElement(By.XPath(ElementLocators.AllocateTo_lbl_Telephone));
                        AllocateTo_lbl_Telephone = _driver.FindElement(By.XPath(ElementLocators.AllocateTo_lbl_Telephone));
                        AllocateTo_lbl_Telephone.Click();
                        if (i == 0) Report.AddToHtmlReportPassed("'Telephone' item name under Allocate to combobox on Create item Page.");
                    }
                    catch (Exception ex)
                    {
                        if (i == 0) Report.AddToHtmlReportFailed(_driver, ex, "'Telephone' item name under Allocate to combobox on Create item Page.");
                    }*/
                    //new Common(_driver).pause(2000);
                }

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Item name: " + strItemName, false);
                Report.AddToHtmlReport("Item number: " + strItemNo, false);
                Report.AddToHtmlReport("Unit of measure: " + strUnit, false);
                Report.AddToHtmlReport("Description: " + strDesc, false);
                Report.AddToHtmlReport("Sell Price: " + strSellPrice, false);
                Report.AddToHtmlReport("Allocate to: " + strAllocateTo + "<br>", false);
                Report.AddToHtmlReport("<hr>");

                CreateItem_btn_SaveItem.Click();
                new Common(_driver).pause(4000);
            }

            return _driver;
        }

        private void InitCreateItemPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.CreateItem_txt_ItemName));
                CreateItem_txt_ItemName = _driver.FindElement(By.XPath(ElementLocators.CreateItem_txt_ItemName));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Item name' textbox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Item name' textbox on Create item Page.");
            }
            try
            {
                CreateItem_txt_ItemNumber = _driver.FindElement(By.XPath(ElementLocators.CreateItem_txt_ItemNumber));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Item number' textbox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Item number' textbox on Create item Page.");
            }
            try
            {
                CreateItem_txt_UnitOfMeasure = _driver.FindElement(By.XPath(ElementLocators.CreateItem_txt_UnitOfMeasure));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Unit of measure' textbox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Unit of measure' textbox on Create item Page.");
            }
            try
            {
                CreateItem_txt_Description = _driver.FindElement(By.XPath(ElementLocators.CreateItem_txt_Description));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Description' textbox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Description' textbox on Create item Page.");
            }
            try
            {
                CreateItem_txt_SellPrice = _driver.FindElement(By.XPath(ElementLocators.CreateItem_txt_SellPrice));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Sell Price' textbox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Sell Price' textbox on Create item Page.");
            }
            try
            {
                CreateItem_ddl_AllocateTo = _driver.FindElement(By.XPath(ElementLocators.CreateItem_ddl_AllocateTo));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Allocate to' combobox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Allocate to' combobox on Create item Page.");
            }
            try
            {
                CreateItem_txt_BuyPrice = _driver.FindElement(By.XPath(ElementLocators.CreateItem_txt_BuyPrice));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Buy Price' textbox on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Buy Price' textbox on Create item Page.");
            }
            try
            {
                CreateItem_ddl_BuyAllocateTo = _driver.FindElement(By.XPath(ElementLocators.CreateItem_ddl_BuyAllocateTo));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Allocate to' combobox for Buying Details on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Allocate to' combobox for Buying Details on Create item Page.");
            }
            try
            {
                CreateItem_btn_SaveItem = _driver.FindElement(By.XPath(ElementLocators.CreateItem_btn_SaveItem));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save Item' button on Create item Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save Item' button on Create item Page.");
            }
        }

        private void InitDashBoardPageElements(bool flgShowInReport = true)
        {
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                Dashboard_lbl_HeaderWelcome = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuInvoices = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuInvoices));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoices' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Invoices' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuExpenses = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuExpenses));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expenses' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expenses' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuBanking = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuBanking));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Banking' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Banking' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuContacts = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuContacts));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contacts' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contacts' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuPayroll = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuPayroll));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Payroll' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Payroll' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuReports = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuReports));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Reports' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Reports' menu button on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Logout' button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Logout' button on Dashboard Page.");
            }
        }
    }
}
