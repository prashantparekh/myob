﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Framework.Init;
using OpenQA.Selenium;

namespace Framework.PageObjects
{
    public class PayrollObjects
    {
        IWebDriver _driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strSignupUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("SignupUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Dashboard_lbl_HeaderWelcome;
        IWebElement Dashboard_btn_logout;
        IWebElement Dashboard_lnk_MenuInvoices;
        IWebElement Dashboard_lnk_MenuExpenses;
        IWebElement Dashboard_lnk_MenuBanking;
        IWebElement Dashboard_lnk_MenuContacts;
        IWebElement Dashboard_lnk_MenuPayroll;
        IWebElement Dashboard_lnk_MenuReports;
        IWebElement Dashboard_lnk_SubMenuPayrollSettings;
        IWebElement PayrollSetting_ddl_BankAccount;
        IWebElement PayrollSetting_btn_Save;
        IWebElement PayCentre_ddl_HowOften;
        IWebElement PayCentre_txt_FromDate;
        IWebElement PayCentre_chk_SelectAllEmp;
        IWebElement PayCentre_btn_StartPayrun;
        IWebElement PayRun_btn_ReviewPayslips;
        IWebElement ReviewPayslips_btn_FinishPayrun;
        IWebElement PayrunSummary_btn_FinishPayrun;
        IWebElement PayrunSummary_btn_PrintPayslipToFinish;
        IWebElement PayrunSummary_btn_PopupPrintSave;

        public IWebDriver Payruns(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Payroll settings page.", false, true);
            InitDashBoardPageElements(); //1-1000 Cheque account
            _driver.Navigate().GoToUrl("https://your.liveaccounts.com.au/LA/app/businesses/" + TestCases.Login.strURLDynCode + "/payrollSettings");
            /* commented to redirect to page via static URL 
            Actions actions = new Actions(_driver);
            actions.MoveToElement(Dashboard_lnk_MenuPayroll).Build().Perform();
            new Common(_driver).pause(1000);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuPayrollSettings));
                Dashboard_lnk_SubMenuPayrollSettings = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_SubMenuPayrollSettings));
                actions.MoveToElement(Dashboard_lnk_MenuPayroll).MoveToElement(Dashboard_lnk_SubMenuPayrollSettings).Click().Build().Perform();
                Report.AddToHtmlReportPassed("'Payroll settings' submenu button under Payroll menu on Dashboard Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Payroll settings' submenu button under Payroll menu on Dashboard Page.");
            }*/

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Payroll page.", false, true);
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [Payruns$] ", strRegDataExcelFilePath, false);
            string strBankAccount = string.Empty, strHowOften = string.Empty, strStartDate = string.Empty;
            int intNoOfPayruns = 0;

            strBankAccount = Convert.ToString(dt.Rows[0][1]);
            strHowOften = Convert.ToString(dt.Rows[1][1]);
            intNoOfPayruns = Convert.ToInt32(dt.Rows[2][1]);
            //DateTime dtStartDate;
            //bool isValidDate = DateTime.TryParse(dt.Rows[3][1].ToString(), out dtStartDate);
            //strStartDate = Convert.ToString(dt.Rows[3][1]);
            strStartDate = Convert.ToDateTime(dt.Rows[3][1]).ToShortDateString();
            
            if (strStartDate.Length > 10) strStartDate = strStartDate.Substring(0, strStartDate.IndexOf(" "));
            
            strStartDate = Convert.ToDateTime(strStartDate).ToString("dd/MM/yyyy");
            
            if (strStartDate.IndexOf("-") > 0) strStartDate = strStartDate.Replace("-", "/");
            
            InitPayrollSettingsPageElements();
            new Common(_driver).select(PayrollSetting_ddl_BankAccount, strBankAccount);
            PayrollSetting_btn_Save.Click();

            //Step-5
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Pay centre page.", false, true);
            for (int i = 0; i < intNoOfPayruns; i++)
            {
                Report.AddToHtmlReport("<br>Payrun process count " + (i+1) + ".<br>",false, true);
                if (i == 0)
                {
                    InitPayCentrePageElements();
                    new Common(_driver).select(PayCentre_ddl_HowOften, strHowOften);
                    Common.enterText(PayCentre_txt_FromDate, strStartDate);
                    Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                    Report.AddToHtmlReport("How often: " + strHowOften, false);
                    Report.AddToHtmlReport("From date: " + strStartDate + "<br>", false);
                }
                else InitPayCentrePageElements(false);
                
                if (PayCentre_chk_SelectAllEmp.GetAttribute("checked") == null) PayCentre_chk_SelectAllEmp.Click();
                PayCentre_btn_StartPayrun.Click();

                if(i==0) InitPayrunPageElements();
                else InitPayrunPageElements(false);
                PayRun_btn_ReviewPayslips.Click();
                new Common(_driver).pause(5000);

                if (i == 0) InitReviewPaySlipsPageElements();
                else InitReviewPaySlipsPageElements(false);
                ReviewPayslips_btn_FinishPayrun.Click();

                if (i == 0) InitPayrunSummaryPageElements();
                else InitPayrunSummaryPageElements(false);
                PayrunSummary_btn_FinishPayrun.Click();

                if (i == 0) InitPayrunSummaryPopupElements();
                else InitPayrunSummaryPopupElements(false);
                //PayrunSummary_btn_PrintPayslipToFinish.Click();

                new Common(_driver).pause(5000);
                
                //##########################################################
                string originalWindowHandle = _driver.CurrentWindowHandle;
                string foundHandle = null;
                IList<string> existingHandles = _driver.WindowHandles;
                PayrunSummary_btn_PrintPayslipToFinish.Click();
                new Common(_driver).pause(4000);
                IList<string> currentHandles = _driver.WindowHandles;
                IList<string> differentHandles = currentHandles.Except(existingHandles).ToList();
                if (differentHandles.Count > 0) foundHandle = differentHandles[0];
                if (string.IsNullOrEmpty(foundHandle)) throw new Exception("didn't find popup window within timeout");
                _driver.SwitchTo().Window(foundHandle);
                new Common(_driver).pause(2000);
                _driver.Close();
                _driver.SwitchTo().Window(originalWindowHandle);
                //##########################################################

                try
                {
                    new Common(_driver).waitForElement(By.XPath(ElementLocators.PayrunSummary_btn_PopupPrintSave));
                    PayrunSummary_btn_PopupPrintSave = _driver.FindElement(By.XPath(ElementLocators.PayrunSummary_btn_PopupPrintSave));
                    //if (flgShowInReport) Report.AddToHtmlReportPassed("'Print payslips to finish pay run' button on Pay run summary Page.");
                    PayrunSummary_btn_PopupPrintSave.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'OK' button under 'Pay Run Complete' popup on Pay run summary Page.");
                }
                new Common(_driver).pause(4000);
            }
           
            return _driver;
        }

        private void InitPayrunSummaryPopupElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).pause(3000);
                new Common(_driver).waitForElement(By.Id(ElementLocators.PayrunSummary_btn_PrintPayslipToFinish));
                PayrunSummary_btn_PrintPayslipToFinish = _driver.FindElement(By.Id(ElementLocators.PayrunSummary_btn_PrintPayslipToFinish));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Print payslips to finish pay run' button under 'Pay run is processing' popup on Pay run summary Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Print payslips to finish pay run' button under 'Pay run is processing' popup on Pay run summary Page.");
            }
        }

        private void InitPayrunSummaryPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.PayrunSummary_btn_FinishPayrun));
                PayrunSummary_btn_FinishPayrun = _driver.FindElement(By.Id(ElementLocators.PayrunSummary_btn_FinishPayrun));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Finish the pay run' button on Pay run summary Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Finish the pay run' button on Pay run summary Page.");
            }
        }

        private void InitReviewPaySlipsPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.ReviewPayslips_btn_FinishPayrun));
                ReviewPayslips_btn_FinishPayrun = _driver.FindElement(By.Id(ElementLocators.ReviewPayslips_btn_FinishPayrun));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Finish pay run' button on Review Payslips Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Finish pay run' button on Review Payslips Page.");
            }
        }

        private void InitPayrunPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.PayRun_btn_ReviewPayslips));
                PayRun_btn_ReviewPayslips = _driver.FindElement(By.Id(ElementLocators.PayRun_btn_ReviewPayslips));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Review payslips' button on Pay run Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Review payslips' button on Pay run Page.");
            }
        }

        private void InitPayCentrePageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.PayCentre_ddl_HowOften));
                PayCentre_ddl_HowOften = _driver.FindElement(By.Id(ElementLocators.PayCentre_ddl_HowOften));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'How often' combobox on Pay centre Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'How often' combobox on Pay centre Page.");
            }
            try
            {
                PayCentre_txt_FromDate = _driver.FindElement(By.Id(ElementLocators.PayCentre_txt_FromDate));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'From' textbox on Pay centre Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'From' textbox on Pay centre Page.");
            }
            try
            {
                PayCentre_chk_SelectAllEmp = _driver.FindElement(By.Id(ElementLocators.PayCentre_chk_SelectAllEmp));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Select all' employees to pay checkbox on Pay centre Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Select all' employees to pay checkbox on Pay centre Page.");
            }
            try
            {
                PayCentre_btn_StartPayrun = _driver.FindElement(By.Id(ElementLocators.PayCentre_btn_StartPayrun));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Start pay run' button on Pay centre Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Start pay run' button on Pay centre Page.");
            }

        }

        private void InitPayrollSettingsPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.PayrollSetting_ddl_BankAccount));
                PayrollSetting_ddl_BankAccount = _driver.FindElement(By.Id(ElementLocators.PayrollSetting_ddl_BankAccount));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Bank account for paying wages' combobox on Payroll settings Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Bank account for paying wages' combobox on Payroll settings Page.");
            }
            try
            {
                PayrollSetting_btn_Save = _driver.FindElement(By.Id(ElementLocators.PayrollSetting_btn_Save));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save' button on Payroll settings Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save' button on Payroll settings Page.");
            }
            
        }

        private void InitDashBoardPageElements(bool flgShowInReport = true)
        {
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                Dashboard_lbl_HeaderWelcome = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuInvoices = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuInvoices));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoices' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Invoices' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuExpenses = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuExpenses));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expenses' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expenses' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuBanking = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuBanking));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Banking' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Banking' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuContacts = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuContacts));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contacts' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contacts' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuPayroll = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuPayroll));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Payroll' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Payroll' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuReports = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuReports));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Reports' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Reports' menu button on Dashboard Page.");
            }*/
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Logout' button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Logout' button on Dashboard Page.");
            }
        }

    }
}
