﻿using System;
//using OpenQA.Selenium;

namespace Framework.Init
{
    public static class ElementLocators
    {
       
        #region //########### Login Page Elements ###########//

        public static String Login_lbl_Header = "//div[@id='main']/div/h2[contains(.,'Welcome to MYOB LiveAccounts')]";
        public static String Login_txt_Email = "username"; //"//input[@id='username']";
        //public static String Login_txt_EmailErrMsg = "//div[@class='semailformError parentFormform1 formError']/div[@class='formErrorContent']";
        public static String Login_txt_Password = "password"; //"//input[@id='password']";
        public static String Login_btn_Login = "logIn"; //"//input[@id='logIn']";
        public static String Login_btn_CloseOtherSession = "continueLogin"; //"//button[@id='continueLogin']";
        public static String Login_lbl_ErrEnterEmail = "//div[@id='message_1']/div[@class='body'][contains(.,'Enter your email address.')]";
        public static String Login_lbl_ErrEnterPwd = "//div[@id='message_1']/div[@class='body'][contains(.,'Enter your password.')]";
        public static String Login_lbl_ErrInvalidUserPwd = "//div[@id='message_1']/div[@class='body'][contains(.,'User does not exist or invalid password.')]";
        public static String Login_lnk_NotRegistered = "//div[@class='not-registered']/a[contains(.,'Not registered?')]";
        public static String Login_lnk_ForgetYourPassword = "//div[@class='forgot-password']/a[contains(.,'Forgot your password?')]";

        //Login popup for new user [Setup Assistant]
        public static String SetupAssi_lbl_SetupAssistantPopup = "//td[@class='dialog-header-text'][contains(.,'Setup assistant')]";
        public static String SetupAssi_btn_Next = "//button[contains(@class,'x-btn-text nextIcon')]";
        public static String SetupAssi_txt_BusinessName = "gsCompanyName"; //"//input[@id='gsCompanyName']";
        public static String SetupAssi_txt_TradingName = "gsTradingName"; //"//input[@id='gsTradingName']";
        public static String SetupAssi_txt_PhoneNumber = "gsPhoneNumber"; //"//input[@id='gsPhoneNumber']";
        public static String SetupAssi_txt_Email = "gsEmail"; //"//input[@id='gsEmail']";
        public static String SetupAssi_txt_ABN = "gsAbn"; //"//input[@id='gsAbn']";
        public static String SetupAssi_txt_BusinessAddr = "gsBusinessAddress"; //"//textarea[@id='gsBusinessAddress']";
        public static String SetupAssi_txt_BusinessCity = "gsBusinessCity"; //"//input[@id='gsBusinessCity']";
        public static String SetupAssi_txt_BusinessState = "gsBusinessState"; //"//input[@id='gsBusinessState']";
        public static String SetupAssi_txt_BusinessPostCode = "gsBusinessPostCode"; //"//input[@id='gsBusinessPostCode']";
        public static String SetupAssi_chk_RegisteredForGST = "gsDoApplyGST"; //"//input[@id='gsDoApplyGST']";
        public static String SetupAssi_chk_SameAsBusinessAddr = "gsSameAsBusinessAddress"; //"//input[@id='gsSameAsBusinessAddress']";
        public static String SetupAssi_btn_Finish = "//button[contains(@class,'x-btn-text') and contains(@class,'finishIcon')]";

        public static String SetupAssi_btn_StartUsingLiveAccounts = "//button[contains(@class,'x-btn-text startusingLiveAccountsIcon')]";

        #endregion

        #region //########### DashBoard Page Elements ###########//

        public static String Dashboard_lbl_HeaderWelcome = "//span[@class='readMoreToggle'][contains(.,'Welcome to') and contains(.,'MYOB LiveAccounts')]";
        public static String Dashboard_btn_logout = "//a[contains(.,'Log out')]";
       /* public static String Dashboard_lnk_MenuInvoices = "//li[@id='invoicesTab']/a[contains(.,'Invoices')]"; 
        public static String Dashboard_lnk_MenuExpenses = "//li[@id='expensesTab']/a[contains(.,'Expenses')]";
        public static String Dashboard_lnk_MenuBanking = "//li[@id='bankingTab']/a[contains(.,'Banking')]";
        public static String Dashboard_lnk_MenuContacts = "//li[@id='contactsTab']/a[contains(.,'Contacts')]";
        public static String Dashboard_lnk_MenuPayroll = "//li[@id='payrollTab']/a[contains(.,'Payroll')]";
        public static String Dashboard_lnk_MenuReports = "//li[@id='reportsTab']/a[contains(.,'Reports')]"; */

        public static String Dashboard_lnk_MenuInvoices = "//li[contains(@class,'dropdown')]/a[contains(.,'Invoices')]";
        public static String Dashboard_lnk_MenuExpenses = "//li[contains(@class,'dropdown')]/a[contains(.,'Expenses')]";
        public static String Dashboard_lnk_MenuBanking = "//li[contains(@class,'dropdown')]/a[contains(.,'Banking')]";
        public static String Dashboard_lnk_MenuContacts = "//li[contains(@class,'dropdown')]/a[contains(.,'Contacts')]";
        public static String Dashboard_lnk_MenuPayroll = "//li[contains(@class,'dropdown')]/a[contains(.,'Payroll')]";
        public static String Dashboard_lnk_MenuReports = "//li[contains(@class,'dropdown')]/a[contains(.,'Reports')]";

        public static String Dashboard_lnk_SubMenuCreateInvoice = "//ul[contains(@class,'invoices') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'Create invoice')]";
        public static String Dashboard_lnk_SubMenuViewItems = "//ul[contains(@class,'invoices') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'View items')]";
        public static String Dashboard_lnk_SubMenuCreateContact = "//ul[contains(@class,'contacts') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'Create contact')]";
        public static String Dashboard_lnk_SubMenuEmployees = "//ul[contains(@class,'payroll') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'Employees')]";
        public static String Dashboard_lnk_SubMenuSpendMoney = "//ul[contains(@class,'banking') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'Spend money')]";
        public static String Dashboard_lnk_SubMenuReceiveMoney = "//ul[contains(@class,'banking') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'Receive money')]";
        public static String Dashboard_lnk_SubMenuPayrollSettings = "//ul[contains(@class,'payroll') and contains(@class,'sub') and contains(@class,'dropdown-menu')]/li/a[contains(.,'Payroll settings')]";
        
        //Elements for Adviser user
        public static String DashboardAdvi_lnk_HeaderMenuUsers = "//a[@id='2'][contains(.,'Users')]";
        public static String DashboardAdvi_lnk_EditUser = "//a[@id='ctl00_ContentPlaceHolderMainBody_buttonEditUser']";
        public static String DashboardAdvi_txt_EditEmail = "EmailAddressTextBox_text"; //"//input[@id='EmailAddressTextBox_text']";
        public static String DashboardAdvi_btn_EditSave = "buttonSave"; //"//input[@id='buttonSave']";

        public static String Dashboard_lnk_Settings = "//div[contains(@class,'topMenu settings right')]/div[contains(.,'Settings')]";
        public static String Dashboard_lnk_SubMenuAccounting = "//div[contains(@class,'topMenu settings right')]/ul/li/a[contains(.,'Accounting')]";
        
	    #endregion

        #region //########### Create invoice Page Elements ###########//

        public static String CreateInvoice_ddl_InvoiceTo = "//input[@id='contactId']/../span[contains(@class,'dropdown-toggle')]"; //"//img[@id='ext-gen19']";
        public static String InvoiceTo_lbl_DavidJonesClothi = "//td[contains(.,'David Jones Clothi...')]";
        public static String InvoiceTo_lbl_ddlFirstItem = "//div[contains(@class,'typeahead')]/table/tbody/tr[1]/td[1]"; //"//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1]";

        public static String InvoiceGrid_td_Row1ItemCell = "//div[@class='x-grid3-body']/div[1]/table/tbody/tr/td[1]";
        public static String InvoiceGrid_txt_Row1ItemCell = "//div[@class='x-grid3-scroller']/div/div/input[@type='text']";
        public static String InvoiceGrid_td_Row2ItemCell = "//div[@class='x-grid3-body']/div[2]/table/tbody/tr/td[1]";
        public static String InvoiceGrid_ddl_Col1ItemCell = "//table[@id='invoiceLinesTable']/tbody/tr[1]/td/div/div/input[@id='itemId']/../span[contains(@class,'dropdown-toggle')]"; //"//div[@class='x-grid3-scroller']/div/div/img[@class='x-form-trigger x-form-arrow-trigger']";
        public static String InvoiceGrid_ddl_Col1Row2ItemCell = "//table[@id='invoiceLinesTable']/tbody/tr[2]/td/div/div/input[@id='itemId']/../span[contains(@class,'dropdown-toggle')]"; 
        public static String InvoiceGrid_lbl_ItemWM324 = "//td[contains(.,'WM324')]";
        public static String InvoiceGrid_lbl_ItemWR99 = "//td[contains(.,'WR99')]";
        public static String InvoiceGrid_lbl_ddlFirstItem = "//table[@id='invoiceLinesTable']/tbody/tr[1]/td/div/div/input[@id='itemId']/../div[contains(@class,'typeahead')]/table/tbody/tr[1]/td[1]"; //"//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'140px')]";
        public static String InvoiceGrid_lbl_ddlSecondItem = "//table[@id='invoiceLinesTable']/tbody/tr[2]/td/div/div/input[@id='itemId']/../div[contains(@class,'typeahead')]/table/tbody/tr[2]/td[1]"; //"//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[4]/td[1][contains(@style,'140px')]";
        public static String InvoiceGrid_txt_Qty = "//table[@id='invoiceLinesTable']/tbody/tr[1]/td[4]/div/input";

        public static String CreateInvoice_txt_Notes = "notes"; //"//textarea[@id='notes']"; //"//textarea[@id='memoText']";
        public static String CreateInvoice_btn_Save = "save"; //"//button[@id='save']"; //"//a[@id='save-as-draft']";
        public static String CreateInvoice_btn_SaveNAdd = "saveAndNew"; //"//button[@id='saveAndNew']"; //"//a[@id='save-and-add-another']";
        public static String CreateInvoice_btn_PreviewNPrint = "saveAndPrint"; //"//button[@id='saveAndPrint']"; //"//a[@id='print']";
        public static String CreateInvoice_btn_EmailToCust = "email"; //"//button[@id='email']"; //"//a[@id='send-to-customer']";
	    #endregion

        #region //########### Create Customer return Page Elements ###########//

        public static String CreateCustReturn_ddl_ReturnFrom = "//input[@id='comboId']/../img";
        public static String ReturnFrom_lbl_ddlFirstItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1]";
        public static String CreateCustReturn_txt_Notes = "memoText"; //"//textarea[@id='memoText']";
        public static String CreateCustReturn_btn_Save = "save-as-draft"; //"//a[@id='save-as-draft']";
        public static String CreateCustReturn_btn_SaveNAdd = "save-and-add-another"; //"//a[@id='save-and-add-another']";
        public static String CreateCustReturn_btn_PreviewNPrint = "//a[@id='print']";
        public static String CreateCustReturn_btn_EmailToCust = "//a[@id='send-to-customer']";
        public static String CustReturnGrid_td_Row1ItemCell2 = "//div[@class='x-grid3-body']/div[1]/table/tbody/tr/td[2]";
        public static String CustReturnGrid_txt_Row1Item2 = "//div[@class='x-grid3-scroller']/div/textarea";
        public static String CustReturnGrid_ddl_Col1ItemCell = "//input[contains(@class,'x-form-text') and contains(@class,'x-form-field') and contains(@class,'x-form-focus')]/../img";
        public static String CustReturnGrid_td_Row1ItemCell = "//div[@class='x-grid3-body']/div[1]/table/tbody/tr/td[1]";
        
	    #endregion

        #region //########### Enter expenses Page Elements ###########//

        public static String EnterExpense_ddl_ExpenseFrom = "//input[@id='comboId']/../img";
        public static String EnterExpense_txt_Notes = "//textarea[@id='memoText']";
        public static String EnterExpense_btn_Save = "//a[@id='save']";
        public static String EnterExpense_btn_SaveNAdd = "//a[@id='save-and-add-another']";
        public static String ExpenseFrom_lbl_ddlFirstItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1]";
        ////div[@id='invoiceGridDiv']/div/div/div/div/div/div[2]/div[2]/div/input/../img[contains(@class,'x-form-arrow-trigger ')]
        public static String ExpenseGrid_td_Row1ItemCell = "//div[@class='x-grid3-body']/div[1]/table/tbody/tr/td[1]";
        public static String ExpenseGrid_ddl_Col1ItemCell = "//div[@class='x-grid3-scroller']/div/div/img[@class='x-form-trigger x-form-arrow-trigger']";
        public static String ExpenseGrid_lbl_ddlFirstItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'140px')]";
        public static String ExpenseGrid_td_Row2ItemCell = "//div[@class='x-grid3-body']/div[2]/table/tbody/tr/td[1]";
        public static String ExpenseGrid_lbl_ddlSecondItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[4]/td[1][contains(@style,'140px')]";
        public static String ExpenseGrid_td_Row1DescCell = "//div[@class='x-grid3-body']/div[1]/table/tbody/tr/td[2]";
        public static String ExpenseGrid_txt_Row1DescCell = "//div[contains(@class,'x-grid-editor')]/textarea";

	    #endregion

        #region //########### Create item Page Elements ###########//

        public static String ViewItem_btn_CreateItem = "//a[contains(.,'Create Item')]";

        public static String CreateItem_txt_ItemName = "//input[@id='itemName']";
        public static String CreateItem_txt_ItemNumber = "//input[@id='itemNumber']";
        public static String CreateItem_txt_UnitOfMeasure = "//input[@id='unitOfMeasure']";
        public static String CreateItem_txt_Description = "//textarea[@id='description']";
        public static String CreateItem_txt_SellPrice = "//input[@id='unitPrice']";
        public static String CreateItem_txt_BuyPrice = "//input[@id='unitBuyPrice']";
        public static String CreateItem_ddl_AllocateTo = "//input[@id='tracksales_account_combo_div']/following-sibling::img";
        public static String CreateItem_ddl_BuyAllocateTo = "//input[@id='trackpurchases']/following-sibling::img";
        public static String AllocateTo_lbl_4_2400Sales1 = "//td[contains(.,'4-2400 Sales 1')]";
        public static String AllocateTo_lbl_4_2600Sales2 = "//td[contains(.,'4-2600 Sales 2')]";
        public static String AllocateTo_lbl_OfficeSupplies = "//td[contains(.,'Office supplies')]";
        public static String AllocateTo_lbl_Telephone = "//td[contains(.,'Telephone')]";
        public static String CreateItem_btn_SaveItem = "//a[@id='saveItem']";

	    #endregion

        #region //########### Create contact Page Elements ###########//

        public static String CreateContact_txt_BusinessName = "//input[@id='name']";
        public static String CreateContact_txt_Address = "//input[@id='shippingAddress.streetAddress1']";
        public static String CreateContact_txt_Suburb = "//input[@id='shippingAddress.suburb']";
        public static String CreateContact_txt_State = "//input[@id='shippingAddress.state']";
        public static String CreateContact_txt_PostCode = "//input[@id='shippingAddress.postCode']";
        public static String CreateContact_txt_Country = "//input[@id='shippingAddress.country']";
        public static String CreateContact_btn_SaveAndAddAnother = "//button[@id='new']";

        public static String CreateContact_ddl_ContactIs = "//select[@id='type']";
        public static String ContactIs_lbl_Supplier = "//option[@value='Supplier']";

	    #endregion

        #region //########### Create employee Page Elements ###########//

        public static String Employees_btn_AddEmployee = "//button[contains(.,'Add employee')]";

        public static String CreateEmployee_txt_FirstName = "//input[@id='employeeDetailsForm.firstName']";
        public static String CreateEmployee_txt_LastName = "//input[@id='employeeDetailsForm.lastName']";
        public static String CreateEmployee_txt_StartDate = "//input[@id='employeeDetailsForm.startDate']";
        public static String CreateEmployee_txt_JobTitle = "//input[@id='employeeDetailsForm.jobTitle']";
        public static String CreateEmployee_txt_TFN = "//input[@id='employeeDetailsForm.tfn']";
        public static String CreateEmployee_rdo_AnnualSalary = "//input[@id='annual']";
        public static String CreateEmployee_txt_SalaryRate = "//input[@id='employeeDetailsForm.salaryRate']";
        public static String CreateEmployee_txt_HoursPerWeek = "//input[@id='employeeDetailsForm.hoursPerWeek']";

        public static String CreateEmployee_lnk_TabTax = "//a[@id='tax-tab-link']";
        public static String CreateEmployee_chk_AusResident = "//input[@id='taxAnswersForm.australianResident']";
        public static String CreateEmployee_chk_TaxFreeThresholdClaimed = "//input[@id='taxAnswersForm.taxFreeThresholdClaimed']";
        public static String CreateEmployee_chk_Declaration = "//input[@id='declaration']";
        public static String CreateEmployee_btn_Save = "//button[@id='saveButton']";

	    #endregion

        #region //########### Registration Page Elements ###########//

        public static String Reg_txt_FirstName = "//input[@id='firstName']";
        public static String Reg_txt_SurName = "//input[@id='surname']";
        public static String Reg_txt_Email = "//input[@id='email']";
        public static String Reg_txt_PhoneNo = "//input[@id='phoneNumber']";
        public static String Reg_txt_UserName = "//input[@id='username']";
        public static String Reg_txt_Password = "//input[@id='password']";
        public static String Reg_txt_ConfPassword = "//input[@id='confirmPassword']";
        public static String Reg_txt_BusinessName = "//input[@id='businessDetails.businessName']";
        public static String Reg_txt_Captcha = "//input[@id='recaptcha_response_field']";
        public static String Reg_btn_Save = "//button[@id='registrationButton']";
        public static String Reg_lbl_MsgUserNameAvailability = "//div[@id='availabilityCheckResult'][contains(.,'This user name is not available.')]";

        //Registration done page elements
        public static String RegDone_lbl_HeaderDone = "//h1[contains(.,'Almost done!')]";
        public static String RegDone_lbl_ToComplete = "//div[@class='mainContent bodyContent'][contains(.,'To complete your registration, please click the link in the email ')]";
        
        #endregion

        #region //########### Registration email Page Elements ###########//

        public static String EmailSub_lnk_Welcome = "//a[contains(.,'welcome to myob liveaccounts!')]";
        public static String EmailCont_lnk_ActivateAcc = "//a[@rel='nofollow'][contains(.,'https://your.liveaccounts.com.au/LA/activateAdvisor.html')]";

        //Reg done congratulations page elements
        public static String RegDone_lbl_Congratulations = "//h1[contains(.,'Congratulations')]";

        #endregion

        #region //########### Payroll Page Elements ###########//

        public static String PayrollSetting_ddl_BankAccount = "bankAccountNumber"; //"//select[@id='bankAccountNumber']";
        public static String PayrollSetting_btn_Save = "saveButton"; //"//button[@id='saveButton']";

        //Payroll centre page elements
        public static String PayCentre_ddl_HowOften = "frequency"; //"//select[@id='frequency']";
        public static String PayCentre_txt_FromDate = "periodStart"; //"//input[@id='periodStart']";
        public static String PayCentre_chk_SelectAllEmp = "selectAllCheckbox"; //"//input[@id='selectAllCheckbox']";
        public static String PayCentre_btn_StartPayrun = "startPayrun"; //"//button[@id='startPayrun']";

        //Payrun page elements
        public static String PayRun_btn_ReviewPayslips = "payslipButton"; //"//button[@id='payslipButton']";

        //Review Payslips page elements
        public static String ReviewPayslips_btn_FinishPayrun = "viewPayslipsFinishPayrunTop"; //"//button[@id='viewPayslipsFinishPayrunTop']";

        //Payrun summary page elements
        public static String PayrunSummary_btn_FinishPayrun = "yes"; //"//button[@id='yes']";
        public static String PayrunSummary_btn_PrintPayslipToFinish = "open-print-window"; //"//button[@id='open-print-window']";
        public static String PayrunSummary_btn_PopupPrintSave = "//div[@id='confirm-dialog']/div[@class='ft']/span/button[contains(.,'OK')]";

        #endregion

        #region //########### Spend Money Page Elements ###########//

        public static String SpendMoney_ddl_PayFrom = "//input[@id='top_account_combo_div']/following-sibling::img";
        public static String SpendMoney_txt_Date = "transDateText"; //"//input[@id='transDateText']";
        public static String SpendMoney_ddl_To = "//input[@id='comboId']/following-sibling::img";
        public static String SpendMoney_txt_Notes = "memo"; //"//input[@id='memo']";
        public static String SpendMoney_btn_SaveAndAddAnother = "//a[contains(.,'Save & add another')]";

        #endregion

        #region //########### Receive Money Page Elements ###########//

        public static String ReceiveMoney_ddl_DepositInto = "//input[@id='top_account_combo_div']/following-sibling::img";
        public static String ReceiveMoney_txt_Date = "transDateText"; //"//input[@id='transDateText']";
        public static String ReceiveMoney_txt_Notes = "memo"; //"//input[@id='memo']";
        public static String ReceiveMoney_btn_SaveAndAddAnother = "//a[contains(.,'Save & add another')]";

        #endregion

        #region //########### Bank transactions Page Elements ###########//

        public static String BankTrans_lnk_ImportQif = "//a[contains(.,'Import Bank or Credit Card Statement')]";
        public static String BankTrans_ddl_PopupAccount = "//input[@id='bankAccountCombo']/../img";
        public static String BankTrans_lbl_PopupddlFirstItem = "//div[contains(@class,'x-combo-list-inner')]/div[@id='account-list']/table/tbody/tr[2]/td[1]";
        public static String BankTrans_fl_PopupFileUpload = "uploadInput"; //"//input[@id='uploadInput']";
        public static String BankTrans_btn_PopupImportNow = "//a[contains(.,'Import Now')]";
        public static String BankTrans_btn_PopupImportDoneOk = "//button[@value='Ok']";
        public static String BankTrans_lnk_MngAlocationRules = "//a[contains(.,'Manage allocation rules')]";

        //Allocation Rules page elements
        public static String AlocnRules_btn_CreateAllocationRule = "//a[contains(.,'Create Allocation Rule')]";
        public static String AlocnRules_txt_PopupDesc = "description"; //"//input[@id='description']";
        public static String AlocnRules_td_PopupRow1ItemCell = "//div[@id='mapGrid']/div/div/div/div/div[2]/div/div/table/tbody/tr/td[1]";
        public static String AlocnRules_txt_PopupRow1ItemCell = "//div[@id='mapGrid']/div/div/div/div/div[2]/div[2]/div/input[@type='text']";
        //public static String AlocnRules_ddl_PopupRow1ItemCell = "//div[@id='mapGrid']/div/div/div/div/div[2]/div[2]/div/input[@type='text']/../img";
        public static String AlocnRules_ddl_PopupRow1ItemCell = "//div[@id='account-list']/table/tbody/tr[1]/td";
        public static String AlocnRules_btn_PopupSave = "saveButton"; //"//table[@id='saveButton']";

        #endregion

        
        #region //########### Accounting Page Elements ###########//

        public static String Accounting_btn_EnterOpBalance = "//div[@class='second-top']/div/ul/li/a[contains(.,'Enter Opening Balances')]";
        public static String Accounting_lnk_ChangeOpBalanceDate = "changeOpeningBalanceDateLink"; //"//a[@id='changeOpeningBalanceDateLink']";
        public static String Accounting_txt_NewOpeningBalanceDate = "newOpeningBalanceDate"; //"//input[@id='newOpeningBalanceDate']";
        public static String Accounting_btn_Save = "save-button"; //"//button[@id='save-button']";
        public static String Accounting_btn_Cancel = "cancel-button"; //"//button[@id='cancel-button']";
        public static String Accounting_lbl_CurrentOpeningBalanceDate = "currentOpeningBalanceDate"; //"//div[@id='currentOpeningBalanceDate']";

        #endregion

        #region //########### External objects to verify ###########//
        //Header text 'Expetory' on Twitter page .
        public static String External_lbl_TwitterHeader = "//h1[@class='fullname editable-group']/span[@class='profile-field']";
        
       
        
        #endregion


    }
}
