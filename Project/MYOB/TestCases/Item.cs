﻿using System;
using System.Configuration;
using Framework.Init;
using Framework.PageObjects;
using OpenQA.Selenium;

namespace TestCases
{
    public class Item
    {
        IWebDriver driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Dashboard_btn_logout;


        public void CreateItems()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check create Items functionality.", true, false, true);
            Console.WriteLine("Check create Items functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            ItemObjects objItem = new ItemObjects();
            driver = objItem.CreateItems(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }
    }
}
