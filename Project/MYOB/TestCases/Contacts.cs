﻿using System;
using System.Configuration;
using Framework.Init;
using Framework.PageObjects;
using OpenQA.Selenium;

namespace TestCases
{
    public class Contacts
    {
        IWebDriver driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath"));
        IWebElement Dashboard_btn_logout;

        public void CreateCustomer()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check create Customer functionality.", true, false, true);
            Console.WriteLine("Check create Customer functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            ContactsObjects objContacts = new ContactsObjects();
            driver = objContacts.CreateCustomer(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }

        public void CreateSupplier()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check create Supplier functionality.", true, false, true);
            Console.WriteLine("Check create Supplier functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            ContactsObjects objContacts = new ContactsObjects();
            driver = objContacts.CreateSupplier(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }
    }
}
