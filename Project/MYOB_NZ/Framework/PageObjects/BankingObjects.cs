﻿using System;
using System.Configuration;
using System.Data;
using Framework.Init;
using OpenQA.Selenium;

namespace Framework.PageObjects
{
    public class BankingObjects
    {
        IWebDriver _driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        public BankingObjects()
        {
            if (TestCases.Login.strBusiFlg == "NZ") strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_NZ")); 
            else strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        }
        IWebElement Dashboard_lbl_HeaderWelcome;
        IWebElement Dashboard_btn_logout;
        IWebElement Dashboard_lnk_MenuInvoices;
        IWebElement Dashboard_lnk_MenuExpenses;
        IWebElement Dashboard_lnk_MenuBanking;
        IWebElement Dashboard_lnk_MenuContacts;
        IWebElement Dashboard_lnk_MenuPayroll;
        IWebElement Dashboard_lnk_MenuReports;
        IWebElement Dashboard_lnk_SubMenuSpendMoney;
        IWebElement SpendMoney_ddl_PayFrom;
        IWebElement SpendMoney_txt_Date;
        IWebElement SpendMoney_txt_To;
        IWebElement SpendMoney_ddl_To;
        IWebElement SpendMoney_txt_Notes;
        IWebElement SpendMoney_txt_AllocateTo;
        IWebElement SpendMoney_btn_SaveAndAddAnother;
        IWebElement Dashboard_lnk_SubMenuReceiveMoney;
        IWebElement ReceiveMoney_ddl_DepositInto;
        IWebElement ReceiveMoney_txt_Date;
        IWebElement ReceiveMoney_txt_Notes;
        IWebElement ReceiveMoney_btn_SaveAndAddAnother;
        IWebElement BankTrans_lnk_ImportQif;
        IWebElement BankTrans_ddl_PopupAccount;
        IWebElement BankTrans_fl_PopupFileUpload;
        IWebElement BankTrans_btn_PopupImportNow;
        IWebElement BankTrans_lbl_PopupddlFirstItem;
        IWebElement BankTrans_lnk_MngAlocationRules;
        IWebElement AlocnRules_txt_PopupDesc;
        IWebElement AlocnRules_btn_PopupSave;
        IWebElement ReceiveMoney_txt_Payer;


        public IWebDriver SpendMoney(IWebDriver driver, int intStartingStepNo = 3)
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Spend Money page.", false, true);
            InitDashBoardPageElements();
            if (TestCases.Login.strBusiFlg.Trim() == "NZ") _driver.Navigate().GoToUrl("https://essentials.myob.co.nz/LA.CO.NZ/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/spendMoney");
            else _driver.Navigate().GoToUrl("https://essentials.myob.com.au/LA/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/spendMoney");

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Spend Money page for Banking.", false, true);
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [SpendMoney$] ", strRegDataExcelFilePath);
            string strPayFrom = string.Empty, strDate = string.Empty, strTo = string.Empty, strNotes = string.Empty, strAllocateTo = string.Empty, strDesc = string.Empty, strAmount = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)InitSpendMoneyPageElements();
                else InitSpendMoneyPageElements(false); 
                new Common(_driver).pause(2000);
                strPayFrom = Convert.ToString(dt.Rows[i][0]);
                strDate = Convert.ToString(dt.Rows[i][1]);
                if (strDate.Length > 10) strDate = strDate.Substring(0, strDate.IndexOf(" "));
                strDate = Convert.ToDateTime(strDate).ToString("dd/MM/yyyy");
                if (strDate.IndexOf("-") > 0) strDate = strDate.Replace("-", "/");
            
                strTo = Convert.ToString(dt.Rows[i][2]);
                strNotes = Convert.ToString(dt.Rows[i][3]);
                strAllocateTo = Convert.ToString(dt.Rows[i][4]);
                strDesc = Convert.ToString(dt.Rows[i][5]);
                strAmount = Convert.ToString(dt.Rows[i][6]);

                SpendMoney_ddl_PayFrom.Click();
                try
                {
                    new Common(_driver).pause(1000);
                    string strPayFromItem = "//td[contains(.,'" + strPayFrom + "')]";
                    IWebElement SpendMoney_ddl_PayFromItem = _driver.FindElement(By.XPath(strPayFromItem));
                    SpendMoney_ddl_PayFromItem.Click();
                }
                catch (Exception ex){
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strPayFrom + "' Item name in 'Pay from' combobox on Spend Money Page.");
                }
                new Common(_driver).pause(1000);
                Common.enterText(SpendMoney_txt_Date, strDate);
                //new Common(_driver).pause(2000);

                //SpendMoney_ddl_To.Click();
                string strToItem = String.Empty;
                try
                {
                    Common.enterText(SpendMoney_txt_To, strTo);
                    strToItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'160px')]";
                    IWebElement SpendMoney_ddl_ToItem = _driver.FindElement(By.XPath(strToItem));
                    SpendMoney_ddl_ToItem.Click();
                }
                catch (Exception ex)
                {
                    try
                    {
                        Common.enterText(SpendMoney_txt_To, Keys.Backspace);
                        strToItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'160px')]";
                        IWebElement SpendMoney_ddl_ToItem = _driver.FindElement(By.XPath(strToItem));
                        strTo = SpendMoney_ddl_ToItem.Text;
                        SpendMoney_ddl_ToItem.Click();
                    }
                    catch (Exception ex1)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "'" + strTo + "' Item name in 'To' combobox on Spend Money Page.");
                    }
                    //Report.AddToHtmlReportFailed(_driver, ex, "'" + strCustomer + "' customer name under Invoice to dropdownlist.");
                }
                /*string strToItem = String.Empty;
                try
                {
                    new Common(_driver).pause(1000);
                    strToItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'160px')]";
                    IWebElement SpendMoney_ddl_ToItem = _driver.FindElement(By.XPath(strToItem));
                    strTo = SpendMoney_ddl_ToItem.Text;
                    //Console.WriteLine("strTo:: " + strTo);
                    SpendMoney_ddl_ToItem.Click();
                }
                catch (Exception ex){
                    //Report.AddToHtmlReportFailed(_driver, ex, "'" + strTo + "' Item name in 'To' combobox on Spend Money Page.");
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strToItem.ToString() + "' Item name in 'To' combobox on Spend Money Page.");
                }*/
                new Common(_driver).pause(1000);
                Common.enterText(SpendMoney_txt_Notes, strNotes);


                string strGridAllocateTo = "//div[@id='payment-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[1]";
                string strGridDesc = "//div[@id='payment-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[2]";
                string strGridAmount = "//div[@id='payment-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[3]";

                try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridAllocateTo));
                    IWebElement GridCellAllocateTo = _driver.FindElement(By.XPath(strGridAllocateTo));
                    GridCellAllocateTo.Click();
                    new Common(_driver).pause(1000);

                    string strGridComboHandle = "//input[contains(@class,'x-form-text x-form-field x-form-focus')]/following-sibling::img";
                    new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                    GridComboHandle.Click();
                    new Common(_driver).pause(1000);

                    string strGridAllocateToItem = "//div[@id='account-list']/table/tbody/tr/td[contains(.,'" + strAllocateTo + "')]"; 
                    //string strGridAllocateToItem = "//div[@id='account-list']/table/tbody/tr/td[contains(.,'Fee income')]"; 
                    new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                    IWebElement GridAllocateToItem = _driver.FindElement(By.XPath(strGridAllocateToItem));
                    GridAllocateToItem.Click();

                    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Spend Money grid.");
                }

                /*try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridAllocateTo));
                    IWebElement GridCellAllocateTo = _driver.FindElement(By.XPath(strGridAllocateTo));
                    GridCellAllocateTo.Click();
                    new Common(_driver).pause(400);

                    new Common(_driver).waitForElement(By.XPath(ElementLocators.SpendMoney_txt_AllocateTo));
                    SpendMoney_txt_AllocateTo = _driver.FindElement(By.XPath(ElementLocators.SpendMoney_txt_AllocateTo));
                    //ExpenseGrid_td_Row1ItemCell.Click();
                    Common.enterText(SpendMoney_txt_AllocateTo, strAllocateTo);
                    new Common(_driver).pause(1000);

                    //string strCustXpath = "//div[@id='account-list']/table/tbody/tr[1]/td[1]";
                    string strCustXpath = "//*[@id='account-list']/table/tbody/tr[1]/td";
                    _driver.SwitchTo().DefaultContent();
                    new Common(_driver).waitForElement(By.XPath(strCustXpath));
                    IWebElement SpendMoney_lbl_ddlFirstItem = _driver.FindElement(By.XPath(strCustXpath));
                    new Common(_driver).pause(1000);
                    Report.AddToHtmlReportPassed("'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Spend Money grid.");
                    SpendMoney_lbl_ddlFirstItem.Click();
                }
                catch (Exception ex)
                {
                    try
                    {
                        Common.enterText(SpendMoney_txt_AllocateTo, Keys.Backspace);
                        string strCustXpath = "//div[@id='account-list']/table/tbody/tr[2]/td[1]";
                        
                        new Common(_driver).waitForElement(By.XPath(strCustXpath));
                        IWebElement SpendMoney_lbl_ddlFirstItem = _driver.FindElement(By.XPath(strCustXpath));
                        new Common(_driver).pause(1000);
                        Report.AddToHtmlReportPassed("'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Spend Money grid.");
                        SpendMoney_lbl_ddlFirstItem.Click();
                    }
                    catch (Exception ex1)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex, "'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Spend Money grid.");
                    }
                }*/

                try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridDesc));
                    IWebElement GridDesc = _driver.FindElement(By.XPath(strGridDesc));
                    GridDesc.Click();
                    new Common(_driver).pause(1000);

                    string strGridTxtDesc = "//div[@id='payment-grid']/div/div/div/div/div/div/div/textarea";
                    new Common(_driver).waitForElement(By.XPath(strGridTxtDesc));
                    IWebElement GridTxtDesc = _driver.FindElement(By.XPath(strGridTxtDesc));
                    Common.enterText(GridTxtDesc, strDesc);
                    new Common(_driver).pause(500);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Description' textbox in Spend Money grid.");
                }

                try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridAmount));
                    IWebElement GridAmount = _driver.FindElement(By.XPath(strGridAmount));
                    GridAmount.Click();
                    new Common(_driver).pause(1000);

                    string strGridTxtAmount = "//div[@id='payment-grid']/div/div/div/div/div/div/div/input";
                    new Common(_driver).waitForElement(By.XPath(strGridTxtAmount));
                    IWebElement GridTxtAmount = _driver.FindElement(By.XPath(strGridTxtAmount));
                    Common.enterText(GridTxtAmount, strAmount);
                    GridTxtAmount.SendKeys(Keys.Tab);
                    new Common(_driver).pause(500);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Amount' textbox in Spend Money grid.");
                }

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Pay from: " + strPayFrom, false);
                Report.AddToHtmlReport("Date: " + strDate, false);
                Report.AddToHtmlReport("To: " + strTo, false);
                Report.AddToHtmlReport("Notes: " + strNotes, false);
                Report.AddToHtmlReport("AllocateTo: " + strAllocateTo, false);
                Report.AddToHtmlReport("Description: " + strDesc, false);
                Report.AddToHtmlReport("Amount: " + strAmount + "<br>", false);
                Report.AddToHtmlReport("<hr>");

                SpendMoney_btn_SaveAndAddAnother.Click();
                new Common(_driver).pause(4000);

                //break; //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Remove before deployment @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            }

            return _driver;
        }

        public IWebDriver ReceiveMoney(IWebDriver driver, int intStartingStepNo = 3 )
        {
            _driver = driver;

            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Receive Money page.", false, true);
            InitDashBoardPageElements();
            if (TestCases.Login.strBusiFlg.Trim() == "NZ")
                _driver.Navigate().GoToUrl("https://essentials.myob.co.nz/LA.CO.NZ/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/depositMoney");
            else _driver.Navigate().GoToUrl("https://essentials.myob.com.au/LA/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/depositMoney");
            

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into Receive Money page for Banking.", false, true);
            //read excel
            DataTable dt = Common.ExcelReadData("select * from [ReceiveMoney$] ", strRegDataExcelFilePath);
            string strDepositInto = string.Empty, strDate = string.Empty, strNotes = string.Empty, strGridDate = string.Empty, strChequeNo = string.Empty, strPayer = string.Empty, strAllocateTo = string.Empty, strDesc = string.Empty, strAmount = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)InitReceiveMoneyPageElements();
                else InitReceiveMoneyPageElements(false); 
                new Common(_driver).pause(3000);
                strDepositInto = Convert.ToString(dt.Rows[i][0]);
                strDate = Convert.ToString(dt.Rows[i][1]);
                if (strDate.Length > 10) strDate = strDate.Substring(0, strDate.IndexOf(" "));
                strDate = Convert.ToDateTime(strDate).ToString("dd/MM/yyyy");
                if (strDate.IndexOf("-") > 0) strDate = strDate.Replace("-", "/");
            
                strNotes = Convert.ToString(dt.Rows[i][2]);
                strGridDate = Convert.ToString(dt.Rows[i][3]);
                if (strGridDate.Length > 10) strGridDate = strGridDate.Substring(0, strGridDate.IndexOf(" "));
                strGridDate = Convert.ToDateTime(strGridDate).ToString("dd/MM/yyyy");
                if (strGridDate.IndexOf("-") > 0) strGridDate = strGridDate.Replace("-", "/");

                strChequeNo = Convert.ToString(dt.Rows[i][4]);
                strPayer = Convert.ToString(dt.Rows[i][5]);
                strAllocateTo = Convert.ToString(dt.Rows[i][6]);
                strDesc = Convert.ToString(dt.Rows[i][7]);
                strAmount = Convert.ToString(dt.Rows[i][8]);

                ReceiveMoney_ddl_DepositInto.Click();
                try
                {
                    new Common(_driver).pause(1000);
                    string strDepositIntoItem = "//td[contains(.,'" + strDepositInto + "')]";
                    IWebElement ReceiveMoney_ddl_DepositIntoItem = _driver.FindElement(By.XPath(strDepositIntoItem));
                    ReceiveMoney_ddl_DepositIntoItem.Click();
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strDepositInto + "' Item name in 'Deposit into' combobox on Receive Money Page.");
                }
                new Common(_driver).pause(1000);
                Common.enterText(ReceiveMoney_txt_Date, strDate);
                new Common(_driver).pause(500);

                Common.enterText(ReceiveMoney_txt_Notes, strNotes);

                string strGridDateCell = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[1]";
                string strGridChequeNo = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[2]";
                string strGridPayer = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[3]";
                string strGridAllocateTo = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[4]";
                string strGridDesc = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[5]";
                string strGridAmount = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/div[1]/table/tbody/tr/td[6]";

                try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridDateCell));
                    IWebElement GridDateCell = _driver.FindElement(By.XPath(strGridDateCell));
                    GridDateCell.Click();
                    new Common(_driver).pause(1000);

                    string strGridTxtDate = "//div[@id='deposit-grid']/div/div/div/div/div/div/div[contains(@style,'visible')]/div/input";
                    new Common(_driver).waitForElement(By.XPath(strGridTxtDate));
                    IWebElement GridTxtDate = _driver.FindElement(By.XPath(strGridTxtDate));
                    Common.enterText(GridTxtDate, strGridDate);
                    new Common(_driver).pause(500);
                    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Grid date' textbox in Receive Money grid.");
                }

                try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridChequeNo));
                    IWebElement GridChequeNo = _driver.FindElement(By.XPath(strGridChequeNo));
                    GridChequeNo.Click();
                    new Common(_driver).pause(1000);

                    string strGridTxtChequeNo = "//div[@id='deposit-grid']/div/div/div/div/div/div/div[contains(@style,'visible')]/input";
                    new Common(_driver).waitForElement(By.XPath(strGridTxtChequeNo));
                    IWebElement GridTxtChequeNo = _driver.FindElement(By.XPath(strGridTxtChequeNo));
                    Common.enterText(GridTxtChequeNo, strChequeNo);
                    new Common(_driver).pause(500);
                    GridTxtChequeNo.SendKeys(Keys.Tab);
                    new Common(_driver).pause(500);
                    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Cheque No.' textbox in Receive Money grid.");
                }
                string strGridPayerFirstItem = String.Empty;
                try
                {
                
                    /*IWebElement GridPayer = _driver.FindElement(By.XPath(strGridPayer));
                    GridPayer.Click();
                    new Common(_driver).pause(1000);*/

                    /*string strGridComboHandle = "//input[contains(@id,'comboId')]/following-sibling::img";
                    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                    GridComboHandle.Click();*/
                    ReceiveMoney_txt_Payer = _driver.FindElement(By.Id(ElementLocators.ReceiveMoney_txt_Payer));
                    //Common.enterText(ReceiveMoney_txt_Payer, strPayer);
                    //ReceiveMoney_txt_Payer.Clear();
                    ReceiveMoney_txt_Payer.SendKeys(strPayer);
                    strGridPayerFirstItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'160px')]";
                    IWebElement GridPayerItem = _driver.FindElement(By.XPath(strGridPayerFirstItem));
                    new Common(_driver).pause(1000);
                    ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", GridPayerItem);
                }
                catch (Exception ex1)
                {
                    try
                    {
                        //Common.enterText(ReceiveMoney_txt_Payer, Keys.Backspace);
                        ReceiveMoney_txt_Payer.Clear();
                        ReceiveMoney_txt_Payer.SendKeys(Keys.Backspace);
                        strGridPayerFirstItem = "//div[contains(@class,'x-combo-list-inner')]/div/table/tbody/tr[3]/td[1][contains(@style,'160px')]";
                        IWebElement GridPayerItem = _driver.FindElement(By.XPath(strGridPayerFirstItem));
                        new Common(_driver).pause(1000);
                        ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", GridPayerItem);
                    }
                    catch (Exception ex2)
                    {
                        Report.AddToHtmlReportFailed(_driver, ex2, "'" + strGridPayerFirstItem.ToString() + "' Item name in Payer dropdownlist in Receive Money grid.");
                    }
                }
                    
                IWebElement GridPayerItemText = _driver.FindElement(By.XPath("//input[@id='comboId']"));
                strPayer = GridPayerItemText.Text;
                new Common(_driver).pause(1000);
                
                try
                {
                    //new Common(_driver).waitForElement(By.XPath(strGridAllocateTo));
                    IWebElement GridCellAllocateTo = _driver.FindElement(By.XPath(strGridAllocateTo));
                    GridCellAllocateTo.Click();
                    new Common(_driver).pause(500);

                    string strGridComboHandle = "//input[contains(@class,'x-form-text x-form-field x-form-focus')]/following-sibling::img";
                    //new Common(_driver).waitForElement(By.XPath(strGridComboHandle));
                    IWebElement GridComboHandle = _driver.FindElement(By.XPath(strGridComboHandle));
                    GridComboHandle.Click();
                    new Common(_driver).pause(1000);

                    string strGridAllocateToItem = "//div[@id='account-list']/table/tbody/tr/td[contains(.,'" + strAllocateTo + "')]";
                    //new Common(_driver).waitForElement(By.XPath(strGridAllocateToItem));
                    IWebElement GridAllocateToItem = _driver.FindElement(By.XPath(strGridAllocateToItem));
                    ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", GridAllocateToItem);
                    //GridAllocateToItem.Click();
                    new Common(_driver).pause(1000);
                    //Report.AddToHtmlReportPassed("'WM324' Item name in Item dropdownlist under row-1 cell-1.");
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'" + strAllocateTo + "' Item name in Allocate to dropdownlist in Receive Money grid.");
                }

                try
                {
                    new Common(_driver).waitForElement(By.XPath(strGridDesc));
                    IWebElement GridDesc = _driver.FindElement(By.XPath(strGridDesc));
                    GridDesc.Click();
                    new Common(_driver).pause(500);

                    string strGridTxtDesc = "//div[@id='deposit-grid']/div/div/div/div/div/div/div/textarea";
                    new Common(_driver).waitForElement(By.XPath(strGridTxtDesc));
                    IWebElement GridTxtDesc = _driver.FindElement(By.XPath(strGridTxtDesc));
                    Common.enterText(GridTxtDesc, strDesc);
                    new Common(_driver).pause(500);
                    GridTxtDesc.SendKeys(Keys.Tab);
                    new Common(_driver).pause(500);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Description' textbox in Receive Money grid.");
                }

                try
                {
                    //new Common(_driver).waitForElement(By.XPath(strGridAmount));
                    //IWebElement GridAmount = _driver.FindElement(By.XPath(strGridAmount));
                    //GridAmount.Click();
                    //new Common(_driver).pause(2000);

                    string strGridTxtAmount = "//div[@id='deposit-grid']/div/div/div/div/div/div/div[contains(@style,'visible')]/input";
                    //new Common(_driver).waitForElement(By.XPath(strGridTxtAmount));
                    IWebElement GridTxtAmount = _driver.FindElement(By.XPath(strGridTxtAmount));
                    Common.enterText(GridTxtAmount, strAmount);
                    GridTxtAmount.SendKeys(Keys.Tab);
                    new Common(_driver).pause(500);
                }
                catch (Exception ex)
                {
                    Report.AddToHtmlReportFailed(_driver, ex, "'Amount' textbox in Receive Money grid.");
                }

                Report.AddToHtmlReport("<br>Data Entered", false, false, true);
                Report.AddToHtmlReport("Deposit into: " + strDepositInto, false);
                Report.AddToHtmlReport("Date: " + strDate, false);
                Report.AddToHtmlReport("Notes: " + strNotes, false);
                Report.AddToHtmlReport("Grid date: " + strGridDate, false);
                Report.AddToHtmlReport("Cheque No.: " + strChequeNo, false);
                Report.AddToHtmlReport("Payer: " + strPayer, false);
                Report.AddToHtmlReport("AllocateTo: " + strAllocateTo, false);
                Report.AddToHtmlReport("Description: " + strDesc, false);
                Report.AddToHtmlReport("Amount: " + strAmount + "<br>", false);
                Report.AddToHtmlReport("<hr>");

                ReceiveMoney_btn_SaveAndAddAnother.Click();
                new Common(_driver).pause(4000);

                //break; //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Remove before deployment @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            }

            return _driver;
        }

        public IWebDriver BankTransactions(IWebDriver driver, int intStartingStepNo = 3 )
        {
            _driver = driver;
            
            //Step-3
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Open Bank transactions page.", false, true);
            InitDashBoardPageElements();
            if (TestCases.Login.strBusiFlg.Trim() == "NZ") _driver.Navigate().GoToUrl("https://essentials.myob.co.nz/LA.CO.NZ/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/reconcileTransactions");
            else _driver.Navigate().GoToUrl("https://essentials.myob.com.au/LA/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/reconcileTransactions");

            try
            {
                new Common(_driver).pause(4000);
                _driver.SwitchTo().Frame((IWebElement)_driver.FindElement(By.XPath("//iframe")));
                new Common(_driver).waitForElement(By.XPath(ElementLocators.BankTrans_lnk_ImportQif));
                BankTrans_lnk_ImportQif = _driver.FindElement(By.XPath(ElementLocators.BankTrans_lnk_ImportQif));
                BankTrans_lnk_ImportQif.Click();
                Report.AddToHtmlReportPassed("'Import Bank or Credit Card Statement' link button on Bank transactions Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Import Bank or Credit Card Statement' link button on Bank transactions Page.");
            }

            //Step-4
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Click on 'Import Bank or Credit Card Statement' link.", false, true);
            InitImportQifFilePopupElements();

            //Step-5
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Select an account, Select Statement file and click on 'Import Now' button.", false, true);
            BankTrans_ddl_PopupAccount.Click();
            new Common(_driver).pause(1000);
            string strAccountName = string.Empty;
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.BankTrans_lbl_PopupddlFirstItem));
                BankTrans_lbl_PopupddlFirstItem = _driver.FindElement(By.XPath(ElementLocators.BankTrans_lbl_PopupddlFirstItem));
                new Common(_driver).pause(500);
                strAccountName = BankTrans_lbl_PopupddlFirstItem.Text;
                Report.AddToHtmlReportPassed("'" + strAccountName + "' Account name under 'Bank/Credit Card Account' dropdownlist.");
                BankTrans_lbl_PopupddlFirstItem.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'" + strAccountName + "' Account name under 'Bank/Credit Card Account' dropdownlist.");
            }
            new Common(_driver).pause(2000);
            //BankTrans_fl_PopupFileUpload.Click();
            string strFilePath = "C:\\MYOB_RuntimeDocs\\DEMOBUSINESSBANKACCT.qif";
            if (TestCases.Login.strBusiFlg == "NZ") strFilePath = "C:\\MYOB_RuntimeDocs\\BankFeedsNZ.qif";
            else strRegDataExcelFilePath = strFilePath = "C:\\MYOB_RuntimeDocs\\BankFeedsAU.qif";
            BankTrans_fl_PopupFileUpload.SendKeys(strFilePath);
            new Common(_driver).pause(1000);

            Report.AddToHtmlReport("<br>Data Entered", false, false, true);
            Report.AddToHtmlReport("Bank/Credit Card Account: " + strAccountName, false);
            Report.AddToHtmlReport("Statement file: " + strFilePath + "<br>", false);

            BankTrans_btn_PopupImportNow.Click();
            new Common(_driver).pause(10000);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.BankTrans_btn_PopupImportDoneOk));
                IWebElement BankTrans_btn_PopupImportDoneOk = _driver.FindElement(By.XPath(ElementLocators.BankTrans_btn_PopupImportDoneOk));
                Report.AddToHtmlReportPassed("'Ok' button on 'Bank Statement Import' acknowledgement popup panel.");
                BankTrans_btn_PopupImportDoneOk.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Ok' button on 'Bank Statement Import' acknowledgement popup panel.");
            }

            //Step-6
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Click on 'Manage allocation rules' link on Bank transactions page.", false, true);
            if (TestCases.Login.strBusiFlg.Trim() == "NZ") _driver.Navigate().GoToUrl("https://essentials.myob.co.nz/LA.CO.NZ/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/allocationRules");
            else _driver.Navigate().GoToUrl("https://essentials.myob.com.au/LA/app.htm#businesses/" + TestCases.Login.strURLDynCode + "/banking/allocationRules");

            try
            {
                new Common(_driver).pause(4000);
                _driver.SwitchTo().Frame((IWebElement)_driver.FindElement(By.XPath("//iframe")));
                new Common(_driver).waitForElement(By.XPath(ElementLocators.BankTrans_lnk_MngAlocationRules));
                BankTrans_lnk_MngAlocationRules = _driver.FindElement(By.XPath(ElementLocators.BankTrans_lnk_MngAlocationRules));
                Report.AddToHtmlReportPassed("'Create Allocation Rule' link on Bank transactions page.");
                BankTrans_lnk_MngAlocationRules.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Create Allocation Rule' link on Bank transactions page.");
            }

            //Step-7
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Click on 'Create Allocation Rule' button on Allocation Rules page.", false, true);
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.AlocnRules_btn_CreateAllocationRule));
                IWebElement AlocnRules_btn_CreateAllocationRule = _driver.FindElement(By.XPath(ElementLocators.AlocnRules_btn_CreateAllocationRule));
                Report.AddToHtmlReportPassed("'Create Allocation Rule' button on Allocation Rules page.");
                AlocnRules_btn_CreateAllocationRule.Click();
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Create Allocation Rule' button on Allocation Rules page.");
            }

            //Step-8
            Report.AddToHtmlReport("Step-" + (intStartingStepNo++) + ": Enter data into 'bank transaction description contains' textbox, Select Income/Expense account and click on 'Save' button on 'Create Allocation Rule' popup panel.", false, true);
            InitCreateAllocationRulePopupElements();
            string strDesc = "Rent";
            Common.enterText(AlocnRules_txt_PopupDesc, strDesc);
            new Common(_driver).pause(500);
            string strGridAccountName = string.Empty;
            try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.AlocnRules_td_PopupRow1ItemCell));
                IWebElement AlocnRules_td_PopupRow1ItemCell = _driver.FindElement(By.XPath(ElementLocators.AlocnRules_td_PopupRow1ItemCell));
                AlocnRules_td_PopupRow1ItemCell.Click();
                new Common(_driver).pause(1000);

                new Common(_driver).waitForElement(By.XPath(ElementLocators.AlocnRules_txt_PopupRow1ItemCell));
                IWebElement AlocnRules_txt_PopupRow1ItemCell = _driver.FindElement(By.XPath(ElementLocators.AlocnRules_txt_PopupRow1ItemCell));
                Common.enterText(AlocnRules_txt_PopupRow1ItemCell, "rent");
                new Common(_driver).pause(1000);

                new Common(_driver).waitForElement(By.XPath(ElementLocators.AlocnRules_ddl_PopupRow1ItemCell));
                IWebElement AlocnRules_ddl_PopupRow1ItemCell = _driver.FindElement(By.XPath(ElementLocators.AlocnRules_ddl_PopupRow1ItemCell));
                new Common(_driver).pause(1000);
                strGridAccountName = AlocnRules_ddl_PopupRow1ItemCell.Text;
                AlocnRules_ddl_PopupRow1ItemCell.Click();
                Report.AddToHtmlReportPassed("'" + strGridAccountName + "' Item name in Account dropdownlist under grid column-1.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'" + strGridAccountName + "' Item name in Account dropdownlist under grid column-1.");
            }
            AlocnRules_btn_PopupSave.Click();

            Report.AddToHtmlReport("<br>Data Entered", false, false, true);
            Report.AddToHtmlReport("Bank transaction description: " + strDesc, false);
            Report.AddToHtmlReport("Account: " + strGridAccountName + "<br>", false);

            new Common(_driver).pause(2000);

            return _driver;
        }

        private void InitCreateAllocationRulePopupElements()
        {
            try
            {
                new Common(_driver).waitForElement(By.Id(ElementLocators.AlocnRules_txt_PopupDesc));
                AlocnRules_txt_PopupDesc = _driver.FindElement(By.Id(ElementLocators.AlocnRules_txt_PopupDesc));
                Report.AddToHtmlReportPassed("'Bank transaction description contains' textbox on Create Allocation Rule popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Bank transaction description contains' textbox on Create Allocation Rule popup panel.");
            }
            try
            {
                AlocnRules_btn_PopupSave = _driver.FindElement(By.XPath(ElementLocators.AlocnRules_btn_PopupSave));
                Report.AddToHtmlReportPassed("'Save' button on Create Allocation Rule popup panel.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Save' button on Create Allocation Rule popup panel.");
            }
        }

        private void InitImportQifFilePopupElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).pause(1000);
                new Common(_driver).waitForElement(By.XPath(ElementLocators.BankTrans_ddl_PopupAccount));
                BankTrans_ddl_PopupAccount = _driver.FindElement(By.XPath(ElementLocators.BankTrans_ddl_PopupAccount));
                Report.AddToHtmlReportPassed("'Bank/Credit Card Account' dropdownlist on Import statement popup.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Bank/Credit Card Account' dropdownlist on Import statement popup.");
            }
            try
            {
                BankTrans_fl_PopupFileUpload = _driver.FindElement(By.Id(ElementLocators.BankTrans_fl_PopupFileUpload));
                Report.AddToHtmlReportPassed("'Statement file' Browse button on Import statement popup.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Statement file' Browse button on Import statement popup.");
            }
            try
            {
                BankTrans_btn_PopupImportNow = _driver.FindElement(By.XPath(ElementLocators.BankTrans_btn_PopupImportNow));
                Report.AddToHtmlReportPassed("'Import Now' button on Import statement popup.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'Import Now' button on Import statement popup.");
            }
        }

        private void InitReceiveMoneyPageElements(bool flgShowInReport = true)
        {
 	        try
            {
                new Common(_driver).pause(5000);
                _driver.SwitchTo().Frame((IWebElement)_driver.FindElement(By.XPath("//iframe")));
                new Common(_driver).waitForElement(By.XPath(ElementLocators.ReceiveMoney_ddl_DepositInto));
                ReceiveMoney_ddl_DepositInto = _driver.FindElement(By.XPath(ElementLocators.ReceiveMoney_ddl_DepositInto));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Deposit into' combobox on Receive Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Deposit into' combobox on Receive Money Page.");
            }
            try
            {
                ReceiveMoney_txt_Date = _driver.FindElement(By.Id(ElementLocators.ReceiveMoney_txt_Date));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Date' textbox on Receive Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Date' textbox on Receive Money Page.");
            }
            try
            {
                ReceiveMoney_txt_Notes = _driver.FindElement(By.Id(ElementLocators.ReceiveMoney_txt_Notes));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Notes' textbox on Receive Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Notes' textbox on Receive Money Page.");
            }
            try
            {
                ReceiveMoney_btn_SaveAndAddAnother = _driver.FindElement(By.Id(ElementLocators.ReceiveMoney_btn_SaveAndAddAnother));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save & add another' button on Receive Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save & add another' button on Receive Money Page.");
            }
        } 
        private void InitSpendMoneyPageElements(bool flgShowInReport = true)
        {
            try
            {
                new Common(_driver).pause(5000);
                _driver.SwitchTo().Frame((IWebElement)_driver.FindElement(By.XPath("//iframe")));
                new Common(_driver).waitForElement(By.XPath(ElementLocators.SpendMoney_ddl_PayFrom));
                SpendMoney_ddl_PayFrom = _driver.FindElement(By.XPath(ElementLocators.SpendMoney_ddl_PayFrom));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Pay from' combobox on Spend Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Pay from' combobox on Spend Money Page.");
            }
            try
            {
                SpendMoney_txt_To = _driver.FindElement(By.Id(ElementLocators.SpendMoney_txt_To));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'To' textbox in Spend Money Page.");
            }
            catch (Exception ex)
            {
                Report.AddToHtmlReportFailed(_driver, ex, "'To' textbox in Spend Money Page.");
            }
            try
            {
                SpendMoney_txt_Date = _driver.FindElement(By.Id(ElementLocators.SpendMoney_txt_Date));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Date' textbox on Spend Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Date' textbox on Spend Money Page.");
            }
            try
            {
                SpendMoney_ddl_To = _driver.FindElement(By.XPath(ElementLocators.SpendMoney_ddl_To));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'To' combobox on Spend Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'To' combobox on Spend Money Page.");
            }
            try
            {
                SpendMoney_txt_Notes = _driver.FindElement(By.Id(ElementLocators.SpendMoney_txt_Notes));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Notes' textbox on Spend Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Notes' textbox on Spend Money Page.");
            }
            try
            {
                SpendMoney_btn_SaveAndAddAnother = _driver.FindElement(By.Id(ElementLocators.SpendMoney_btn_SaveAndAddAnother));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Save & add another' button on Spend Money Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Save & add another' button on Spend Money Page.");
            }
        }

        private void InitDashBoardPageElements(bool flgShowInReport = true)
        {
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                Dashboard_lbl_HeaderWelcome = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lbl_HeaderWelcome));
                if (flgShowInReport) Report.AddToHtmlReportPassed("Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "Header Text 'Welcome to MYOB LiveAccounts' on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuInvoices = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuInvoices));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Invoices' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Invoices' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuExpenses = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuExpenses));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Expenses' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Expenses' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuBanking = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuBanking));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Banking' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Banking' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuContacts = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuContacts));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Contacts' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Contacts' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuPayroll = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuPayroll));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Payroll' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Payroll' menu button on Dashboard Page.");
            }
            try
            {
                Dashboard_lnk_MenuReports = _driver.FindElement(By.XPath(ElementLocators.Dashboard_lnk_MenuReports));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Reports' menu button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Reports' menu button on Dashboard Page.");
            }*/
            /*try
            {
                new Common(_driver).waitForElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout = _driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                if (flgShowInReport) Report.AddToHtmlReportPassed("'Logout' button on Dashboard Page.");
            }
            catch (Exception ex)
            {
                //if (flgShowInReport) Report.AddToHtmlReportFailed(_driver, ex, "'Logout' button on Dashboard Page.");
            }*/
        }
    }
}
