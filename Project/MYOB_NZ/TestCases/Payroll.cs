﻿using System;
using System.Configuration;
using Framework.Init;
using Framework.PageObjects;
using OpenQA.Selenium;

namespace TestCases
{
    public class Payroll
    {
        IWebDriver driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        public Payroll()
        {
            if (TestCases.Login.strBusiFlg == "NZ") strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_NZ")); 
            else strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        }
        IWebElement Dashboard_btn_logout;

        public void Payruns()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check Payruns functionality.", true, false, true);
            Console.WriteLine("Check Payruns functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            PayrollObjects objPayroll = new PayrollObjects();
            driver = objPayroll.Payruns(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }
    }
}
