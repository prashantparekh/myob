﻿using System;
using System.Configuration;
using Framework.Init;
using Framework.PageObjects;
using OpenQA.Selenium;

namespace TestCases
{
    public class Employee
    {
        IWebDriver driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        public Employee()
        {
            if (TestCases.Login.strBusiFlg == "NZ") strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_NZ")); 
            else strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        }
        IWebElement Dashboard_btn_logout;


        public void CreateEmployee()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check create Employee functionality.", true, false, true);
            Console.WriteLine("Check create Employee functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            EmployeeObjects objEmployee = new EmployeeObjects();
            driver = objEmployee.CreateEmployee(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }


    }
}
