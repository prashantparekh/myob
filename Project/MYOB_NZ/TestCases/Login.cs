﻿using System;
using System.Configuration;
using System.Data;
using Framework.Init;
using Framework.PageObjects;
using OpenQA.Selenium;
using System.Threading;

namespace TestCases
{
    public class Login
    {

        //public bool IsTCAdded = false;
        IWebDriver driver;
        string strProjUrl = Convert.ToString(ConfigurationSettings.AppSettings.Get("ProjectUrl"));
        public static string strBusiFlg = Convert.ToString(ConfigurationSettings.AppSettings.Get("UseBusiness"));
        string strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        public Login()
        {
            if (TestCases.Login.strBusiFlg == "NZ") strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_NZ")); 
            else strRegDataExcelFilePath = Convert.ToString(ConfigurationSettings.AppSettings.Get("RegDataExcelFilePath_AU"));
        }
        IWebElement Login_lbl_Header;
        IWebElement Login_txt_Email;
        IWebElement Login_txt_EmailErrMsg;
        IWebElement Login_txt_Password;
        IWebElement Login_btn_Login;
        IWebElement Login_lnk_ForgetYourPassword;
        IWebElement Login_lbl_Header_ForgetPasswordEmailPopup;
        IWebElement Login_txt_Email_ForgetPasswordEmailPopup;
        IWebElement Login_btn_Submit_ForgetPasswordEmailPopup;

        IWebElement Dashboard_HelloPopup_btn_Close;
        IWebElement Dashboard_HelloPopup_btn_GoToMyDashBoard;
        IWebElement Dashboard_BoxPanel_Headers;

        IWebElement ForgotPassword_lbl_Header;
        IWebElement ForgotPassword_txt_NewPassword;
        IWebElement ForgotPassword_txt_ConfirmPassword;
        IWebElement ForgotPassword_btn_ResetPassword;
        IWebElement Dashboard_btn_logout;
        public static string strURLDynCode = string.Empty;

        public static Boolean IsSetupAssiPopupOpen = false;

        public void CreateInvoice()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check create invoice functionality.", true, false, true);
            Console.WriteLine("Check create invoice functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            driver = objLogin.CreateInvoice(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }

        public void EnterExpense()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check enter expense functionality.", true, false, true);
            Console.WriteLine("Check enter expense functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            driver = objLogin.EnterExpense(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }

        public void CreateCustReturn()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check Customer return functionality.", true, false, true);
            Console.WriteLine("Check Customer return functionality.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithValidDataWOlogout(driver);

            driver = objLogin.CreateCustReturn(driver);


            try
            {
                Dashboard_btn_logout = driver.FindElement(By.XPath(ElementLocators.Dashboard_btn_logout));
                Dashboard_btn_logout.Click();
                new Common(driver).pause(5000);
            }
            catch (Exception ex) { }

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }

        }

        public void UserRegistration()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check user Sign up Functionality (read data from excel).", true, false, true);
            Console.WriteLine("Check user Sign up Functionality (read data from excel).");
            LoginObjects objLogin = new LoginObjects();
            DataTable dt = Common.ExcelReadData("select * from [SignupConfig$] ", strRegDataExcelFilePath);
            if (dt.Rows.Count > 0)
            {
                driver = objLogin.UserRegistration(driver, dt);
            }
            else
            {
                Console.WriteLine("No pending data found in excel.");
                Report.AddToHtmlReportPassed("No pending data found in excel.");
            }

        }

        public void LoginBlankData()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            //Report.AddToHtmlReport("TC_119: Check Login button functionality with blank Email Address and password.", true, false, true);
            //Console.WriteLine("TC_119: Check Login button functionality with blank Email Address and password.");
            LoginObjects objLogin = new LoginObjects();
            driver = objLogin.LoginWithBlankData(driver);

            if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
            {
                Browser.CloseBrowser(driver);
            }
        }

        public void SignUpWithAllDemoBusiness()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Create new business for advisor with demo business.", true, false, true);
            Console.WriteLine("Create new business for advisor with demo business.");

            string strUserName = string.Empty;
            string strPwd = string.Empty;
            string strURL = string.Empty;
            LoginObjects objLogin = new LoginObjects();
            DataTable dt = Common.ExcelReadData("select * from [DemoBusiness$] where [status] = 'Pending' ", strRegDataExcelFilePath);
            int intRecCnt = 0;
            //ItemObjects objItem = new ItemObjects();
            //Thread oThread = new Thread(new ThreadStart(objItem.CreateItems(driver, 1)));
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Report.AddToHtmlReport("Demo Business Entry No.: " + (i + 1) + "<br>", false, true);
                    strURL = Convert.ToString(dt.Rows[i][0]);
                    strUserName = Convert.ToString(dt.Rows[i][1]);
                    strPwd = Convert.ToString(dt.Rows[i][2]);
                    intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='In-progress' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);

                    //driver = objLogin.SignUpWithAllDemoBusiness(driver, dt);
                    driver = objLogin.SignUpWithAllDemoBusiness(driver, strURL, strUserName, strPwd);
                    if (TestCases.Login.IsSetupAssiPopupOpen)
                    {
                        //Create Items #####
                        Report.AddToHtmlReport("<br>Create Items process.", false, true, true);
                        Console.WriteLine("Create Items process.");
                        ItemObjects objItem = new ItemObjects();
                        driver = objItem.CreateItems(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Customers #####
                        Report.AddToHtmlReport("<br>Create Customer process.", false, true, true);
                        Console.WriteLine("Create Customer process.");
                        ContactsObjects objCustomer = new ContactsObjects();
                        driver = objCustomer.CreateCustomer(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Suppliers #####
                        Report.AddToHtmlReport("<br>Create Supplier process.", false, true, true);
                        Console.WriteLine("Create Supplier process.");
                        ContactsObjects objSupplier = new ContactsObjects();
                        driver = objSupplier.CreateSupplier(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Invoice #####
                        Report.AddToHtmlReport("<br>Create Invoice process.", false, true, true);
                        Console.WriteLine("Create Invoice process.");
                        LoginObjects objCreateInvoice = new LoginObjects();
                        driver = objCreateInvoice.CreateInvoice(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Quotes
                        Report.AddToHtmlReport("<br>Create Quote process.", false, true, true);
                        Console.WriteLine("Create Quote process.");
                        LoginObjects objCreateQuote = new LoginObjects();
                        driver = objCreateQuote.CreateQuote(driver, 1);
                        new Common(driver).pause(2000);

                        ////Customer return ##### Now no need to process (As per call with VAR on 18Mar2014)
                        //Report.AddToHtmlReport("<br>Customer return process.", false, true, true);
                        //Console.WriteLine("Customer return process.");
                        //LoginObjects objCreateCustReturn = new LoginObjects();
                        //driver = objCreateCustReturn.CreateCustReturn(driver, 1);
                        //new Common(driver).pause(1000);

                        //Enter Expense #####
                        Report.AddToHtmlReport("<br>Enter Expense process.", false, true, true);
                        Console.WriteLine("Enter Expense process.");
                        LoginObjects objEnterExpense = new LoginObjects();
                        driver = objEnterExpense.EnterExpense(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Employee #####
                        Report.AddToHtmlReport("<br>Create Employee process.", false, true, true);
                        Console.WriteLine("Create Employee process.");
                        EmployeeObjects objEmployee = new EmployeeObjects();
                        driver = objEmployee.CreateEmployee(driver, 1);
                        new Common(driver).pause(1000);

                        //Generate Payruns #####
                        Report.AddToHtmlReport("<br>Generate Payruns process.", false, true, true);
                        Console.WriteLine("Generate Payruns process.");
                        PayrollObjects objPayroll = new PayrollObjects();
                        driver = objPayroll.Payruns(driver, 1);
                        new Common(driver).pause(1000);

                        //Spend Money #####
                        Report.AddToHtmlReport("<br>Generate Spend Money process.", false, true, true);
                        Console.WriteLine("Generate Spend Money process.");
                        BankingObjects objSpendMoney = new BankingObjects();
                        driver = objSpendMoney.SpendMoney(driver, 1);
                        new Common(driver).pause(1000);

                        //Receive Money #####
                        Report.AddToHtmlReport("<br>Generate Receive Money process.", false, true, true);
                        Console.WriteLine("Generate Receive Money process.");
                        BankingObjects objReceiveMoney = new BankingObjects();
                        driver = objReceiveMoney.ReceiveMoney(driver, 1);
                        new Common(driver).pause(1000);

                        //Receive Money #####
                        Report.AddToHtmlReport("<br>Import Bank Transactions from .qif file process.", false, true, true);
                        Console.WriteLine("Import Bank Transactions from .qif file process.");
                        BankingObjects objBankTransactions = new BankingObjects();
                        driver = objBankTransactions.BankTransactions(driver, 1);
                        new Common(driver).pause(1000);

                        intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='Done' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);
                    }

                    else
                    {
                        intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='Skipped' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);
                    }

                    //dt = Common.ExcelReadData("select * from [DemoBusiness$] where [status] = 'Pending' ", strRegDataExcelFilePath);

                    try
                    {
                        new Common(driver).pause(3000);
                        driver.SwitchTo().DefaultContent();
                        IWebElement Dashboard_ddl_DemoBusiness = driver.FindElement(By.XPath(ElementLocators.Dashboard_ddl_DemoBusiness));
                        Dashboard_ddl_DemoBusiness.Click();
                        new Common(driver).pause(1000);
                        Dashboard_btn_logout = driver.FindElement(By.Id(ElementLocators.Dashboard_btn_logout));
                        Dashboard_btn_logout.Click();
                        new Common(driver).pause(3000);
                    }
                    catch (Exception ex) { Console.WriteLine("77"); }
                    if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
                    {
                        Browser.CloseBrowser(driver);
                    }

                    Report.AddToHtmlReport("<hr><br><br>");
                    //break; //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Remove before deployment @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                }
            }
            else
            {
                Console.WriteLine("No pending data found in excel.");
                Report.AddToHtmlReportPassed("No pending data found in excel.");
            }
        }

        public void SignUpWithAllDemoBusiness1()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Create new business for advisor with demo business.", true, false, true);
            Console.WriteLine("Create new business for advisor with demo business.");

            string strUserName = string.Empty;
            string strPwd = string.Empty;
            string strURL = string.Empty;
            LoginObjects objLogin = new LoginObjects();
            DataTable dt = Common.ExcelReadData("select * from [DemoBusiness$] where [status] = 'Pending' ", strRegDataExcelFilePath);
            int intRecCnt = 0;
            //ItemObjects objItem = new ItemObjects();
            //Thread oThread = new Thread(new ThreadStart(objItem.CreateItems(driver, 1)));
            if (dt.Rows.Count > 0)
            {
                //for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Report.AddToHtmlReport("Demo Business Entry No.: " + (i + 1) + "<br>", false, true);
                    strURL = Convert.ToString(dt.Rows[0][0]);
                    strUserName = Convert.ToString(dt.Rows[0][1]);
                    strPwd = Convert.ToString(dt.Rows[0][2]);
                    intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='In-progress' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);

                    //driver = objLogin.SignUpWithAllDemoBusiness(driver, dt);
                    driver = objLogin.SignUpWithAllDemoBusiness(driver, strURL, strUserName, strPwd);
                    if (TestCases.Login.IsSetupAssiPopupOpen)
                    {
                        ////Create Items #####
                        //Report.AddToHtmlReport("<br>Create Items process.", false, true, true);
                        //Console.WriteLine("Create Items process.");
                        //ItemObjects objItem = new ItemObjects();
                        //driver = objItem.CreateItems(driver, 1);
                        //new Common(driver).pause(2000);

                        //////Create Customers #####
                        //Report.AddToHtmlReport("<br>Create Customer process.", false, true, true);
                        //Console.WriteLine("Create Customer process.");
                        //ContactsObjects objCustomer = new ContactsObjects();
                        //driver = objCustomer.CreateCustomer(driver, 1);
                        //new Common(driver).pause(2000);

                        //////Create Suppliers #####
                        //Report.AddToHtmlReport("<br>Create Supplier process.", false, true, true);
                        //Console.WriteLine("Create Supplier process.");
                        //ContactsObjects objSupplier = new ContactsObjects();
                        //driver = objSupplier.CreateSupplier(driver, 1);
                        //new Common(driver).pause(8000);

                        ////Create Invoice #####
                        //Report.AddToHtmlReport("<br>Create Invoice process.", false, true, true);
                        //Console.WriteLine("Create Invoice process.");
                        //LoginObjects objCreateInvoice = new LoginObjects();
                        //driver = objCreateInvoice.CreateInvoice(driver, 1);
                        //new Common(driver).pause(2000);

                        ////Create Quotes 
                        //Report.AddToHtmlReport("<br>Create Quote process.", false, true, true);
                        //Console.WriteLine("Create Quote process.");
                        //LoginObjects objCreateQuote = new LoginObjects();
                        //driver = objCreateQuote.CreateQuote(driver, 1);
                        //new Common(driver).pause(2000);

                        ////Customer return ##### Now no need to process (As per call with VAR on 18Mar2014)*****************************************************
                        //Report.AddToHtmlReport("<br>Customer return process.", false, true, true);
                        //Console.WriteLine("Customer return process.");
                        //LoginObjects objCreateCustReturn = new LoginObjects();
                        //driver = objCreateCustReturn.CreateCustReturn(driver, 1);
                        //new Common(driver).pause(2000);

                        ////Enter Expense #####
                        //Report.AddToHtmlReport("<br>Enter Expense process.", false, true, true);
                        //Console.WriteLine("Enter Expense process.");
                        //LoginObjects objEnterExpense = new LoginObjects();
                        //driver = objEnterExpense.EnterExpense(driver, 1);
                        //new Common(driver).pause(4000);

                        ////Create Employee #####
                        //Report.AddToHtmlReport("<br>Create Employee process.", false, true, true);
                        //Console.WriteLine("Create Employee process.");
                        //EmployeeObjects objEmployee = new EmployeeObjects();
                        //driver = objEmployee.CreateEmployee(driver, 1);
                        //new Common(driver).pause(2000);

                        //Generate Payruns #####
                        Report.AddToHtmlReport("<br>Generate Payruns process.", false, true, true);
                        Console.WriteLine("Generate Payruns process.");
                        PayrollObjects objPayroll = new PayrollObjects();
                        driver = objPayroll.Payruns(driver, 1);
                        new Common(driver).pause(2000);

                        ////Spend Money #####
                        //Report.AddToHtmlReport("<br>Generate Spend Money process.", false, true, true);
                        //Console.WriteLine("Generate Spend Money process.");
                        //BankingObjects objSpendMoney = new BankingObjects();
                        //driver = objSpendMoney.SpendMoney(driver, 1);
                        //new Common(driver).pause(2000);

                        ////Receive Money #####
                        //Report.AddToHtmlReport("<br>Generate Receive Money process.", false, true, true);
                        //Console.WriteLine("Generate Receive Money process.");
                        //BankingObjects objReceiveMoney = new BankingObjects();
                        //driver = objReceiveMoney.ReceiveMoney(driver, 1);
                        //new Common(driver).pause(2000);

                        ////Bank Transactions #####
                        //Report.AddToHtmlReport("<br>Import Bank Transactions from .qif file process.", false, true, true);
                        //Console.WriteLine("Import Bank Transactions from .qif file process.");
                        //BankingObjects objBankTransactions = new BankingObjects();
                        //driver = objBankTransactions.BankTransactions(driver, 1);
                        //new Common(driver).pause(2000);

                        intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='Done' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);
                    }

                    else
                    {
                        intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='Skipped' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);
                    }

                    //dt = Common.ExcelReadData("select * from [DemoBusiness$] where [status] = 'Pending' ", strRegDataExcelFilePath);

                    try
                    {
                        new Common(driver).pause(3000);
                        driver.SwitchTo().DefaultContent();
                        IWebElement Dashboard_ddl_DemoBusiness = driver.FindElement(By.XPath(ElementLocators.Dashboard_ddl_DemoBusiness));
                        Dashboard_ddl_DemoBusiness.Click();
                        new Common(driver).pause(1000);
                        Dashboard_btn_logout = driver.FindElement(By.Id(ElementLocators.Dashboard_btn_logout));
                        Dashboard_btn_logout.Click();
                        new Common(driver).pause(3000);
                    }
                    catch (Exception ex) { Console.WriteLine("77"); }
                    if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
                    {
                        Browser.CloseBrowser(driver);
                    }

                    Report.AddToHtmlReport("<hr><br><br>");
                    //break; //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Remove before deployment @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                }
            }
            else
            {
                Console.WriteLine("No pending data found in excel.");
                Report.AddToHtmlReportPassed("No pending data found in excel.");
            }
        }

        public void UserRegistrationAndSignUpWithAllDemoBusiness()
        {
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Check user Sign up Functionality (read data from excel).", true, false, true);
            Console.WriteLine("Check user Sign up Functionality (read data from excel).");
            LoginObjects objLogin = new LoginObjects();
            DataTable dt = Common.ExcelReadData("select * from [SignupConfig$] ", strRegDataExcelFilePath);
            if (dt.Rows.Count > 0)
            {
                driver = objLogin.UserRegistrationAndSignUpWithAllDemoBusiness(driver, dt);
            }
            else
            {
                Console.WriteLine("No pending data found in excel.");
                Report.AddToHtmlReportPassed("No pending data found in excel.");
            }
        }

        public IWebDriver SignUpWithAllDemoBusinessUR(IWebDriver _driver, string strUserName, string strPwd)
        {
            driver = _driver;
            if (!TestSuites.IsTCAdded)
            {
                Report.AddToHtmlReportTCHeader("QA Automation Suite - Phase I.");
                TestSuites.IsTCAdded = true;
            }
            Report.AddToHtmlReport("Create new business for advisor with demo business.", true, false, true);
            Console.WriteLine("Create new business for advisor with demo business.");

            string strURL = string.Empty;
            LoginObjects objLogin = new LoginObjects();
            //DataTable dt = Common.ExcelReadData("select * from [DemoBusiness$] where [status] = 'Pending' ", strRegDataExcelFilePath);
            int intRecCnt = 0;
            //ItemObjects objItem = new ItemObjects();
            //Thread oThread = new Thread(new ThreadStart(objItem.CreateItems(driver, 1)));
            //if (dt.Rows.Count > 0)
            {
                //for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Report.AddToHtmlReport("Demo Business Entry <br>", false, true);
                    //strURL = Convert.ToString(dt.Rows[i][0]);
                    //strUserName = Convert.ToString(dt.Rows[i][1]);
                    //strPwd = Convert.ToString(dt.Rows[i][2]);
                    //intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='In-progress' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);

                    //driver = objLogin.SignUpWithAllDemoBusiness(driver, dt);
                    driver = objLogin.SignUpWithAllDemoBusinessUR(driver, strUserName, strPwd);
                    if (TestCases.Login.IsSetupAssiPopupOpen)
                    {
                        //Create Items #####
                        Report.AddToHtmlReport("<br>Create Items process.", false, true, true);
                        Console.WriteLine("Create Items process.");
                        ItemObjects objItem = new ItemObjects();
                        driver = objItem.CreateItems(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Customers #####
                        Report.AddToHtmlReport("<br>Create Customer process.", false, true, true);
                        Console.WriteLine("Create Customer process.");
                        ContactsObjects objCustomer = new ContactsObjects();
                        driver = objCustomer.CreateCustomer(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Suppliers #####
                        Report.AddToHtmlReport("<br>Create Supplier process.", false, true, true);
                        Console.WriteLine("Create Supplier process.");
                        ContactsObjects objSupplier = new ContactsObjects();
                        driver = objSupplier.CreateSupplier(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Invoice #####
                        Report.AddToHtmlReport("<br>Create Invoice process.", false, true, true);
                        Console.WriteLine("Create Invoice process.");
                        LoginObjects objCreateInvoice = new LoginObjects();
                        driver = objCreateInvoice.CreateInvoice(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Quotes
                        Report.AddToHtmlReport("<br>Create Quote process.", false, true, true);
                        Console.WriteLine("Create Quote process.");
                        LoginObjects objCreateQuote = new LoginObjects();
                        driver = objCreateQuote.CreateQuote(driver, 1);
                        new Common(driver).pause(2000);

                        ////Customer return ##### Now no need to process (As per call with VAR on 18Mar2014)
                        //Report.AddToHtmlReport("<br>Customer return process.", false, true, true);
                        //Console.WriteLine("Customer return process.");
                        //LoginObjects objCreateCustReturn = new LoginObjects();
                        //driver = objCreateCustReturn.CreateCustReturn(driver, 1);
                        //new Common(driver).pause(1000);

                        //Enter Expense #####
                        Report.AddToHtmlReport("<br>Enter Expense process.", false, true, true);
                        Console.WriteLine("Enter Expense process.");
                        LoginObjects objEnterExpense = new LoginObjects();
                        driver = objEnterExpense.EnterExpense(driver, 1);
                        new Common(driver).pause(1000);

                        //Create Employee #####
                        Report.AddToHtmlReport("<br>Create Employee process.", false, true, true);
                        Console.WriteLine("Create Employee process.");
                        EmployeeObjects objEmployee = new EmployeeObjects();
                        driver = objEmployee.CreateEmployee(driver, 1);
                        new Common(driver).pause(1000);

                        //Generate Payruns #####
                        Report.AddToHtmlReport("<br>Generate Payruns process.", false, true, true);
                        Console.WriteLine("Generate Payruns process.");
                        PayrollObjects objPayroll = new PayrollObjects();
                        driver = objPayroll.Payruns(driver, 1);
                        new Common(driver).pause(1000);

                        //Spend Money #####
                        Report.AddToHtmlReport("<br>Generate Spend Money process.", false, true, true);
                        Console.WriteLine("Generate Spend Money process.");
                        BankingObjects objSpendMoney = new BankingObjects();
                        driver = objSpendMoney.SpendMoney(driver, 1);
                        new Common(driver).pause(1000);

                        //Receive Money #####
                        Report.AddToHtmlReport("<br>Generate Receive Money process.", false, true, true);
                        Console.WriteLine("Generate Receive Money process.");
                        BankingObjects objReceiveMoney = new BankingObjects();
                        driver = objReceiveMoney.ReceiveMoney(driver, 1);
                        new Common(driver).pause(1000);

                        //Receive Money #####
                        Report.AddToHtmlReport("<br>Import Bank Transactions from .qif file process.", false, true, true);
                        Console.WriteLine("Import Bank Transactions from .qif file process.");
                        BankingObjects objBankTransactions = new BankingObjects();
                        driver = objBankTransactions.BankTransactions(driver, 1);
                        new Common(driver).pause(1000);

                        intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='Done' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);
                    }

                    else
                    {
                        intRecCnt = Common.ExcelWriteData("UPDATE [DemoBusiness$] SET [status]='Skipped' where [username] = '" + strUserName + "' and [password] = '" + strPwd + "' ", strRegDataExcelFilePath);
                    }

                    //dt = Common.ExcelReadData("select * from [DemoBusiness$] where [status] = 'Pending' ", strRegDataExcelFilePath);

                    try
                    {
                        new Common(driver).pause(3000);
                        driver.SwitchTo().DefaultContent();
                        IWebElement Dashboard_ddl_DemoBusiness = driver.FindElement(By.XPath(ElementLocators.Dashboard_ddl_DemoBusiness));
                        Dashboard_ddl_DemoBusiness.Click();
                        new Common(driver).pause(1000);
                        Dashboard_btn_logout = driver.FindElement(By.Id(ElementLocators.Dashboard_btn_logout));
                        Dashboard_btn_logout.Click();
                        new Common(driver).pause(3000);
                    }
                    catch (Exception ex) { Console.WriteLine("77"); }
                    if (Convert.ToInt16(ConfigurationSettings.AppSettings.Get("CloseBrowser")) == 1)
                    {
                        Browser.CloseBrowser(driver);
                    }

                    Report.AddToHtmlReport("<hr><br><br>");
                    //break; //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Remove before deployment @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                }
            }
            //else
            //{
            //    Console.WriteLine("No pending data found in excel.");
            //    Report.AddToHtmlReportPassed("No pending data found in excel.");
            //}

            return driver;
        }
    }
}
