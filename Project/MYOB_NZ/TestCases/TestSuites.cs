﻿using System;
using Framework.Init;
using NUnit.Framework;
using System.Threading;

namespace TestCases
{
    [TestFixture]
    public class TestSuites
    {
        static bool IsTestFinished;
        static Int32 intLoginPassCnt = 0;
        static Int32 intLoginFailCnt = 0;
        static Int32 intSignupPassCnt = 0;
        static Int32 intSignupFailCnt = 0;
        static Int32 intCreateClassPassCnt = 0;
        static Int32 intCreateClassFailCnt = 0;
        static Int32 intProfileOwnPassCnt = 0;
        static Int32 intProfileOwnFailCnt = 0;
        static Int32 intAboutusPassCnt = 0;
        static Int32 intAboutusFailCnt = 0;
        public static bool IsTCAdded;
        
        public TestSuites()
        {
            Report.sbHtml = null;
            Report.sbSummaryHtml = null;
            IsTestFinished = true;
            intLoginPassCnt = 0;
            intLoginFailCnt = 0;
            intSignupPassCnt = 0;
            intSignupFailCnt = 0;
            intCreateClassPassCnt = 0;
            intCreateClassFailCnt = 0;
            intProfileOwnPassCnt = 0;
            intProfileOwnFailCnt = 0;
            intAboutusPassCnt = 0;
            intAboutusFailCnt = 0;
            //if (Report.TCcnt > 1) Report.TCcnt = 1;
            Report.TCcnt = 1;
            //IsTCAddedLogin = false;
            //IsTCAddedSignup = false;
            //IsTCAddedCreateClass = false;
            //IsTCAddedProfileOwn = false;
            //IsTCAddedAboutus = false;
            Report.IsPassed = 0;

            //IsTCAdded = false;
            //Signup.IsTCAdded = false;
            IsTCAdded = false;
            
            Report.IsHeaderUpdated = false;
            Report.sbTcHtml = null;
            Report.sbFeatureHtml = null;
            //Console.WriteLine("TestSuites Cons Called");
            /*
            Console.WriteLine("sbHtml Bfore" + Report.sbHtml);
            Report.sbHtml = null;
            Console.WriteLine("sbHtml Aftr" + Report.sbHtml);*/
        }

        #region ## Login Suites ##
        
        //[Test, RequiresSTA]
        //public void LoginBlankData()
        //{
        //    try
        //    {
        //        waitForPrevTestToFinish();
        //        IsTestFinished = false;
        //        Login objLogin = new Login();
        //        objLogin.LoginBlankData();
        //    }
        //    finally
        //    {
        //        Report.AddToHtmlReportFeatureFinish();
        //        Report.GenerateHtmlReport();
        //        IsTestFinished = true;
        //        if (Report.IsFtrPassed == 1) intLoginPassCnt++;
        //        else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
        //        IsTCAdded = true;
        //    }
        //}

        [Test, RequiresSTA]
        public void CreateInvoice()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Login objLogin = new Login();
                objLogin.CreateInvoice();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void EnterExpense()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Login objLogin = new Login();
                objLogin.EnterExpense();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void CreateCustReturn()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Login objLogin = new Login();
                objLogin.CreateCustReturn();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void UserRegistration()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Login objLogin = new Login();
                objLogin.UserRegistration();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void CreateItems()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Item objItem = new Item();
                objItem.CreateItems();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void CreateCustomer()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Contacts objContacts = new Contacts();
                objContacts.CreateCustomer();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void CreateSupplier()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Contacts objContacts = new Contacts();
                objContacts.CreateSupplier();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void CreateEmployee()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Employee objEmployee = new Employee();
                objEmployee.CreateEmployee();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
       
        [Test, RequiresSTA]
        public void Payruns()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Payroll objPayroll = new Payroll();
                objPayroll.Payruns();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
       
        [Test, RequiresSTA]
        public void SpendMoney()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Banking objBanking = new Banking();
                objBanking.SpendMoney();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
       
        [Test, RequiresSTA]
        public void ReceiveMoney()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Banking objBanking = new Banking();
                objBanking.ReceiveMoney();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
       
        [Test, RequiresSTA]
        public void BankTransactions()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Banking objBanking = new Banking();
                objBanking.BankTransactions();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
       
        [Test, RequiresMTA]
        public void SignUpWithAllDemoBusiness()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Login objLogin = new Login();
                objLogin.SignUpWithAllDemoBusiness();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
       
        [Test, RequiresMTA]
        public void SignUpWithAllDemoBusiness1()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                
                Login objLogin = new Login();
                objLogin.SignUpWithAllDemoBusiness1();
                //Thread thr = null;
                //for (int i = 0; i < 2; i++)
                //{
                //    thr = new Thread(new ThreadStart(objLogin.SignUpWithAllDemoBusiness1));
                //    thr.Start();
                //    Thread.Sleep(TimeSpan.FromMinutes(5));
                //}
                //if (thr != null) thr.Join();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }

        [Test, RequiresSTA]
        public void UserRegistrationAndSignUpWithAllDemoBusiness()
        {
            try
            {
                waitForPrevTestToFinish();
                IsTestFinished = false;
                Login objLogin = new Login();
                objLogin.UserRegistrationAndSignUpWithAllDemoBusiness();
            }
            finally
            {
                Report.AddToHtmlReportFeatureFinish();
                Report.GenerateHtmlReport();
                IsTestFinished = true;
                if (Report.IsFtrPassed == 1) intLoginPassCnt++;
                else if (Report.IsFtrPassed == 2) intLoginFailCnt++;
                IsTCAdded = true;
            }
        }
        
        #endregion

        //[Test, RequiresSTA]
        [TestFixtureTearDown]
        public void zzzGenerateSummaryReport()
        {
            try
            {
                Report.AddToHtmlSummaryReport("QA Automation Suite - Phase I.", intLoginPassCnt, intLoginFailCnt);
                Report.AddToHtmlSummaryReportTotal();
                Report.GenerateHtmlSummaryReport();
                //Console.WriteLine("Teardown called....");
            }
            finally {
                Report._intPassedCnt = 0;
                Report._intFailedCnt = 0;
                Report._inTotalCnt = 0;
            }
        }
        
        // #######################################################
        private void waitForPrevTestToFinish()
        {
            while (true) {
                if (!IsTestFinished) Common.pauseStatic(2000);
                else break;
            }
        }
    }
}
